# Embedded file name: MainGui.py
import wx, ConfigParser
import wx.xrc
import wx.richtext
from gui.StateMachine import StateMachine
from gui.BufferConsole import *
from gui.sysVars import *

def strip(s):
    return s.strip()


class MainGUI(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u'AFI TEST PROGRAM ', pos=wx.DefaultPosition, size=wx.Size(900, 700), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        self.SetFont(wx.Font(10, 74, 90, 90, False, 'Arial'))
        self.SetIcon(wx.Icon(u'icon/PyScripter.ico', wx.BITMAP_TYPE_ICO))
        self.SetBackgroundColour(wx.Colour(245, 245, 245))
        self.DisableColor = wx.Colour(192, 192, 192)
        self.YellowColor = wx.Colour(255, 255, 0)
        self.RedColor = wx.Colour(255, 0, 0)
        self.BlueColor = wx.Colour(0, 0, 255)
        self.LimeColor = wx.Colour(0, 255, 0)
        self.ActionColor = wx.Colour(85, 141, 255)
        self.m_menubar1 = wx.MenuBar(0)
        self.M_Debug = wx.Menu()
        self.m_menu2 = wx.Menu()
        self.M_L_CCU = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'CCU Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_CCU)
        self.M_L_S0 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S0 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S0)
        self.M_L_S1 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S1 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S1)
        self.M_L_S2 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S2 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S2)
        self.M_L_S3 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S3 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S3)
        self.M_L_S4 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S4 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S4)
        self.M_L_S5 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S5 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S5)
        self.M_L_S6 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S6 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S6)
        self.m_menu2.AppendSeparator()
        self.M_L_DUT0 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT0 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT0)
        self.M_L_DUT1 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT1 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT1)
        self.M_L_DUT2 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT2 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT2)
        self.M_L_DUT3 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT3 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT3)
        self.M_L_DUT4 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT4 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT4)
        self.M_L_DUT5 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT5 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT5)
        self.M_L_DUT6 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT6 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT6)
        self.M_L_DUT7 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT7 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT7)
        self.M_Debug.AppendSubMenu(self.m_menu2, u'Left')
        self.m_menu21 = wx.Menu()
        self.M_R_CCU = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'CCU Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_CCU)
        self.M_R_S0 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S0 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S0)
        self.M_R_S1 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S1 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S1)
        self.M_R_S2 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S2 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S2)
        self.M_R_S3 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S3 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S3)
        self.M_R_S4 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S4 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S4)
        self.M_R_S5 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S5 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S5)
        self.M_R_S6 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S6 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S6)
        self.m_menu21.AppendSeparator()
        self.M_R_DUT0 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT0 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT0)
        self.M_R_DUT1 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT1 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT1)
        self.M_R_DUT2 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT2 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT2)
        self.M_R_DUT3 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT3 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT3)
        self.M_R_DUT4 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT4 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT4)
        self.M_R_DUT5 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT5 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT5)
        self.M_R_DUT6 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT6 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT6)
        self.M_R_DUT7 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT7 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT7)
        self.M_Debug.AppendSubMenu(self.m_menu21, u'RIGHT')
        self.m_menubar1.Append(self.M_Debug, u'Debug')
        self.M_Config = wx.Menu()
        self.m_menubar1.Append(self.M_Config, u'Config')
        self.Process = self.M_Config.Append(-1, 'Process')
        self.M_MES = wx.Menu()
        self.m_menubar1.Append(self.M_MES, u'MES')
        self.Repair = self.M_MES.Append(-1, 'To Repair')
        self.m_tools = wx.Menu()
        self.m_menubar1.Append(self.m_tools, u'Tools')
        self.m_calibration = self.m_tools.Append(-1, 'Calibration')
        self.M_Help = wx.Menu()
        self.m_menubar1.Append(self.M_Help, u'Help')
        self.About = self.M_Help.Append(-1, 'About')
        self.SetMenuBar(self.m_menubar1)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)
        bSizer9 = wx.BoxSizer(wx.VERTICAL)
        self.m_staticText1 = wx.StaticText(self, wx.ID_ANY, u'AFI MANUFACTORY TEST PROGRAM', wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE)
        self.m_staticText1.Wrap(-1)
        self.m_staticText1.SetFont(wx.Font(20, 74, 90, 92, False, wx.EmptyString))
        self.m_staticText1.SetForegroundColour(wx.Colour(0, 128, 0))
        bSizer9.Add(self.m_staticText1, 1, wx.ALL | wx.EXPAND, 5)
        self.gui_version = wx.StaticText(self, wx.ID_ANY, VERSION, wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT)
        self.gui_version.Wrap(-1)
        bSizer9.Add(self.gui_version, 0, wx.ALL | wx.EXPAND, 5)
        bSizer1.Add(bSizer9, 0, wx.EXPAND, 5)
        self.m_staticline4 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, 5), wx.LI_HORIZONTAL)
        bSizer1.Add(self.m_staticline4, 0, wx.EXPAND | wx.ALL, 5)
        bSizer7 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticline5 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_VERTICAL)
        bSizer7.Add(self.m_staticline5, 0, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)
        bSizer6 = wx.BoxSizer(wx.VERTICAL)
        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u'LABEL :', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        self.m_staticText4.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizer4.Add(self.m_staticText4, 0, wx.ALL, 5)
        self.LabelName = wx.StaticText(self, wx.ID_ANY, u'PN', wx.DefaultPosition, wx.DefaultSize, 0)
        self.LabelName.Wrap(-1)
        self.LabelName.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        self.LabelName.SetBackgroundColour(wx.Colour(153, 180, 209))
        bSizer4.Add(self.LabelName, 0, wx.ALL, 5)
        bSizer6.Add(bSizer4, 0, 0, 5)
        bSizer5 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText6 = wx.StaticText(self, wx.ID_ANY, u'SCAN', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText6.Wrap(-1)
        self.m_staticText6.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizer5.Add(self.m_staticText6, 0, wx.ALL, 5)
        self.ScanLabel = wx.TextCtrl(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.Size(120, -1), style=wx.TE_PROCESS_ENTER)
        self.ScanLabel.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.ScanLabel.SetBackgroundColour(wx.Colour(255, 255, 0))
        bSizer5.Add(self.ScanLabel, 0, 0, 5)
        bSizer6.Add(bSizer5, 0, 0, 5)
        bSizer7.Add(bSizer6, 0, wx.EXPAND, 5)
        self.m_staticline6 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_VERTICAL)
        bSizer7.Add(self.m_staticline6, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_BOTTOM | wx.EXPAND, 5)
        bSizer22 = wx.BoxSizer(wx.VERTICAL)
        bSizer23 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText21 = wx.StaticText(self, wx.ID_ANY, u'TFTP', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText21.Wrap(-1)
        self.m_staticText21.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizer23.Add(self.m_staticText21, 0, wx.ALL, 5)
        self.TFTPServerState = wx.StaticText(self, wx.ID_ANY, u'    ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.TFTPServerState.Wrap(-1)
        self.TFTPServerState.SetBackgroundColour(self.DisableColor)
        bSizer23.Add(self.TFTPServerState, 0, wx.ALL, 5)
        bSizer22.Add(bSizer23, 0, 0, 5)
        bSizer231 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText211 = wx.StaticText(self, wx.ID_ANY, u'AFI   ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText211.Wrap(-1)
        self.m_staticText211.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizer231.Add(self.m_staticText211, 0, wx.ALL, 5)
        self.AFIServerState = wx.StaticText(self, wx.ID_ANY, u'    ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.AFIServerState.Wrap(-1)
        self.AFIServerState.SetBackgroundColour(self.DisableColor)
        bSizer231.Add(self.AFIServerState, 0, wx.ALL, 5)
        bSizer22.Add(bSizer231, 1, 0, 5)
        bSizer7.Add(bSizer22, 0, wx.EXPAND, 5)
        self.m_staticline51 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
        bSizer7.Add(self.m_staticline51, 0, wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        bSizer14 = wx.BoxSizer(wx.VERTICAL)
        bSizer10 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText7 = wx.StaticText(self, wx.ID_ANY, u'Module Name:', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText7.Wrap(-1)
        self.m_staticText7.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizer10.Add(self.m_staticText7, 0, wx.ALL, 5)
        self.ModuleName = wx.StaticText(self, wx.ID_ANY, u'My Module', wx.DefaultPosition, (100, -1), 0)
        self.ModuleName.Wrap(-1)
        self.ModuleName.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        self.ModuleName.SetForegroundColour(wx.Colour(0, 128, 255))
        bSizer10.Add(self.ModuleName, 0, wx.ALL, 5)
        bSizer14.Add(bSizer10, 1, wx.EXPAND, 5)
        bSizer101 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText71 = wx.StaticText(self, wx.ID_ANY, u'Station Name:', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText71.Wrap(-1)
        self.m_staticText71.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizer101.Add(self.m_staticText71, 0, wx.ALL, 5)
        self.StationName = wx.StaticText(self, wx.ID_ANY, u'My Station', wx.DefaultPosition, (100, -1), 0)
        self.StationName.Wrap(-1)
        self.StationName.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        self.StationName.SetForegroundColour(wx.Colour(0, 128, 255))
        bSizer101.Add(self.StationName, 0, wx.ALL, 5)
        bSizer14.Add(bSizer101, 1, wx.EXPAND, 5)
        bSizer7.Add(bSizer14, 0, wx.EXPAND, 5)
        self.m_staticline61 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
        bSizer7.Add(self.m_staticline61, 0, wx.EXPAND | wx.RIGHT | wx.LEFT, 5)
        bSizer141 = wx.BoxSizer(wx.VERTICAL)
        bSizer102 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText72 = wx.StaticText(self, wx.ID_ANY, u'TFTP Path:', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText72.Wrap(-1)
        self.m_staticText72.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizer102.Add(self.m_staticText72, 0, wx.ALL, 5)
        self.TFTPPath = wx.StaticText(self, wx.ID_ANY, u'My TFTP', wx.DefaultPosition, wx.DefaultSize, 0)
        self.TFTPPath.Wrap(-1)
        self.TFTPPath.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        self.TFTPPath.SetForegroundColour(wx.Colour(0, 128, 255))
        bSizer102.Add(self.TFTPPath, 0, wx.ALL, 5)
        bSizer141.Add(bSizer102, 1, wx.EXPAND, 5)
        bSizer1011 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText711 = wx.StaticText(self, wx.ID_ANY, u'Transfer File:', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText711.Wrap(-1)
        self.m_staticText711.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizer1011.Add(self.m_staticText711, 0, wx.ALL, 5)
        self.TFTPTransferFile = wx.StaticText(self, wx.ID_ANY, u'My File', wx.DefaultPosition, wx.DefaultSize, 0)
        self.TFTPTransferFile.Wrap(-1)
        self.TFTPTransferFile.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        self.TFTPTransferFile.SetForegroundColour(wx.Colour(0, 128, 255))
        bSizer1011.Add(self.TFTPTransferFile, 0, wx.ALL, 5)
        bSizer141.Add(bSizer1011, 1, wx.EXPAND, 5)
        bSizer7.Add(bSizer141, 0, wx.EXPAND, 5)
        bSizer1.Add(bSizer7, 0, 0, 5)
        bSizer3 = wx.BoxSizer(wx.VERTICAL)
        self.m_staticline7 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, 5), wx.LI_HORIZONTAL)
        bSizer3.Add(self.m_staticline7, 0, wx.EXPAND | wx.ALL, 5)
        gSizer1 = wx.GridSizer(0, 4, 0, 0)
        self.Panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel1.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel1.SetBackgroundColour(self.DisableColor)
        #self.Panel1.SetToolTipString(u'Failed:ERROR')
        _Group1 = wx.StaticBox(self.Panel1, wx.ID_ANY, u'DUT 1 - 123456789')
        Group1 = wx.StaticBoxSizer(_Group1, wx.VERTICAL)
        bSizer25 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText17 = wx.StaticText(self.Panel1, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText17.Wrap(-1)
        self.m_staticText17.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer25.Add(self.m_staticText17, 0, wx.ALL, 5)
        self.Barcode11 = wx.TextCtrl(self.Panel1, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode11.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer25.Add(self.Barcode11, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group1.Add(bSizer25, 1, wx.EXPAND, 5)
        bSizer253 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText173 = wx.StaticText(self.Panel1, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText173.Wrap(-1)
        self.m_staticText173.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer253.Add(self.m_staticText173, 0, wx.ALL, 5)
        self.Barcode12 = wx.TextCtrl(self.Panel1, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode12.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer253.Add(self.Barcode12, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group1.Add(bSizer253, 1, wx.EXPAND, 5)
        bSizer254 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText174 = wx.StaticText(self.Panel1, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText174.Wrap(-1)
        self.m_staticText174.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer254.Add(self.m_staticText174, 0, wx.ALL, 5)
        self.Process1 = wx.TextCtrl(self.Panel1, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process1.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer254.Add(self.Process1, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group1.Add(bSizer254, 1, wx.EXPAND, 5)
        self.Gauge1 = wx.Gauge(self.Panel1, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge1.SetValue(98)
        self.Gauge1.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group1.Add(self.Gauge1, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel1.SetSizer(Group1)
        self.Panel1.Layout()
        Group1.Fit(self.Panel1)
        gSizer1.Add(self.Panel1, 1, wx.ALL | wx.EXPAND, 5)
        self.Panel3 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel3.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel3.SetBackgroundColour(self.DisableColor)
        #self.Panel3.SetToolTipString(u'Failed:ERROR')
        _Group3 = wx.StaticBox(self.Panel3, wx.ID_ANY, u'DUT 1 - 123456789')
        Group3 = wx.StaticBoxSizer(_Group3, wx.VERTICAL)
        bSizer255 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText175 = wx.StaticText(self.Panel3, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText175.Wrap(-1)
        self.m_staticText175.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer255.Add(self.m_staticText175, 0, wx.ALL, 5)
        self.Barcode31 = wx.TextCtrl(self.Panel3, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode31.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer255.Add(self.Barcode31, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group3.Add(bSizer255, 1, wx.EXPAND, 5)
        bSizer2531 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1731 = wx.StaticText(self.Panel3, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1731.Wrap(-1)
        self.m_staticText1731.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2531.Add(self.m_staticText1731, 0, wx.ALL, 5)
        self.Barcode32 = wx.TextCtrl(self.Panel3, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode32.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2531.Add(self.Barcode32, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group3.Add(bSizer2531, 1, wx.EXPAND, 5)
        bSizer2541 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1741 = wx.StaticText(self.Panel3, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1741.Wrap(-1)
        self.m_staticText1741.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2541.Add(self.m_staticText1741, 0, wx.ALL, 5)
        self.Process3 = wx.TextCtrl(self.Panel3, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process3.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2541.Add(self.Process3, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group3.Add(bSizer2541, 1, wx.EXPAND, 5)
        self.Gauge3 = wx.Gauge(self.Panel3, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge3.SetValue(98)
        self.Gauge3.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group3.Add(self.Gauge3, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel3.SetSizer(Group3)
        self.Panel3.Layout()
        Group3.Fit(self.Panel3)
        gSizer1.Add(self.Panel3, 1, wx.ALL | wx.EXPAND, 5)
        self.Panel5 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel5.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel5.SetBackgroundColour(self.DisableColor)
        #self.Panel5.SetToolTipString(u'Failed:ERROR')
        _Group5 = wx.StaticBox(self.Panel5, wx.ID_ANY, u'DUT 1 - 123456789')
        Group5 = wx.StaticBoxSizer(_Group5, wx.VERTICAL)
        bSizer251 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText171 = wx.StaticText(self.Panel5, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText171.Wrap(-1)
        self.m_staticText171.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer251.Add(self.m_staticText171, 0, wx.ALL, 5)
        self.Barcode51 = wx.TextCtrl(self.Panel5, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode51.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer251.Add(self.Barcode51, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group5.Add(bSizer251, 1, wx.EXPAND, 5)
        bSizer2532 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1732 = wx.StaticText(self.Panel5, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1732.Wrap(-1)
        self.m_staticText1732.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2532.Add(self.m_staticText1732, 0, wx.ALL, 5)
        self.Barcode52 = wx.TextCtrl(self.Panel5, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode52.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2532.Add(self.Barcode52, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group5.Add(bSizer2532, 1, wx.EXPAND, 5)
        bSizer2542 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1742 = wx.StaticText(self.Panel5, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1742.Wrap(-1)
        self.m_staticText1742.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2542.Add(self.m_staticText1742, 0, wx.ALL, 5)
        self.Process5 = wx.TextCtrl(self.Panel5, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process5.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2542.Add(self.Process5, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group5.Add(bSizer2542, 1, wx.EXPAND, 5)
        self.Gauge5 = wx.Gauge(self.Panel5, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge5.SetValue(98)
        self.Gauge5.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group5.Add(self.Gauge5, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel5.SetSizer(Group5)
        self.Panel5.Layout()
        Group5.Fit(self.Panel5)
        gSizer1.Add(self.Panel5, 1, wx.EXPAND | wx.ALL, 5)
        self.Panel7 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel7.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel7.SetBackgroundColour(self.DisableColor)
        #self.Panel7.SetToolTipString(u'')
        _Group7 = wx.StaticBox(self.Panel7, wx.ID_ANY, u'DUT 1 - 123456789')
        Group7 = wx.StaticBoxSizer(_Group7, wx.VERTICAL)
        bSizer252 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText172 = wx.StaticText(self.Panel7, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText172.Wrap(-1)
        self.m_staticText172.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer252.Add(self.m_staticText172, 0, wx.ALL, 5)
        self.Barcode71 = wx.TextCtrl(self.Panel7, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode71.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer252.Add(self.Barcode71, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group7.Add(bSizer252, 1, wx.EXPAND, 5)
        bSizer2533 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1733 = wx.StaticText(self.Panel7, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1733.Wrap(-1)
        self.m_staticText1733.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2533.Add(self.m_staticText1733, 0, wx.ALL, 5)
        self.Barcode72 = wx.TextCtrl(self.Panel7, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode72.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2533.Add(self.Barcode72, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group7.Add(bSizer2533, 1, wx.EXPAND, 5)
        bSizer2543 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1743 = wx.StaticText(self.Panel7, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1743.Wrap(-1)
        self.m_staticText1743.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2543.Add(self.m_staticText1743, 0, wx.ALL, 5)
        self.Process7 = wx.TextCtrl(self.Panel7, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process7.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2543.Add(self.Process7, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group7.Add(bSizer2543, 1, wx.EXPAND, 5)
        self.Gauge7 = wx.Gauge(self.Panel7, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge7.SetValue(98)
        self.Gauge7.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group7.Add(self.Gauge7, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel7.SetSizer(Group7)
        self.Panel7.Layout()
        Group7.Fit(self.Panel7)
        gSizer1.Add(self.Panel7, 1, wx.EXPAND | wx.ALL, 5)
        self.Panel2 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel2.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel2.SetBackgroundColour(self.DisableColor)
        #self.Panel2.SetToolTipString(u'Failed:ERROR')
        _Group2 = wx.StaticBox(self.Panel2, wx.ID_ANY, u'DUT 1 - 123456789')
        Group2 = wx.StaticBoxSizer(_Group2, wx.VERTICAL)
        bSizer256 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText176 = wx.StaticText(self.Panel2, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText176.Wrap(-1)
        self.m_staticText176.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer256.Add(self.m_staticText176, 0, wx.ALL, 5)
        self.Barcode21 = wx.TextCtrl(self.Panel2, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode21.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer256.Add(self.Barcode21, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group2.Add(bSizer256, 1, wx.EXPAND, 5)
        bSizer2534 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1734 = wx.StaticText(self.Panel2, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1734.Wrap(-1)
        self.m_staticText1734.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2534.Add(self.m_staticText1734, 0, wx.ALL, 5)
        self.Barcode22 = wx.TextCtrl(self.Panel2, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode22.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2534.Add(self.Barcode22, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group2.Add(bSizer2534, 1, wx.EXPAND, 5)
        bSizer2544 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1744 = wx.StaticText(self.Panel2, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1744.Wrap(-1)
        self.m_staticText1744.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2544.Add(self.m_staticText1744, 0, wx.ALL, 5)
        self.Process2 = wx.TextCtrl(self.Panel2, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process2.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2544.Add(self.Process2, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group2.Add(bSizer2544, 1, wx.EXPAND, 5)
        self.Gauge2 = wx.Gauge(self.Panel2, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge2.SetValue(98)
        self.Gauge2.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group2.Add(self.Gauge2, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel2.SetSizer(Group2)
        self.Panel2.Layout()
        Group2.Fit(self.Panel2)
        gSizer1.Add(self.Panel2, 1, wx.EXPAND | wx.ALL, 5)
        self.Panel4 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel4.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel4.SetBackgroundColour(self.DisableColor)
        #self.Panel4.SetToolTipString(u'Failed:ERROR')
        _Group4 = wx.StaticBox(self.Panel4, wx.ID_ANY, u'DUT 1 - 123456789')
        Group4 = wx.StaticBoxSizer(_Group4, wx.VERTICAL)
        bSizer257 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText177 = wx.StaticText(self.Panel4, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText177.Wrap(-1)
        self.m_staticText177.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer257.Add(self.m_staticText177, 0, wx.ALL, 5)
        self.Barcode41 = wx.TextCtrl(self.Panel4, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode41.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer257.Add(self.Barcode41, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group4.Add(bSizer257, 1, wx.EXPAND, 5)
        bSizer2535 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1735 = wx.StaticText(self.Panel4, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1735.Wrap(-1)
        self.m_staticText1735.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2535.Add(self.m_staticText1735, 0, wx.ALL, 5)
        self.Barcode42 = wx.TextCtrl(self.Panel4, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode42.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2535.Add(self.Barcode42, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group4.Add(bSizer2535, 1, wx.EXPAND, 5)
        bSizer2545 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1745 = wx.StaticText(self.Panel4, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1745.Wrap(-1)
        self.m_staticText1745.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2545.Add(self.m_staticText1745, 0, wx.ALL, 5)
        self.Process4 = wx.TextCtrl(self.Panel4, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process4.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2545.Add(self.Process4, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group4.Add(bSizer2545, 1, wx.EXPAND, 5)
        self.Gauge4 = wx.Gauge(self.Panel4, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge4.SetValue(98)
        self.Gauge4.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group4.Add(self.Gauge4, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel4.SetSizer(Group4)
        self.Panel4.Layout()
        Group4.Fit(self.Panel4)
        gSizer1.Add(self.Panel4, 1, wx.EXPAND | wx.ALL, 5)
        self.Panel6 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel6.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel6.SetBackgroundColour(self.DisableColor)
        #self.Panel6.SetToolTipString(u'Failed:ERROR')
        _Group6 = wx.StaticBox(self.Panel6, wx.ID_ANY, u'DUT 1 - 123456789')
        Group6 = wx.StaticBoxSizer(_Group6, wx.VERTICAL)
        bSizer258 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText178 = wx.StaticText(self.Panel6, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText178.Wrap(-1)
        self.m_staticText178.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer258.Add(self.m_staticText178, 0, wx.ALL, 5)
        self.Barcode61 = wx.TextCtrl(self.Panel6, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode61.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer258.Add(self.Barcode61, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group6.Add(bSizer258, 1, wx.EXPAND, 5)
        bSizer2536 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1736 = wx.StaticText(self.Panel6, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1736.Wrap(-1)
        self.m_staticText1736.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2536.Add(self.m_staticText1736, 0, wx.ALL, 5)
        self.Barcode62 = wx.TextCtrl(self.Panel6, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode62.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2536.Add(self.Barcode62, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group6.Add(bSizer2536, 1, wx.EXPAND, 5)
        bSizer2546 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1746 = wx.StaticText(self.Panel6, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1746.Wrap(-1)
        self.m_staticText1746.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2546.Add(self.m_staticText1746, 0, wx.ALL, 5)
        self.Process6 = wx.TextCtrl(self.Panel6, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process6.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2546.Add(self.Process6, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group6.Add(bSizer2546, 1, wx.EXPAND, 5)
        self.Gauge6 = wx.Gauge(self.Panel6, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge6.SetValue(98)
        self.Gauge6.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group6.Add(self.Gauge6, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel6.SetSizer(Group6)
        self.Panel6.Layout()
        Group6.Fit(self.Panel6)
        gSizer1.Add(self.Panel6, 1, wx.EXPAND | wx.ALL, 5)
        self.Panel8 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel8.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel8.SetBackgroundColour(wx.Colour(255, 255, 50))
        #self.Panel8.SetToolTipString(u'Failed:ERROR')
        _Group8 = wx.StaticBox(self.Panel8, wx.ID_ANY, u'DUT 1 - 123456789')
        Group8 = wx.StaticBoxSizer(_Group8, wx.VERTICAL)
        bSizer259 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText179 = wx.StaticText(self.Panel8, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText179.Wrap(-1)
        self.m_staticText179.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer259.Add(self.m_staticText179, 0, wx.ALL, 5)
        self.Barcode81 = wx.TextCtrl(self.Panel8, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode81.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer259.Add(self.Barcode81, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group8.Add(bSizer259, 1, wx.EXPAND, 5)
        bSizer2537 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1737 = wx.StaticText(self.Panel8, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1737.Wrap(-1)
        self.m_staticText1737.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2537.Add(self.m_staticText1737, 0, wx.ALL, 5)
        self.Barcode82 = wx.TextCtrl(self.Panel8, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode82.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2537.Add(self.Barcode82, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group8.Add(bSizer2537, 1, wx.EXPAND, 5)
        bSizer2547 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1747 = wx.StaticText(self.Panel8, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1747.Wrap(-1)
        self.m_staticText1747.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2547.Add(self.m_staticText1747, 0, wx.ALL, 5)
        self.Process8 = wx.TextCtrl(self.Panel8, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process8.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2547.Add(self.Process8, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group8.Add(bSizer2547, 1, wx.EXPAND, 5)
        self.Gauge8 = wx.Gauge(self.Panel8, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge8.SetValue(98)
        self.Gauge8.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group8.Add(self.Gauge8, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel8.SetSizer(Group8)
        self.Panel8.Layout()
        Group8.Fit(self.Panel8)
        gSizer1.Add(self.Panel8, 1, wx.EXPAND | wx.ALL, 5)
        bSizer3.Add(gSizer1, 0, wx.EXPAND, 0)
        self.m_notebook2 = wx.Notebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_notebook2.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.m_panel1 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer20 = wx.BoxSizer(wx.VERTICAL)
        self.Message1 = wx.TextCtrl(self.m_panel1, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 | wx.TE_LINEWRAP)
        self.Message1.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message1.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizer20.Add(self.Message1, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel1.SetSizer(bSizer20)
        self.m_panel1.Layout()
        bSizer20.Fit(self.m_panel1)
        self.m_notebook2.AddPage(self.m_panel1, u'DUT1 Console Message', True)
        self.m_panel11 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer201 = wx.BoxSizer(wx.VERTICAL)
        self.Message2 = wx.TextCtrl(self.m_panel11, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 | wx.TE_LINEWRAP)
        self.Message2.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message2.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizer201.Add(self.Message2, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel11.SetSizer(bSizer201)
        self.m_panel11.Layout()
        bSizer201.Fit(self.m_panel11)
        self.m_notebook2.AddPage(self.m_panel11, u'DUT2 Console Message', False)
        self.m_panel12 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer202 = wx.BoxSizer(wx.VERTICAL)
        self.Message3 = wx.TextCtrl(self.m_panel12, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 | wx.TE_LINEWRAP)
        self.Message3.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message3.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizer202.Add(self.Message3, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel12.SetSizer(bSizer202)
        self.m_panel12.Layout()
        bSizer202.Fit(self.m_panel12)
        self.m_notebook2.AddPage(self.m_panel12, u'DUT3 Console Message', False)
        self.m_panel13 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer203 = wx.BoxSizer(wx.VERTICAL)
        self.Message4 = wx.TextCtrl(self.m_panel13, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 | wx.TE_LINEWRAP)
        self.Message4.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message4.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizer203.Add(self.Message4, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel13.SetSizer(bSizer203)
        self.m_panel13.Layout()
        bSizer203.Fit(self.m_panel13)
        self.m_notebook2.AddPage(self.m_panel13, u'DUT4 Console Message', False)
        self.m_panel14 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer204 = wx.BoxSizer(wx.VERTICAL)
        self.Message5 = wx.TextCtrl(self.m_panel14, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 | wx.TE_LINEWRAP)
        self.Message5.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message5.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizer204.Add(self.Message5, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel14.SetSizer(bSizer204)
        self.m_panel14.Layout()
        bSizer204.Fit(self.m_panel14)
        self.m_notebook2.AddPage(self.m_panel14, u'DUT5 Console Message', False)
        self.m_panel15 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer205 = wx.BoxSizer(wx.VERTICAL)
        self.Message6 = wx.TextCtrl(self.m_panel15, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 | wx.TE_LINEWRAP)
        self.Message6.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message6.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizer205.Add(self.Message6, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel15.SetSizer(bSizer205)
        self.m_panel15.Layout()
        bSizer205.Fit(self.m_panel15)
        self.m_notebook2.AddPage(self.m_panel15, u'DUT6 Console Message', False)
        self.m_panel16 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer206 = wx.BoxSizer(wx.VERTICAL)
        self.Message7 = wx.TextCtrl(self.m_panel16, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 | wx.TE_LINEWRAP)
        self.Message7.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message7.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizer206.Add(self.Message7, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel16.SetSizer(bSizer206)
        self.m_panel16.Layout()
        bSizer206.Fit(self.m_panel16)
        self.m_notebook2.AddPage(self.m_panel16, u'DUT7 Console Message', False)
        self.m_panel17 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer207 = wx.BoxSizer(wx.VERTICAL)
        self.Message8 = wx.TextCtrl(self.m_panel17, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 | wx.TE_LINEWRAP)
        self.Message8.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message8.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizer207.Add(self.Message8, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel17.SetSizer(bSizer207)
        self.m_panel17.Layout()
        bSizer207.Fit(self.m_panel17)
        self.m_notebook2.AddPage(self.m_panel17, u'DUT8 Console Message', False)
        bSizer3.Add(self.m_notebook2, 1, wx.EXPAND | wx.ALL, 5)
        bSizer1.Add(bSizer3, 1, wx.EXPAND, 5)
        self.SetSizer(bSizer1)
        self.Layout()
        self.StatusBar = self.CreateStatusBar(3, wx.ST_SIZEGRIP, wx.ID_ANY)
        self.StatusBar.SetFont(wx.Font(10, 74, 93, 92, False, 'Arial'))
        self.StatusBar.SetStatusWidths([-1, 100, 150])
        self.Centre(wx.BOTH)
        self.CheckLED_Dialog = CheckMACLED_Dialog(self)
        self.DUT_LIST = ((self.Panel1,
          _Group1,
          self.Barcode11,
          self.Barcode12,
          self.Process1,
          self.Gauge1,
          self.Message1),
         (self.Panel2,
          _Group2,
          self.Barcode21,
          self.Barcode22,
          self.Process2,
          self.Gauge2,
          self.Message2),
         (self.Panel3,
          _Group3,
          self.Barcode31,
          self.Barcode32,
          self.Process3,
          self.Gauge3,
          self.Message3),
         (self.Panel4,
          _Group4,
          self.Barcode41,
          self.Barcode42,
          self.Process4,
          self.Gauge4,
          self.Message4),
         (self.Panel5,
          _Group5,
          self.Barcode51,
          self.Barcode52,
          self.Process5,
          self.Gauge5,
          self.Message5),
         (self.Panel6,
          _Group6,
          self.Barcode61,
          self.Barcode62,
          self.Process6,
          self.Gauge6,
          self.Message6),
         (self.Panel7,
          _Group7,
          self.Barcode71,
          self.Barcode72,
          self.Process7,
          self.Gauge7,
          self.Message7),
         (self.Panel8,
          _Group8,
          self.Barcode81,
          self.Barcode82,
          self.Process8,
          self.Gauge8,
          self.Message8))
        dutid = 1
        for dut in self.DUT_LIST:
            dut[0].SetBackgroundColour(self.DisableColor)
            dut[0].SetToolTipString('')
            dut[1].SetLabel('DUT %s' % dutid)
            dut[2].SetValue('')
            dut[3].SetValue('')
            dut[4].SetValue('')
            dut[5].SetValue(0)
            dut[6].SetBackgroundColour(wx.BLACK)
            dut[6].SetForegroundColour(wx.WHITE)
            dut[2].SetBackgroundColour(wx.Colour(245, 245, 245))
            dut[3].SetBackgroundColour(wx.Colour(245, 245, 245))
            dut[4].SetBackgroundColour(wx.Colour(245, 245, 245))
            dutid += 1

        self.StatusBar.SetStatusText('Welcome to AFI test program')
        self.emp = '00000'
        self.pn = ''
        if len(sys.argv) > 1:
            self.pn = sys.argv[1]
        self.ScanLabel.SetFocus()
        self.ScanLabel.SelectAll()
        self.ConfigStateMachine = StateMachine(self)
        self.InitStr = 'Read config and initialization'
        self.TFTPServer = ''
        self.configfile = ''
        self.Process.Enable(False)
        self.Repair.Enable(False)
        self.m_calibration.Enable(False)
        self.repair = TransferRepair(self)
        self.flash = [wx.Colour(215, 80, 159),
         wx.Colour(99, 0, 255),
         wx.Colour(0, 255, 255),
         wx.Colour(204, 204, 0),
         self.YellowColor]
        self.Buffers = [[Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self)], [Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self)]]
        self.Buffers[0][0].CardName = 'CCU'
        self.Buffers[1][0].CardName = 'CCU'
        for i in range(8, 16):
            self.Buffers[0][i].CardName = 'DUT\t%s' % (i - 8)
            self.Buffers[1][i].CardName = 'DUT\t%s' % (i - 8)

        wx.FutureCall(500, self._Status_Thread)
        wx.FutureCall(500, self._TFTPTransfer_Thread)
        wx.FutureCall(500, self._Result_Panel_Thread)
        self.ScanLabel.Bind(wx.EVT_TEXT_ENTER, self.ScanLabelEnter)
        self.Bind(wx.EVT_MENU, self.Show_L_CCU, id=self.M_L_CCU.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S0, id=self.M_L_S0.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S1, id=self.M_L_S1.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S2, id=self.M_L_S2.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S3, id=self.M_L_S3.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S4, id=self.M_L_S4.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S5, id=self.M_L_S5.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S6, id=self.M_L_S6.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT0, id=self.M_L_DUT0.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT1, id=self.M_L_DUT1.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT2, id=self.M_L_DUT2.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT3, id=self.M_L_DUT3.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT4, id=self.M_L_DUT4.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT5, id=self.M_L_DUT5.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT6, id=self.M_L_DUT6.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT7, id=self.M_L_DUT7.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_CCU, id=self.M_R_CCU.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S0, id=self.M_R_S0.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S1, id=self.M_R_S1.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S2, id=self.M_R_S2.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S3, id=self.M_R_S3.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S4, id=self.M_R_S4.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S5, id=self.M_R_S5.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S6, id=self.M_R_S6.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT0, id=self.M_R_DUT0.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT1, id=self.M_R_DUT1.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT2, id=self.M_R_DUT2.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT3, id=self.M_R_DUT3.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT4, id=self.M_R_DUT4.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT5, id=self.M_R_DUT5.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT6, id=self.M_R_DUT6.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT7, id=self.M_R_DUT7.GetId())
        self.Bind(wx.EVT_MENU, self.Debug_Message, self.Process)
        self.Bind(wx.EVT_MENU, self.show_help_message, self.About)
        self.Bind(wx.EVT_MENU, self.show_repair, self.Repair)
        self.Bind(wx.EVT_MENU, self.calibration, self.m_calibration)

    def calibration(self, event):
        if self.configfile:
            CalibrationGUI.AFI_Clibration_Tools(self).Show(True)
        else:
            dlgmsg = wx.MessageDialog(self, 'Please selection model', 'Infomation', wx.OK | wx.ICON_INFORMATION)
            dlgmsg.Center()
            dlgmsg.ShowModal()
            dlgmsg.Destroy()

    def show_help_message(self, event):
        HelpGui(self).Show(True)

    def show_repair(self, event):
        self.repair.ShowModal()

    def Debug_Message(self, event):
        dlgtext = wx.TextEntryDialog(self, 'Please input password :', 'Password', '')
        if dlgtext.ShowModal() == wx.ID_OK:
            if dlgtext.GetValue() != 'hitron':
                dlgmsg = wx.MessageDialog(self, 'Input password error', 'Password', wx.OK | wx.ICON_ERROR)
                dlgmsg.Center()
                dlgmsg.ShowModal()
                dlgmsg.Destroy()
            elif self.configfile:
                ProcessGui(self).Show(True)
            else:
                dlgmsg = wx.MessageDialog(self, 'Please selection model', 'Infomation', wx.OK | wx.ICON_INFORMATION)
                dlgmsg.Center()
                dlgmsg.ShowModal()
                dlgmsg.Destroy()
        dlgtext.Destroy()

    def Show_L_CCU(self, event):
        self.Buffers[0][0].Show(True)
        self.Buffers[0][0].SetTitle('Left CCU Console - Socket 9')

    def Show_L_S0(self, event):
        self.Buffers[0][1].SetTitle('Left S0 Console - Socket 8')
        self.Buffers[0][1].Show(True)

    def Show_L_S1(self, event):
        self.Buffers[0][2].SetTitle('Left S1 Console - Socket 7')
        self.Buffers[0][2].Show(True)

    def Show_L_S2(self, event):
        self.Buffers[0][3].SetTitle('Left S2 Console - Socket 6')
        self.Buffers[0][3].Show(True)

    def Show_L_S3(self, event):
        self.Buffers[0][4].SetTitle('Left S3 Console - Socket 5')
        self.Buffers[0][4].Show(True)

    def Show_L_S4(self, event):
        self.Buffers[0][5].SetTitle('Left S4 Console - Socket 4')
        self.Buffers[0][5].Show(True)

    def Show_L_S5(self, event):
        self.Buffers[0][6].SetTitle('Left S5 Console - Socket 3')
        self.Buffers[0][6].Show(True)

    def Show_L_S6(self, event):
        self.Buffers[0][7].SetTitle('Left S6 Console - Socket 2')
        self.Buffers[0][7].Show(True)

    def Show_L_DUT0(self, event):
        self.Buffers[0][8].SetTitle('Left DUT 0 Console - Socket 9')
        self.Buffers[0][8].Show(True)

    def Show_L_DUT1(self, event):
        self.Buffers[0][9].SetTitle('Left DUT 1 Console - Socket 9')
        self.Buffers[0][9].Show(True)

    def Show_L_DUT2(self, event):
        self.Buffers[0][10].SetTitle('Left DUT 2 Console - Socket 9')
        self.Buffers[0][10].Show(True)

    def Show_L_DUT3(self, event):
        self.Buffers[0][11].SetTitle('Left DUT 3 Console - Socket 9')
        self.Buffers[0][11].Show(True)

    def Show_L_DUT4(self, event):
        self.Buffers[0][12].SetTitle('Left DUT 4 Console - Socket 9')
        self.Buffers[0][12].Show(True)

    def Show_L_DUT5(self, event):
        self.Buffers[0][13].SetTitle('Left DUT 5 Console - Socket 9')
        self.Buffers[0][13].Show(True)

    def Show_L_DUT6(self, event):
        self.Buffers[0][14].SetTitle('Left DUT 6 Console - Socket 9')
        self.Buffers[0][14].Show(True)

    def Show_L_DUT7(self, event):
        self.Buffers[0][15].SetTitle('Left DUT 7 Console - Socket 9')
        self.Buffers[0][15].Show(True)

    def Show_R_CCU(self, event):
        self.Buffers[1][0].SetTitle('Right CCU Console - Socket 10')
        self.Buffers[1][0].Show(True)

    def Show_R_S0(self, event):
        self.Buffers[1][1].SetTitle('Right S0 Console - Socket 11')
        self.Buffers[1][1].Show(True)

    def Show_R_S1(self, event):
        self.Buffers[1][2].SetTitle('Right S1 Console - Socket 12')
        self.Buffers[1][2].Show(True)

    def Show_R_S2(self, event):
        self.Buffers[1][3].SetTitle('Right S2 Console - Socket 13')
        self.Buffers[1][3].Show(True)

    def Show_R_S3(self, event):
        self.Buffers[1][4].SetTitle('Right S3 Console - Socket 14')
        self.Buffers[1][4].Show(True)

    def Show_R_S4(self, event):
        self.Buffers[1][5].SetTitle('Right S4 Console - Socket 15')
        self.Buffers[1][5].Show(True)

    def Show_R_S5(self, event):
        self.Buffers[1][6].SetTitle('Right S5 Console - Socket 16')
        self.Buffers[1][6].Show(True)

    def Show_R_S6(self, event):
        self.Buffers[1][7].SetTitle('Right S6 Console - Socket 17')
        self.Buffers[1][7].Show(True)

    def Show_R_DUT0(self, event):
        self.Buffers[1][8].SetTitle('Right DUT 0 Console - Socket 10')
        self.Buffers[1][8].Show(True)

    def Show_R_DUT1(self, event):
        self.Buffers[1][9].SetTitle('Right DUT 1 Console - Socket 10')
        self.Buffers[1][9].Show(True)

    def Show_R_DUT2(self, event):
        self.Buffers[1][10].SetTitle('Right DUT 2 Console - Socket 10')
        self.Buffers[1][10].Show(True)

    def Show_R_DUT3(self, event):
        self.Buffers[1][11].SetTitle('Right DUT 3 Console - Socket 10')
        self.Buffers[1][11].Show(True)

    def Show_R_DUT4(self, event):
        self.Buffers[1][12].SetTitle('Right DUT 4 Console - Socket 10')
        self.Buffers[1][12].Show(True)

    def Show_R_DUT5(self, event):
        self.Buffers[1][13].SetTitle('Right DUT 5 Console - Socket 10')
        self.Buffers[1][13].Show(True)

    def Show_R_DUT6(self, event):
        self.Buffers[1][14].SetTitle('Right DUT 6 Console - Socket 10')
        self.Buffers[1][14].Show(True)

    def Show_R_DUT7(self, event):
        self.Buffers[1][15].SetTitle('Right DUT 7 Console - Socket 10')
        self.Buffers[1][15].Show(True)

    def _Result_Panel_Thread(self):
        for i in range(8):
            if self.DUT_LIST[i][0].GetLabel() == '3':
                if self.DUT_LIST[i][0].GetBackgroundColour() == self.YellowColor:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.flash[0])
                    self.DUT_LIST[i][0].Refresh()
                else:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.YellowColor)
                    self.DUT_LIST[i][0].Refresh()
            if self.DUT_LIST[i][0].GetLabel() == '4':
                if self.DUT_LIST[i][0].GetBackgroundColour() == self.YellowColor:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.flash[1])
                    self.DUT_LIST[i][0].Refresh()
                else:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.YellowColor)
                    self.DUT_LIST[i][0].Refresh()
            if self.DUT_LIST[i][0].GetLabel() == '5':
                if self.DUT_LIST[i][0].GetBackgroundColour() == self.YellowColor:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.flash[2])
                    self.DUT_LIST[i][0].Refresh()
                else:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.YellowColor)
                    self.DUT_LIST[i][0].Refresh()
            if self.DUT_LIST[i][0].GetLabel() == '6':
                if self.DUT_LIST[i][0].GetBackgroundColour() == self.YellowColor:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.flash[3])
                    self.DUT_LIST[i][0].Refresh()
                else:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.YellowColor)
                    self.DUT_LIST[i][0].Refresh()

        wx.FutureCall(500, self._Result_Panel_Thread)

    def _TFTPTransfer_Thread(self):
        if type(self.TFTPServer) != type(''):
            msg = self.TFTPServer.text.split('\n')[-1]
            if msg:
                self.TFTPTransferFile.SetLabel(msg)
        wx.FutureCall(500, self._TFTPTransfer_Thread)

    def _Status_Thread(self):
        if self.LabelName.GetLabel() == 'PN' and self.pn:
            self.ScanLabel.SetValue(self.pn)
            self.ScanLabelEnter('enter')
        if self.LabelName.GetLabel() == 'INIT':
            StatusBarStr = self.StatusBar.GetStatusText()
            if len(StatusBarStr) < len(self.InitStr) + 7:
                self.StatusBar.SetStatusText(StatusBarStr + '.')
            else:
                self.StatusBar.SetStatusText(self.InitStr)
        if type(self.TFTPServer) != type(''):
            if self.TFTPServer.serving:
                if self.TFTPServerState.GetBackgroundColour() == self.YellowColor:
                    self.TFTPServerState.SetBackgroundColour('blue')
                else:
                    self.TFTPServerState.SetBackgroundColour('yellow')
            else:
                self.TFTPServerState.SetBackgroundColour('red')
        self.TFTPServerState.Refresh()
        wx.FutureCall(500, self._Status_Thread)

    def ScanLabelEnter(self, event):
        self.ConfigStateMachine.state(self.LabelName.GetLabel())
        self.ScanLabel.SetFocus()
        self.ScanLabel.SelectAll()
        self.ScanLabel.Refresh()


class HelpGui(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title='About ', pos=wx.DefaultPosition, size=wx.Size(634, 481), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)
        self.about = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS)
        bSizer2.Add(self.about, 1, wx.EXPAND | wx.ALL, 5)
        self.SetSizer(bSizer2)
        self.Layout()
        self.Centre(wx.BOTH)
        self.about.LoadFile('C:\\AFIMainGui\\Randme.txt')

    def __del__(self):
        pass


class ProcessGui(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition, size=wx.Size(346, 300), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)
        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer2.AddButton(self.m_sdbSizer2OK)
        self.m_sdbSizer2Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer2.AddButton(self.m_sdbSizer2Cancel)
        m_sdbSizer2.Realize()
        bSizer1.Add(m_sdbSizer2, 0, 0, 5)
        self.m_scrolledWindow1 = wx.ScrolledWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL | wx.VSCROLL)
        self.m_scrolledWindow1.SetScrollRate(5, 5)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)
        self.checkboxs = []
        self.configfile = parent.configfile
        self.ConfigFile = ConfigParser.SafeConfigParser()
        if self.ConfigFile.read(self.configfile):
            self.Flows = map(strip, self.ConfigFile.get('Base', 'Flows').split(','))
            for flow in self.Flows:
                fname = self.ConfigFile.get(flow, 'FlowName')
                fenable = self.ConfigFile.get(flow, 'Enable').strip()
                self.checkboxs.append(wx.CheckBox(self.m_scrolledWindow1, wx.ID_ANY, fname, wx.DefaultPosition, wx.DefaultSize, 0))
                bSizer2.Add(self.checkboxs[-1], 0, wx.ALL, 5)
                self.checkboxs[-1].SetValue(int(fenable))

        self.m_scrolledWindow1.SetSizer(bSizer2)
        self.m_scrolledWindow1.Layout()
        bSizer2.Fit(self.m_scrolledWindow1)
        bSizer1.Add(self.m_scrolledWindow1, 1, wx.EXPAND | wx.ALL, 5)
        self.SetSizer(bSizer1)
        self.Layout()
        self.Centre(wx.BOTH)
        self.m_sdbSizer2OK.Bind(wx.EVT_BUTTON, self.save)
        self.m_sdbSizer2Cancel.Bind(wx.EVT_BUTTON, self.exit)

    def __del__(self):
        pass

    def save(self, event):
        try:
            i = 0
            for flow in self.Flows:
                val = '0'
                if self.checkboxs[i].GetValue():
                    val = '1'
                self.ConfigFile.set(flow, 'Enable', val)
                i += 1

            fp = open(self.configfile, 'w')
            self.ConfigFile.write(fp)
            fp.close()
            dlgmsg = wx.MessageDialog(self, 'Config set success', 'Infomation', wx.OK | wx.ICON_INFORMATION)
            dlgmsg.Center()
            dlgmsg.ShowModal()
            dlgmsg.Destroy()
        except:
            dlgmsg = wx.MessageDialog(self, 'Config set failed', 'Infomation', wx.OK | wx.ICON_ERROR)
            dlgmsg.Center()
            dlgmsg.ShowModal()
            dlgmsg.Destroy()

    def exit(self, event):
        self.Show(False)


class CheckMACLED_Dialog(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'Check LED & MAC', pos=wx.DefaultPosition, size=wx.Size(224, 160), style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)
        self.DUT = wx.StaticText(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE)
        self.DUT.Wrap(-1)
        self.DUT.SetFont(wx.Font(28, 74, 90, 92, False, 'Arial Black'))
        self.DUT.SetForegroundColour(wx.Colour(0, 0, 255))
        self.DUT.SetBackgroundColour(wx.Colour(85, 141, 255))
        bSizer1.Add(self.DUT, 0, wx.ALL | wx.EXPAND, 5)
        self.LabelName = wx.StaticText(self, wx.ID_ANY, u'SCAN MAC', wx.DefaultPosition, wx.DefaultSize, 0)
        self.LabelName.Wrap(-1)
        self.LabelName.SetFont(wx.Font(10, 74, 90, 90, False, 'Arial'))
        bSizer1.Add(self.LabelName, 0, wx.ALL | wx.EXPAND, 5)
        self.ScanLabel = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(200, -1), wx.TE_NO_VSCROLL | wx.TE_PROCESS_ENTER | wx.TE_PROCESS_TAB)
        self.ScanLabel.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        bSizer1.Add(self.ScanLabel, 0, wx.ALL | wx.EXPAND, 5)
        self.SetSizer(bSizer1)
        self.Layout()
        self.Label_ = ''
        self.Centre(wx.BOTH)
        self.ScanLabel.SetFocus()
        self.ScanLabel.SelectAll()
        self.ScanLabel.Bind(wx.EVT_TEXT_ENTER, self.Scaned)

    def __del__(self):
        pass

    def Scaned(self, event):
        self.Label_ = self.ScanLabel.GetValue()
        self.ScanLabel.SetFocus()
        self.ScanLabel.SelectAll()
        self.ScanLabel.Refresh()


class TransferRepair(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'To Repair', pos=wx.DefaultPosition, size=wx.Size(379, 123), style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)
        bSizer2.AddSpacer((0, 0), 1, wx.EXPAND, 5)
        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY, u'DUT ID', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        self.m_staticText2.SetFont(wx.Font(10, 74, 93, 92, False, 'Arial'))
        bSizer4.Add(self.m_staticText2, 0, wx.ALL, 5)
        dutidChoices = [u'DUT 1',
         u'DUT 2',
         u'DUT 3',
         u'DUT 4',
         u'DUT 5',
         u'DUT 6',
         u'DUT 7',
         u'DUT 8']
        self.dutid = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(60, -1), dutidChoices, 0)
        self.dutid.SetSelection(0)
        self.dutid.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer4.Add(self.dutid, 0, wx.ALL, 5)
        self.m_staticText3 = wx.StaticText(self, wx.ID_ANY, u'MAC', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        self.m_staticText3.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        bSizer4.Add(self.m_staticText3, 0, wx.ALL, 5)
        self.mac = wx.TextCtrl(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.Size(110, -1), style=wx.TE_PROCESS_ENTER)
        self.mac.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        bSizer4.Add(self.mac, 0, wx.ALL, 5)
        self.m_staticText5 = wx.StaticText(self, wx.ID_ANY, u'-', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText5.Wrap(-1)
        self.m_staticText5.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        bSizer4.Add(self.m_staticText5, 0, wx.ALL, 5)
        self.errcode = wx.TextCtrl(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.Size(60, -1), style=wx.TE_PROCESS_ENTER)
        self.errcode.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        bSizer4.Add(self.errcode, 0, wx.ALL, 5)
        bSizer2.Add(bSizer4, 1, wx.EXPAND, 5)
        bSizer2.AddSpacer((0, 0), 1, wx.EXPAND, 5)
        self.SetSizer(bSizer2)
        self.Layout()
        self.Centre(wx.BOTH)
        self.dutid.Bind(wx.EVT_CHOICE, self.dutchoice)
        self.mac.Bind(wx.EVT_TEXT_ENTER, self.MACEnter)
        self.errcode.Bind(wx.EVT_TEXT_ENTER, self.ErrCodeEnter)
        self.parent = parent
        self.mac.SetFocus()
        self.mac.SelectAll()

    def tr(self):
        if self.parent.StatusBar.GetStatusText(2) == 'MES Offline':
            dlg = wx.MessageDialog(self, 'MES Off line', 'MES Status', wx.OK | wx.ICON_INFORMATION)
            dlg.Center()
            dlg.ShowModal()
            dlg.Destroy()
            return
        dlgmsg = wx.MessageDialog(self, 'Confirm to repair ?', 'Repair', wx.YES_NO | wx.ICON_INFORMATION)
        dlgmsg.Center()
        flag = dlgmsg.ShowModal()
        dlgmsg.Destroy()
        if flag == 5103:
            msg = failtravel(self.parent.emp, self.mac.GetValue(), self.errcode.GetValue())
            dlg = wx.MessageDialog(self, msg[-1], 'To repair', wx.OK | wx.ICON_INFORMATION)
            dlg.Center()
            dlg.ShowModal()
            dlg.Destroy()
            self.mac.SetFocus()

    def dutchoice(self, evt):
        self.mac.SetValue(self.parent.DUT_LIST[self.dutid.GetSelection()][2].GetValue())
        self.errcode.SetValue(self.parent.DUT_LIST[self.dutid.GetSelection()][1].GetLabel().split()[-1])
        self.tr()

    def MACEnter(self, evt):
        self.errcode.SetFocus()
        self.errcode.SelectAll()

    def ErrCodeEnter(self, evt):
        self.tr()


class App(wx.App):

    def OnInit(self):
        try:
            wx.InitAllImageHandlers()
            self.main = MainGUI(None)
            self.main.Show(True)
            self.SetTopWindow(self.main)
        except Exception as e:
            print e

        return True


def main():
    application = App(0)
    application.MainLoop()


if __name__ == '__main__':
    main()