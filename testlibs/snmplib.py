# Embedded file name: testlibs\snmplib.pyc
import os, Snmp
from toolslib import *
ifPhysAddress2_OID = '.1.3.6.1.2.1.2.2.1.6.2'
docsDevServerBootState_OID = '.1.3.6.1.2.1.69.1.4.1'
modemProdResetAccessStart_OID = '.1.3.6.1.4.1.8595.1.400.2.1.1.8.0'
modemCmTelnetAccessEnable_OID = '.1.3.6.1.4.1.8595.1.400.3.1.9.0'
modemProdCommandLine_OID = '.1.3.6.1.4.1.8595.1.400.4.1.9.0'
modemProdSwBank_OID = '1.3.6.1.4.1.8595.1.400.2.1.2.40.5.1.1.0'
docsIfCmStatusValue_OID = '.1.3.6.1.2.1.10.127.1.2.2.1.1.2'
docsIfSigQSignalNoise_OID = '.1.3.6.1.2.1.10.127.1.1.4.1.5.3'
docsIfDownChannelPower_OID = '.1.3.6.1.2.1.10.127.1.1.1.1.6.3'
docsIfCmStatusTxPower_OID = '.1.3.6.1.2.1.10.127.1.2.2.1.3.2'
sysDescr_OID = '1.3.6.1.2.1.1.1.0'

def MacTrans(mac):
    return '%02X%02X%02X%02X%02X%02X' % (ord(mac[0]),
     ord(mac[1]),
     ord(mac[2]),
     ord(mac[3]),
     ord(mac[4]),
     ord(mac[5]))


def WaitingSnmpReady(ipaddr, timeout):
    os.system('arp -d')
    i = 0
    print 'Waiting Snmp ready...'
    while i < timeout:
        try:
            Snmp.SnmpGet(ipaddr, ifPhysAddress2_OID).values()
            flag = 1
        except Exception as e:
            print e.message
            flag = 0

        if flag:
            return 1
        i += 1
        time.sleep(1)

    return 0


def CheckSnmpMACSN(*argv):
    """
     argv :
         dutid,terms,labels,Panel,Log,Config,flow,[Return])
         terms : ccu , cb , sw , vm ,dut 
    """
    mac = argv[2][0]
    sn = argv[2][1]
    term = argv[1][-1]
    log = argv[-4]
    ipaddr = GetWanIP(term)
    timeout = int(argv[-3](argv[-2], 'timeout'))
    if not WaitingSnmpReady(ipaddr, timeout):
        raise Except('ErrorCode(0007):Waiting Snmp ready Failed!!')
    for try_ in range(3):
        try:
            mac_addr = MacTrans(Snmp.SnmpGet(ipaddr, ifPhysAddress2_OID).values()[0])
            break
        except:
            mac_addr = ''

    if len(mac_addr) != 12:
        raise Except("ErrorCode(414043):Can't get mac address for snmp")
    if mac.upper() != mac_addr:
        raise Except('ErrorCode(E00164):Snmp Get MAC address compare error %s (%s)' % (mac_addr, mac))
    for try_ in range(3):
        try:
            serialNumber = Xurl('snmp://%s/private/DOCS-CABLE-DEVICE-MIB/docsDevSerialNumber.0/s' % ipaddr).get()
        except:
            serialNumber = ''

        if sn.upper() == serialNumber:
            break

    if sn.upper() != serialNumber:
        raise Except('ErrorCode(214077):Snmp Get Serial Number compare error %s (%s)' % (serialNumber, sn))
    msg = 'Snmp get SerialNumber: %s (%s)\nSnmp get Macaddress: %s (%s)' % (serialNumber,
     sn,
     mac_addr,
     mac)
    log(msg, 2)


def lPrintSysDescr(*argv):
    """
     argv :
         dutid,terms,labels,Panel,Log,Config,flow,[Return])
         terms : ccu , cb , sw , vm ,dut 
    """
    mac = argv[2][0]
    sn = argv[2][1]
    term = argv[1][-1]
    log = argv[-4]
    ipaddr = GetWanIP(term)
    sysdesc = argv[-3]('Base', 'sysdesc')
    value = result = ''
    for try_ in range(3):
        try:
            value = result = Snmp.SnmpGet(ipaddr, sysDescr_OID).values()[0]
            break
        except:
            if try_ == 2:
                raise Except('ErrorCode(214036):Snmp get failed : No response')

    if value:
        msg = 'DOCSIS Version:      '
        msg = msg + result[:result.find('<<')].strip() + '\n'
        result = result[result.find('<<') + 2:result.find('>>')].split(';')
        msg = msg + 'Hardware Version:    '
        msg = msg + result[0].split(':')[1].strip() + '\n'
        msg = msg + 'Vendor:              '
        msg = msg + result[1].split(':')[1].strip() + '\n'
        msg = msg + 'BootLoader Version:  '
        msg = msg + result[2].split(':')[1].strip() + '\n'
        msg = msg + 'Software Version:    '
        msg = msg + result[3].split(':')[1].strip() + '\n'
        msg = msg + 'Model Name:          '
        msg = msg + result[4].split(':')[1].strip()
        log(msg)
    if value != sysdesc:
        raise Except('ErrorCode(214036):SNMP sysDescr.0 incorrect!')
    log('SNMP sysDescr check pass', 2)