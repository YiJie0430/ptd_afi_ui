# Embedded file name: tftp\tftp_engine.pyc
"""
tftp_engine.py - runs the TFTP server for TFTPgui

Normally imported by tftpgui.py which creates an instance
of the ServerState class defined here, the instance holds
the ip address and port to bind to.

tftpgui.py then calls either:
loop_nogui(server)
or
loop(server)

Both create a loop, calling the poll() method of the ServerState
instance 'server', however loop_nogui exits if unable to bind to
the port whereas loop(server) is intended to run with a gui in
another thread, and keeps the loop working, so the user has the
option to change port parameters.
"""
import os, time, asyncore, socket, logging, logging.handlers, string
import ipv4

def create_logger(logfolder):
    """Create logger, return rootLogger on success, None on failure"""
    if not logfolder:
        return None
    try:
        rootLogger = logging.getLogger('')
        rootLogger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        logfile = os.path.join(logfolder, 'tftplog')
        loghandler = logging.handlers.RotatingFileHandler(logfile, maxBytes=20000, backupCount=5)
        loghandler.setFormatter(formatter)
        rootLogger.addHandler(loghandler)
    except Exception:
        return None

    return rootLogger


class DropPacket(Exception):
    """Raised to flag the packet should be dropped"""
    pass


class ServerState(object):
    """Defines a class which records the current server state
    and produces logs, and a text attribute for a gui"""

    def __init__(self, **cfgdict):
        """Creates a class which defines the state of the server
        cfgdict is a dictionary read from the config file
          tftprootfolder  - path to a folder
          logfolder       - path to a folder
          anyclient       - 1 if any client can call, 0 if only from a specific subnet
          clientipaddress - specific subnet ip address of the client
          clientmask      - specific subnet mask of the client
          listenport      - tftp port to listen on
          listenipaddress - address to listen on"""
        self.serving = False
        self._serving = False
        self.tftp_server = None
        self._engine_available = True
        self.logging_enabled = False
        self.break_loop = False
        raise self.set_from_config_dict(cfgdict) or AssertionError
        self._connections = {}
        self.text = 'TFTPgui - a free tftp Server\n\nVersion\t:  TFTPgui 2.2\nAuthor\t:  Bernard Czenkusz\nWeb site\t:  www.skipole.co.uk\nLicense\t:  GPLv3\n\nPress Start to enable the tftp server\n'
        return

    def log_exception(self, e):
        """Used to log exceptions"""
        if self.logging_enabled:
            try:
                logging.exception(e)
            except Exception:
                self.logging_enabled = False

    def add_text(self, text_line, clear = False):
        """Adds text_line to the log, and also to self.text,
        which is used by the gui interface - adds the line to
        the text, keeping a maximum of 12 lines.
        If clear is True, deletes previous lines, making text
        equal to this text_line only"""
        if len(text_line) > 100:
            text_line = text_line[:100]
        text_line = ''.join([ char for char in text_line if char in string.printable ])
        if self.logging_enabled:
            try:
                logging.info(text_line)
            except Exception:
                self.logging_enabled = False

        if clear:
            self.text = text_line
            return
        text_list = self.text.splitlines()
        if not text_list:
            self.text = text_line
            return
        if len(text_list) > 12:
            text_list.pop(0)
        text_list.append(text_line)
        self.text = '\n'.join(text_list)

    def __len__(self):
        """Returns the number of connections"""
        return len(self._connections)

    def __getitem__(self, rx_addr):
        """Returns the connection with the given rx_addr"""
        if rx_addr not in self._connections:
            raise IndexError
        return self._connections[rx_addr]

    def __contains__(self, rx_addr):
        """Retrurns True if the rx_addr is associated with a connection"""
        if rx_addr in self._connections:
            return True
        else:
            return None
        return None

    def del_connection(self, connection):
        """Deletes the given connection from the _connections dictionary"""
        if connection.rx_addr not in self._connections:
            return
        del self._connections[connection.rx_addr]

    def clear_all_connections(self):
        """Clears all connections from the connection list"""
        connections_list = self.get_connections_list()
        for connection in connections_list:
            connection.shutdown()

        self._connections = {}

    def get_connections_list(self):
        """Returns a list of current connection objects"""
        return list(self._connections.values())

    def create_connection(self, rx_data, rx_addr):
        """Creates either a ReceiveData or SendData connection object
        and adds it to dictionary"""
        if rx_addr in self._connections:
            raise DropPacket
        if rx_data[0] != '\x00':
            raise DropPacket
        if rx_data[1] == '\x01':
            connection = SendData(self, rx_data, rx_addr)
        elif rx_data[1] == '\x02':
            connection = ReceiveData(self, rx_data, rx_addr)
        else:
            raise DropPacket
        self._connections[rx_addr] = connection

    def get_config_dict(self):
        """Returns a dictionary of the config attributes"""
        cfgdict = {'tftprootfolder': self.tftprootfolder,
         'logfolder': self.logfolder,
         'anyclient': self.anyclient,
         'clientipaddress': self.clientipaddress,
         'clientmask': self.clientmask,
         'listenport': self.listenport,
         'listenipaddress': self.listenipaddress}
        return cfgdict

    def set_from_config_dict(self, cfgdict):
        """Sets attributes from a given dictionary
        Returns True if all attributes supplied, or False if not"""
        if not not self._serving:
            raise AssertionError
            if not not self.serving:
                raise AssertionError
                all_attributes = True
                if 'logfolder' in cfgdict:
                    self.logfolder = cfgdict['logfolder']
                else:
                    all_attributes = False
                if 'tftprootfolder' in cfgdict:
                    self.tftprootfolder = cfgdict['tftprootfolder']
                else:
                    all_attributes = False
                if 'anyclient' in cfgdict:
                    self.anyclient = cfgdict['anyclient']
                else:
                    all_attributes = False
                if 'clientipaddress' in cfgdict:
                    self.clientipaddress = cfgdict['clientipaddress']
                else:
                    all_attributes = False
                if 'clientmask' in cfgdict:
                    self.clientmask = cfgdict['clientmask']
                else:
                    all_attributes = False
                if 'listenport' in cfgdict:
                    self.listenport = cfgdict['listenport']
                self.listenipaddress = 'listenipaddress' in cfgdict and cfgdict['listenipaddress'] == '0.0.0.0' and ''
            else:
                self.listenipaddress = cfgdict['listenipaddress']
        else:
            all_attributes = False
        return all_attributes

    def shutdown(self):
        """Shuts down the server"""
        if not self._engine_available:
            return
        self.stop_serving()
        self.add_text('TFTPgui application stopped')
        self._engine_available = False

    def start_serving(self):
        """Starts the server serving"""
        if not self._engine_available:
            return
        if self._serving:
            self.serving = True
            return
        try:
            self.tftp_server = TFTPserver(self)
        except Exception:
            self.stop_serving()
            raise

        self._serving = True
        self.serving = True
        if self.listenipaddress:
            self.add_text('Listenning on %s:%s' % (self.listenipaddress, self.listenport), clear=True)
        else:
            self.add_text('Listenning on port %s' % self.listenport, clear=True)

    def stop_serving(self):
        """Stops the server serving"""
        if self.tftp_server != None:
            self.tftp_server.close()
            self.tftp_server = None
            self.add_text('Server stopped')
        self.clear_all_connections()
        self._serving = False
        self.serving = False
        return

    def poll(self):
        """Polls asyncore if serving,
        checks the attribute self.serving, turning on listenning
        if True, or off if false"""
        if not self._engine_available:
            return
        if self._serving:
            if not self.serving:
                self.stop_serving()
                return
            asyncore.poll()
            connection_list = self.get_connections_list()
            for connection in connection_list:
                connection.poll()
                asyncore.poll()

            return
        if self.serving:
            self.start_serving()

    def get_engine_available(self):
        """returns the value af self._engine_available"""
        return self._engine_available

    engine_available = property(get_engine_available)


class STOPWATCH_ERROR(Exception):
    """time_it should only be called if start has been called first."""
    pass


class Stopwatch(object):
    """stopwatch class calculates the TTL - the time to live in seconds
    
    The start() method should be called, each time a packet is transmitted
    which expects a reply, and then the time_it() method should be called
    periodically while waiting for the reply.
    If  time_it() returns True, then the time is still within the TTL - 
    so carry on waiting.
    If time_it() returns False, then the TTL has expired and the calling
    program needs to do something about it.
    When a packet is received, the calling program should call the
    stop() method - this then calculates the average round trip
    time (aveRTT), and a TTL of three times the aveRTT.
    TTL is  a minimum of 0.5 secs, and a maximum of 5 seconds.
    Methods: 
      start() to start  the stopwatch
      stop() to stop the stopwatch, and update aveRTT and TTL
      time_it() return True if the time between start and time_it is less than TTL
      return False if it is greater
    Exceptions:
        STOPWATCH_ERROR is raised by time_it() if is called without
        start() being called first - as the stopwatch must be running
        for the time_it measurement to have any validity
      """

    def __init__(self):
        self.RTTcount = 1
        self.TotalRTT = 0.5
        self.aveRTT = 0.5
        self.TTL = 1.5
        self.rightnow = 0.0
        self.started = False

    def start(self):
        self.rightnow = time.time()
        self.started = True

    def stop(self):
        if not self.started:
            return
        RTT = time.time() - self.rightnow
        if RTT == 0.0:
            RTT = 0.5
        RTT = min(3.0, RTT)
        RTT = max(0.01, RTT)
        self.TotalRTT += RTT
        self.RTTcount += 1
        self.aveRTT = self.TotalRTT / self.RTTcount
        if self.RTTcount > 20:
            self.TotalRTT = 5.0 * self.aveRTT
            self.RTTcount = 5
        if self.aveRTT > 2.0:
            self.TotalRTT = 10.0
            self.RTTcount = 5
            self.aveRTT = 2.0
        self.TTL = 3.0 * self.aveRTT
        self.TTL = min(5.0, self.TTL)
        self.TTL = max(0.5, self.TTL)
        self.started = False

    def time_it(self):
        """Called to check time is within TTL, if it is, return True
        If not, started attribute is set to False, and returns False"""
        if not self.started:
            raise STOPWATCH_ERROR
        deltatime = time.time() - self.rightnow
        if deltatime <= self.TTL:
            return True
        self.aveRTT += 0.5
        self.aveRTT = min(2.0, self.aveRTT)
        self.TotalRTT = 5.0 * self.aveRTT
        self.RTTcount = 5
        self.TTL = 3.0 * self.aveRTT
        self.TTL = min(5.0, self.TTL)
        self.TTL = max(0.5, self.TTL)
        self.started = False
        return False


class NoService(Exception):
    """Raised to flag the service is unavailable"""
    pass


class TFTPserver(asyncore.dispatcher):
    """Class for binding the tftp listenning socket
    asyncore.poll will call the handle_read method whenever data is
    available to be read, and handle_write to see if data is to be transmitted"""

    def __init__(self, server):
        """Bind the tftp listener to the address given in server.listenipaddress
        and port given in server.listenport"""
        asyncore.dispatcher.__init__(self)
        self.server = server
        self.create_socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.connection_list = []
        self.connection = None
        try:
            self.bind((server.listenipaddress, server.listenport))
        except Exception as e:
            server.log_exception(e)
            if server.listenipaddress:
                server.text = "Failed to bind to %s : %s\nPossible reasons:\nCheck this IP address exists on this server.\n(Try with 0.0.0.0 set as the 'listenipaddress'\nin the configuration file which binds to any\nserver address.)" % (server.listenipaddress, server.listenport)
            else:
                server.text = 'Failed to bind to port %s.' % server.listenport
            server.text += '\nCheck you do not have another service listenning on\nthis port (you may have a tftp daemon already running).\nAlso check your user permissions allow you to open a\nsocket on this port.'
            if os.name == 'posix' and server.listenport < 1000 and os.geteuid() != 0:
                server.text += '\n(Ports below 1000 may need root or administrator privileges.)'
            server.text += '\nFurther error details will be given in the logs file.'
            raise NoService, 'Unable to bind to given address and port'

        return

    def handle_read(self):
        """Handle incoming data - Checks if this is an existing connection,
        if not, creates a new connection object and adds it to server
        _connections dictionary.
        If it is, then calls the connection object incoming_data method
        for that object to handle it"""
        rx_data, rx_addr = self.recvfrom(4100)
        if len(rx_data) > 4100:
            raise DropPacket
        try:
            if rx_addr not in self.server:
                self.server.create_connection(rx_data, rx_addr)
            else:
                self.server[rx_addr].incoming_data(rx_data)
        except DropPacket:
            pass

    def writable(self):
        """If data available to write, return True"""
        if self.connection:
            if not self.connection.expired and self.connection.tx_data:
                return True
            else:
                self.connection = None
        if not self.connection_list:
            if not len(self.server):
                return False
            self.connection_list = self.server.get_connections_list()
        self.connection = self.connection_list.pop()
        if self.connection.tx_data:
            return True
        else:
            return False
        return

    def handle_write(self):
        """Send any data on the current connection"""
        if not self.connection:
            return
        self.connection.send_data(self.sendto)
        if self.connection.expired or not self.connection.tx_data:
            self.connection = None
        return

    def handle_connect(self):
        pass

    def handle_error(self):
        pass


class Connection(object):
    """Stores details of a connection, acts as a parent to
    SendData and ReceiveData classes"""

    def __init__(self, server, rx_data, rx_addr):
        """New connection, check header"""
        if not server.anyclient:
            if not ipv4.address_in_subnet(rx_addr[0], server.clientipaddress, server.clientmask):
                raise DropPacket
        if len(rx_data) > 512:
            raise DropPacket
        if rx_data[0] != '\x00':
            raise DropPacket
        if rx_data[1] != '\x01' and rx_data[1] != '\x02':
            raise DropPacket
        parts = rx_data[2:].split('\x00')
        if len(parts) < 2:
            raise DropPacket
        self.filename = parts[0]
        self.mode = parts[1].lower()
        if self.mode != 'netascii' and self.mode != 'octet':
            raise DropPacket
        if len(self.filename) < 1 or len(self.filename) > 256:
            raise DropPacket
        if self.filename[0] == '.':
            raise DropPacket
        if self.filename[0] == '\\' or self.filename[0] == '/':
            if len(self.filename) == 1:
                raise DropPacket
            self.filename = self.filename[1:]
        if self.filename[0] == '.':
            raise DropPacket
        temp_filename = self.filename.replace('.', 'x')
        temp_filename = temp_filename.replace('-', 'x')
        temp_filename = temp_filename.replace('_', 'x')
        if not temp_filename.isalnum():
            raise DropPacket
        for conn in server.get_connections_list():
            if self.filename == conn.filename and isinstance(conn, ReceiveData):
                raise DropPacket

        self.filepath = os.path.join(server.tftprootfolder, self.filename)
        self.request_options = {}
        self.options = {}
        self.tx_data = None
        self.blksize = 512
        try:
            if not parts[-1]:
                parts.pop(-1)
            if len(parts) > 3 and not len(parts) % 2:
                self.tx_data = '\x00\x06'
                option_parts = parts[2:]
                for index, value in enumerate(option_parts):
                    if not index % 2:
                        self.request_options[value.lower()] = option_parts[index + 1].lower()

                if 'blksize' in self.request_options:
                    blksize = int(self.request_options['blksize'])
                    if blksize > 4096:
                        blksize = 4096
                    if blksize > 7:
                        self.blksize = blksize
                        self.tx_data += 'blksize\x00' + str(blksize) + '\x00'
                        self.options['blksize'] = str(blksize)
                if not self.options:
                    self.tx_data = None
        except Exception:
            self.blksize = 512
            self.options = {}
            self.tx_data = None

        self.connection_time = time.time()
        self.blkcount = [0, '\x00\x00', 0]
        self.fp = None
        self.server = server
        self.rx_addr = rx_addr
        self.rx_data = rx_data
        self.expired = False
        self.re_tx_data = self.tx_data
        self.timer = Stopwatch()
        self.timeouts = 0
        self.last_packet = False
        return

    def increment_blockcount(self):
        """blkcount is a list, index 0 is blkcount_int holding
        the integer value of the blockcount which rolls over at 65535
        index 1 is the two byte string holding the hex value of blkcount_int.
        index 2 is blkcount_total which holds total number of blocks
        This function increments them all."""
        blkcount_total = self.blkcount[2] + 1
        blkcount_int = self.blkcount[0] + 1
        if blkcount_int > 65535:
            blkcount_int = 0
        blkcount_hex = chr(blkcount_int // 256) + chr(blkcount_int % 256)
        self.blkcount = [blkcount_int, blkcount_hex, blkcount_total]

    def send_data(self, tftp_server_sendto):
        """send any data in self.tx_data, using dispatchers sendto method"""
        if self.expired or not self.tx_data:
            return
        self.connection_time = time.time()
        sent = tftp_server_sendto(self.tx_data, self.rx_addr)
        if sent == -1:
            self.shutdown()
            return
        self.tx_data = self.tx_data[sent:]
        if not self.tx_data:
            if self.last_packet:
                self.shutdown()
            else:
                self.timer.start()

    def poll(self):
        """Checks connection is no longer than 30 seconds between packets.
        Checks TTL timer, resend on timeouts, or if too many timeouts
        send an error packet and flag last_packet as True"""
        if time.time() - self.connection_time > 30.0:
            self.server.add_text('Connection from %s:%s timed out' % self.rx_addr)
            self.shutdown()
            return
        if self.expired:
            return
        if self.tx_data or not self.timer.started:
            return
        if self.timer.time_it():
            return
        self.timeouts += 1
        if self.timeouts <= 3:
            self.tx_data = self.re_tx_data
            return
        self.tx_data = '\x00\x05\x00\x00Terminated due to timeout\x00'
        self.server.add_text('Connection to %s:%s terminated due to timeout' % self.rx_addr)
        self.last_packet = True

    def shutdown(self):
        """Shuts down the connection by closing the file pointer and
        setting the expired flag to True.  Removes the connection from
        the servers connections dictionary"""
        if self.fp:
            self.fp.close()
        self.expired = True
        self.tx_data = ''
        self.server.del_connection(self)

    def __str__(self):
        """String value of connection, for diagnostic purposes"""
        str_list = '%s %s' % (self.rx_addr, self.blkcount[2])
        return str_list


class SendData(Connection):
    """A connection which handles file sending
    the client is reading a file, the connection is of type RRQ"""

    def __init__(self, server, rx_data, rx_addr):
        Connection.__init__(self, server, rx_data, rx_addr)
        if rx_data[1] != '\x01':
            raise DropPacket
        if not os.path.exists(self.filepath) or os.path.isdir(self.filepath):
            server.add_text('%s requested %s: file not found' % (rx_addr[0], self.filename))
            self.tx_data = '\x00\x05\x00\x01File not found\x00'
            self.last_packet = True
            return
        try:
            if self.mode == 'octet':
                self.fp = open(self.filepath, 'rb')
            elif self.mode == 'netascii':
                self.fp = open(self.filepath, 'r')
            else:
                raise DropPacket
        except IOError as e:
            server.add_text('%s requested %s: unable to open file' % (rx_addr[0], self.filename))
            self.tx_data = '\x00\x05\x00\x02Unable to open file\x00'
            self.last_packet = True
            return

        server.add_text('Sending %s to %s' % (self.filename, rx_addr[0]))
        self.last_receive = False
        if not self.tx_data:
            self.get_payload()

    def get_payload(self):
        """Read file, a block of self.blksize bytes at a time which is put
        into re_tx_data and tx_data."""
        if not not self.last_receive:
            raise AssertionError
            payload = self.fp.read(self.blksize)
            len(payload) < self.blksize and self.fp.close()
            self.fp = None
            bytes = self.blksize * self.blkcount[2] + len(payload)
            self.server.add_text('%s bytes of %s sent to %s' % (bytes, self.filename, self.rx_addr[0]))
            self.last_receive = True
        self.increment_blockcount()
        self.re_tx_data = '\x00\x03' + self.blkcount[1] + payload
        self.tx_data = self.re_tx_data
        return

    def incoming_data(self, rx_data):
        """Handles incoming data - these should be acks from the client
        for each data packet sent"""
        if self.expired:
            return
        if self.tx_data or not self.timer.started:
            return
        if rx_data[0] != '\x00':
            return
        if rx_data[1] == '\x05':
            try:
                if len(rx_data[4:]) > 1 and len(rx_data[4:]) < 255:
                    self.server.add_text('Error from %s:%s code %s : %s' % (self.rx_addr[0],
                     self.rx_addr[1],
                     ord(rx_data[3]),
                     rx_data[4:-1]))
                else:
                    self.server.add_text('Error from %s:%s code %s' % (self.rx_addr[0], self.rx_addr[1], ord(rx_data[3])))
            except Exception:
                pass

            self.shutdown()
            return
        if rx_data[1] != '\x04':
            return
        rx_blkcount = rx_data[2:4]
        if self.blkcount[1] != rx_blkcount:
            return
        self.connection_time = time.time()
        self.timeouts = 0
        self.timer.stop()
        if self.last_receive:
            self.shutdown()
            return
        self.get_payload()


class ReceiveData(Connection):
    """A connection which handles file receiving
    the client is sending a file, the connection is of type WRQ"""

    def __init__(self, server, rx_data, rx_addr):
        Connection.__init__(self, server, rx_data, rx_addr)
        if rx_data[1] != '\x02':
            raise DropPacket
        if os.path.exists(self.filepath):
            server.add_text('%s trying to send %s: file already exists' % (rx_addr[0], self.filename))
            self.tx_data = '\x00\x05\x00\x06File already exists\x00'
            self.last_packet = True
            return
        try:
            if self.mode == 'octet':
                self.fp = open(self.filepath, 'wb')
            elif self.mode == 'netascii':
                self.fp = open(self.filepath, 'w')
            else:
                raise DropPacket
        except IOError as e:
            server.add_text('%s trying to send %s: unable to open file' % (rx_addr[0], self.filename))
            self.tx_data = '\x00\x05\x00\x02Unable to open file\x00'
            self.last_packet = True
            return

        server.add_text('Receiving %s from %s' % (self.filename, rx_addr[0]))
        if not self.tx_data:
            self.re_tx_data = '\x00\x04' + self.blkcount[1]
            self.tx_data = self.re_tx_data

    def incoming_data(self, rx_data):
        """Handles incoming data, these should contain the data to be saved to a file"""
        if self.expired:
            return
        if self.tx_data or not self.timer.started:
            return
        if rx_data[0] != '\x00':
            return
        if rx_data[1] == '\x05':
            try:
                if len(rx_data[4:]) > 1 and len(rx_data[4:]) < 255:
                    self.server.add_text('Error from %s:%s code %s : %s' % (self.rx_addr[0],
                     self.rx_addr[1],
                     ord(rx_data[3]),
                     rx_data[4:-1]))
                else:
                    self.server.add_text('Error from %s:%s code %s' % (self.rx_addr[0], self.rx_addr[1], ord(rx_data[3])))
            except Exception:
                pass

            self.shutdown()
            return
        if rx_data[1] != '\x03':
            return
        old_blockcount = self.blkcount
        self.increment_blockcount()
        rx_blkcount = rx_data[2:4]
        if self.blkcount[1] != rx_blkcount:
            self.blkcount = old_blockcount
            return
        self.timeouts = 0
        self.timer.stop()
        if len(rx_data) > self.blksize + 4:
            self.tx_data = '\x00\x05\x00\x04Block size too long\x00'
            self.last_packet = True
            return
        payload = rx_data[4:]
        self.re_tx_data = '\x00\x04' + self.blkcount[1]
        self.tx_data = self.re_tx_data
        if len(payload) > 0:
            self.fp.write(payload)
        if len(payload) < self.blksize:
            self.last_packet = True
            self.fp.close()
            self.fp = None
            bytes = self.blksize * old_blockcount[2] + len(payload)
            self.server.add_text('%s bytes of %s received from %s' % (bytes, self.filename, self.rx_addr[0]))
        return


def loop_nogui(server):
    """This loop is run if there is no gui
    It sets server.serving attribute.
    Then enters loop, calling server.poll()
    If an exception
    occurs, then exits loop
    """
    rootLogger = create_logger(server.logfolder)
    if rootLogger is not None:
        server.logging_enabled = True
    server.serving = True
    try:
        while True:
            server.poll()
            if not len(server):
                time.sleep(0.1)

    except Exception as e:
        server.log_exception(e)
        print server.text
        return 1
    except KeyboardInterrupt:
        return 0
    finally:
        server.shutdown()

    return 0


def loop(server):
    """This loop runs while server.break_loop is False.
    Intended to run with a GUI in another thread,
    it does not exit the loop if a NoService
    exception occurs.
    If the other thread sets server.break_loop to
    True, then the loop exists and shuts down the server"""
    rootLogger = create_logger(server.logfolder)
    if rootLogger is not None:
        server.logging_enabled = True
    try:
        while not server.break_loop:
            try:
                server.poll()
                if server.serving:
                    if not len(server):
                        time.sleep(0.1)
                else:
                    time.sleep(0.25)
            except NoService as e:
                server.log_exception(e)

    except Exception as e:
        server.log_exception(e)
        return 1
    except KeyboardInterrupt:
        return 0
    finally:
        server.shutdown()

    return 0


def loop_multiserver(server_list):
    """This loop is run with a list of servers
    
    This is an experimental loop, showing that if multiple servers
    are given (each with different ports) - then multiple tftp servers
    can operate
    """
    rootLogger = create_logger(server_list[0].logfolder)
    if rootLogger is not None:
        for server in server_list:
            server.logging_enabled = True

    for server in server_list:
        server.serving = True

    try:
        while True:
            for server in server_list:
                server.poll()

    except Exception as e:
        server.log_exception(e)
        print server.text
        return 1
    except KeyboardInterrupt:
        return 0
    finally:
        for server in server_list:
            server.shutdown()

    return 0