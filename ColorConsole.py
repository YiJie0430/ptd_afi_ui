# Embedded file name: ColorConsole.pyc
"""ColorConsole

Colors the text in console mode application (win32).
Calls SetConsoleTextAttribute and GetConsoleScreenBufferInfo.

Author: Andre Burgaud - http://www.burgaud.com/

$Id: ColorConsole.py 260 2006-11-19 20:58:57Z andre $

"""
from ctypes import *
SHORT = c_short
WORD = c_ushort

class COORD(Structure):
    _fields_ = [('X', SHORT), ('Y', SHORT)]


class SMALL_RECT(Structure):
    _fields_ = [('Left', SHORT),
     ('Top', SHORT),
     ('Right', SHORT),
     ('Bottom', SHORT)]


class CONSOLE_SCREEN_BUFFER_INFO(Structure):
    _fields_ = [('dwSize', COORD),
     ('dwCursorPosition', COORD),
     ('wAttributes', WORD),
     ('srWindow', SMALL_RECT),
     ('dwMaximumWindowSize', COORD)]


STD_INPUT_HANDLE = -10
STD_OUTPUT_HANDLE = -11
STD_ERROR_HANDLE = -12
FOREGROUND_COLOR = {'FOREGROUND_BLACK': 0,
 'FOREGROUND_BLUE': 1,
 'FOREGROUND_GREEN': 2,
 'FOREGROUND_CYAN': 3,
 'FOREGROUND_RED': 4,
 'FOREGROUND_MAGENTA': 5,
 'FOREGROUND_YELLOW': 6,
 'FOREGROUND_GREY': 7,
 'FOREGROUND_INTENSITY': 8}
BACKGROUND_BLACK = 0
BACKGROUND_BLUE = 16
BACKGROUND_GREEN = 32
BACKGROUND_CYAN = 48
BACKGROUND_RED = 64
BACKGROUND_MAGENTA = 80
BACKGROUND_YELLOW = 96
BACKGROUND_GREY = 112
BACKGROUND_INTENSITY = 128
h_stdout = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
setConsoleTextAttribute = windll.kernel32.SetConsoleTextAttribute
getConsoleScreenBufferInfo = windll.kernel32.GetConsoleScreenBufferInfo

def getTextAttr():
    """Returns the character attributes (colors) of the console screen
    buffer."""
    csbi = CONSOLE_SCREEN_BUFFER_INFO()
    getConsoleScreenBufferInfo(h_stdout, byref(csbi))
    return csbi.wAttributes


def setTextAttr(color):
    """Sets the character attributes (colors) of the console screen
    buffer. Color is a combination of foreground and background color,
    foreground and background intensity."""
    setConsoleTextAttribute(h_stdout, color)


def printColor(color, msg):
    default_colors = FOREGROUND_COLOR['FOREGROUND_GREY']
    default_bg = default_colors & 112
    if 'PASS' in color.upper():
        color = 'FOREGROUND_GREEN'
    elif 'FAIL' in color.upper():
        color = 'FOREGROUND_RED'
    for i in FOREGROUND_COLOR.keys():
        if color.upper() in i:
            fg_color = FOREGROUND_COLOR[i]
            break
    else:
        fg_color = default_colors

    setTextAttr(fg_color | default_bg | FOREGROUND_COLOR['FOREGROUND_INTENSITY'])
    print msg
    setTextAttr(default_colors)


colorPrint = printColor

def test():
    """Simple test for ColorConsole."""
    default_colors = getTextAttr()
    default_fg = default_colors & 7
    default_bg = default_colors & 112
    setTextAttr(FOREGROUND_BLUE | default_bg | FOREGROUND_INTENSITY)
    print '==========================================='
    setTextAttr(FOREGROUND_BLUE | BACKGROUND_GREY | FOREGROUND_INTENSITY | BACKGROUND_INTENSITY)
    print 'And Now for Something',
    setTextAttr(FOREGROUND_RED | BACKGROUND_GREY | FOREGROUND_INTENSITY | BACKGROUND_INTENSITY)
    print 'Completely Different!',
    setTextAttr(default_colors)
    print
    setTextAttr(FOREGROUND_RED | default_bg | FOREGROUND_INTENSITY)
    print '==========================================='
    setTextAttr(default_colors)