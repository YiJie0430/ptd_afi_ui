# Embedded file name: serial\loopback_connection.pyc
from serialutil import *
import threading
import time
import logging
LOGGER_LEVELS = {'debug': logging.DEBUG,
 'info': logging.INFO,
 'warning': logging.WARNING,
 'error': logging.ERROR}

class LoopbackSerial(SerialBase):
    """Serial port implementation for plain sockets."""
    BAUDRATES = (50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200)

    def open(self):
        """Open port with current settings. This may throw a SerialException
        if the port cannot be opened."""
        self.logger = None
        self.buffer_lock = threading.Lock()
        self.loop_buffer = bytearray()
        self.cts = False
        self.dsr = False
        if self._port is None:
            raise SerialException('Port must be configured before it can be used.')
        self.fromURL(self.port)
        self._reconfigurePort()
        self._isOpen = True
        if not self._rtscts:
            self.setRTS(True)
            self.setDTR(True)
        self.flushInput()
        self.flushOutput()
        return

    def _reconfigurePort(self):
        """Set communication parameters on opened port. for the loop://
        protocol all settings are ignored!"""
        if not isinstance(self._baudrate, (int, long)) or not 0 < self._baudrate < 4294967296L:
            raise ValueError('invalid baudrate: %r' % self._baudrate)
        if self.logger:
            self.logger.info('_reconfigurePort()')

    def close(self):
        """Close port"""
        if self._isOpen:
            self._isOpen = False
            time.sleep(0.3)

    def makeDeviceName(self, port):
        raise SerialException('there is no sensible way to turn numbers into URLs')

    def fromURL(self, url):
        """extract host and port from an URL string"""
        if url.lower().startswith('loop://'):
            url = url[7:]
        try:
            for option in url.split('/'):
                if '=' in option:
                    option, value = option.split('=', 1)
                else:
                    value = None
                if not option:
                    pass
                elif option == 'logging':
                    logging.basicConfig()
                    self.logger = logging.getLogger('pySerial.loop')
                    self.logger.setLevel(LOGGER_LEVELS[value])
                    self.logger.debug('enabled logging')
                else:
                    raise ValueError('unknown option: %r' % (option,))

        except ValueError as e:
            raise SerialException('expected a string in the form "[loop://][option[/option...]]": %s' % e)

        return

    def inWaiting(self):
        """Return the number of characters currently in the input buffer."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.debug('inWaiting() -> %d' % (len(self.loop_buffer),))
        return len(self.loop_buffer)

    def read(self, size = 1):
        """Read size bytes from the serial port. If a timeout is set it may
        return less characters as requested. With no timeout it will block
        until the requested number of bytes is read."""
        if not self._isOpen:
            raise portNotOpenError
        if self._timeout is not None:
            timeout = time.time() + self._timeout
        else:
            timeout = None
        data = bytearray()
        while len(data) < size:
            self.buffer_lock.acquire()
            try:
                block = to_bytes(self.loop_buffer[:size])
                del self.loop_buffer[:size]
            finally:
                self.buffer_lock.release()

            data += block
            if timeout and time.time() > timeout:
                break

        return bytes(data)

    def write(self, data):
        """Output the given string over the serial port. Can block if the
        connection is blocked. May raise SerialException if the connection is
        closed."""
        if not self._isOpen:
            raise portNotOpenError
        time_used_to_send = 10.0 * len(data) / self._baudrate
        if self._writeTimeout is not None and time_used_to_send > self._writeTimeout:
            time.sleep(self._writeTimeout)
            raise writeTimeoutError
        self.buffer_lock.acquire()
        try:
            self.loop_buffer += bytes(data)
        finally:
            self.buffer_lock.release()

        return len(data)

    def flushInput(self):
        """Clear input buffer, discarding all that is in the buffer."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('flushInput()')
        self.buffer_lock.acquire()
        try:
            del self.loop_buffer[:]
        finally:
            self.buffer_lock.release()

    def flushOutput(self):
        """Clear output buffer, aborting the current output and
        discarding all that is in the buffer."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('flushOutput()')

    def sendBreak(self, duration = 0.25):
        """Send break condition. Timed, returns to idle state after given
        duration."""
        if not self._isOpen:
            raise portNotOpenError

    def setBreak(self, level = True):
        """Set break: Controls TXD. When active, to transmitting is
        possible."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('setBreak(%r)' % (level,))

    def setRTS(self, level = True):
        """Set terminal status line: Request To Send"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('setRTS(%r) -> state of CTS' % (level,))
        self.cts = level

    def setDTR(self, level = True):
        """Set terminal status line: Data Terminal Ready"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('setDTR(%r) -> state of DSR' % (level,))
        self.dsr = level

    def getCTS(self):
        """Read terminal status line: Clear To Send"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('getCTS() -> state of RTS (%r)' % (self.cts,))
        return self.cts

    def getDSR(self):
        """Read terminal status line: Data Set Ready"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('getDSR() -> state of DTR (%r)' % (self.dsr,))
        return self.dsr

    def getRI(self):
        """Read terminal status line: Ring Indicator"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for getRI()')
        return False

    def getCD(self):
        """Read terminal status line: Carrier Detect"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for getCD()')
        return True


try:
    import io
except ImportError:

    class Serial(LoopbackSerial, FileLike):
        pass


else:

    class Serial(LoopbackSerial, io.RawIOBase):
        pass


if __name__ == '__main__':
    import sys
    s = Serial('socket://localhost:7000')
    sys.stdout.write('%s\n' % s)
    sys.stdout.write('write...\n')
    s.write('hello\n')
    s.flush()
    sys.stdout.write('read: %s\n' % s.read(5))
    s.close()