# Embedded file name: serial\serialposix.pyc
import sys, os, fcntl, termios, struct, select, errno, time
from serialutil import *
if sys.hexversion < 33620208:
    import TERMIOS
else:
    TERMIOS = termios
if sys.hexversion < 33685744:
    import FCNTL
else:
    FCNTL = fcntl
plat = sys.platform.lower()
if plat[:5] == 'linux':

    def device(port):
        return '/dev/ttyS%d' % port


    ASYNC_SPD_MASK = 4144
    ASYNC_SPD_CUST = 48

    def set_special_baudrate(port, baudrate):
        import array
        buf = array.array('i', [0] * 32)
        FCNTL.ioctl(port.fd, TERMIOS.TIOCGSERIAL, buf)
        buf[6] = buf[7] / baudrate
        buf[4] &= ~ASYNC_SPD_MASK
        buf[4] |= ASYNC_SPD_CUST
        try:
            res = FCNTL.ioctl(port.fd, TERMIOS.TIOCSSERIAL, buf)
        except IOError:
            raise ValueError('Failed to set custom baud rate: %r' % baudrate)


    baudrate_constants = {0: 0,
     50: 1,
     75: 2,
     110: 3,
     134: 4,
     150: 5,
     200: 6,
     300: 7,
     600: 8,
     1200: 9,
     1800: 10,
     2400: 11,
     4800: 12,
     9600: 13,
     19200: 14,
     38400: 15,
     57600: 4097,
     115200: 4098,
     230400: 4099,
     460800: 4100,
     500000: 4101,
     576000: 4102,
     921600: 4103,
     1000000: 4104,
     1152000: 4105,
     1500000: 4106,
     2000000: 4107,
     2500000: 4108,
     3000000: 4109,
     3500000: 4110,
     4000000: 4111}
elif plat == 'cygwin':

    def device(port):
        return '/dev/com%d' % (port + 1)


    def set_special_baudrate(port, baudrate):
        raise ValueError("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
elif plat == 'openbsd3':

    def device(port):
        return '/dev/ttyp%d' % port


    def set_special_baudrate(port, baudrate):
        raise ValueError("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
elif plat[:3] == 'bsd' or plat[:7] == 'freebsd' or plat[:7] == 'openbsd':

    def device(port):
        return '/dev/cuad%d' % port


    def set_special_baudrate(port, baudrate):
        raise ValueError("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
elif plat[:6] == 'darwin':
    version = os.uname()[2].split('.')
    if int(version[0]) >= 8:

        def set_special_baudrate(port, baudrate):
            import array, fcntl
            buf = array.array('i', [baudrate])
            IOSSIOSPEED = 2147767298L
            fcntl.ioctl(port.fd, IOSSIOSPEED, buf, 1)


    else:

        def set_special_baudrate(port, baudrate):
            raise ValueError('baud rate not supported')


    def device(port):
        return '/dev/cuad%d' % port


    baudrate_constants = {}
elif plat[:6] == 'netbsd':

    def device(port):
        return '/dev/dty%02d' % port


    def set_special_baudrate(port, baudrate):
        raise ValueError("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
elif plat[:4] == 'irix':

    def device(port):
        return '/dev/ttyf%d' % (port + 1)


    def set_special_baudrate(port, baudrate):
        raise ValueError("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
elif plat[:2] == 'hp':

    def device(port):
        return '/dev/tty%dp0' % (port + 1)


    def set_special_baudrate(port, baudrate):
        raise ValueError("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
elif plat[:5] == 'sunos':

    def device(port):
        return '/dev/tty%c' % (ord('a') + port)


    def set_special_baudrate(port, baudrate):
        raise ValueError("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
elif plat[:3] == 'aix':

    def device(port):
        return '/dev/tty%d' % port


    def set_special_baudrate(port, baudrate):
        raise ValueError("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
else:
    sys.stderr.write("don't know how to number ttys on this system.\n! Use an explicit path (eg /dev/ttyS1) or send this information to\n! the author of this module:\n\nsys.platform = %r\nos.name = %r\nserialposix.py version = %s\n\nalso add the device name of the serial port and where the\ncounting starts for the first serial port.\ne.g. 'first serial port: /dev/ttyS0'\nand with a bit luck you can get this module running...\n" % (sys.platform, os.name, VERSION))

    def device(portum):
        return '/dev/ttyS%d' % portnum


    def set_special_baudrate(port, baudrate):
        raise SerialException("sorry don't know how to handle non standard baud rate on this platform")


    baudrate_constants = {}
if hasattr(TERMIOS, 'TIOCMGET'):
    TIOCMGET = TERMIOS.TIOCMGET or 21525
    TIOCMBIS = hasattr(TERMIOS, 'TIOCMBIS') and (TERMIOS.TIOCMBIS or 21526)
    TIOCMBIC = hasattr(TERMIOS, 'TIOCMBIC') and (TERMIOS.TIOCMBIC or 21527)
    TIOCMSET = hasattr(TERMIOS, 'TIOCMSET') and (TERMIOS.TIOCMSET or 21528)
    TIOCM_DTR = hasattr(TERMIOS, 'TIOCM_DTR') and (TERMIOS.TIOCM_DTR or 2)
    TIOCM_RTS = hasattr(TERMIOS, 'TIOCM_RTS') and (TERMIOS.TIOCM_RTS or 4)
    TIOCM_CTS = hasattr(TERMIOS, 'TIOCM_CTS') and (TERMIOS.TIOCM_CTS or 32)
    TIOCM_CAR = hasattr(TERMIOS, 'TIOCM_CAR') and (TERMIOS.TIOCM_CAR or 64)
    TIOCM_RNG = hasattr(TERMIOS, 'TIOCM_RNG') and (TERMIOS.TIOCM_RNG or 128)
    TIOCM_DSR = hasattr(TERMIOS, 'TIOCM_DSR') and (TERMIOS.TIOCM_DSR or 256)
    TIOCM_CD = hasattr(TERMIOS, 'TIOCM_CD') and (TERMIOS.TIOCM_CD or TIOCM_CAR)
    TIOCM_RI = hasattr(TERMIOS, 'TIOCM_RI') and (TERMIOS.TIOCM_RI or TIOCM_RNG)
    TIOCINQ = hasattr(TERMIOS, 'FIONREAD') and (TERMIOS.FIONREAD or 21531)
    TIOCM_zero_str = struct.pack('I', 0)
    TIOCM_RTS_str = struct.pack('I', TIOCM_RTS)
    TIOCM_DTR_str = struct.pack('I', TIOCM_DTR)
    TIOCSBRK = hasattr(TERMIOS, 'TIOCSBRK') and (TERMIOS.TIOCSBRK or 21543)
    TIOCCBRK = hasattr(TERMIOS, 'TIOCCBRK') and (TERMIOS.TIOCCBRK or 21544)

    class PosixSerial(SerialBase):
        """Serial port class POSIX implementation. Serial port configuration is 
        done with termios and fcntl. Runs on Linux and many other Un*x like
        systems."""

        def open(self):
            """Open port with current settings. This may throw a SerialException
            if the port cannot be opened."""
            self.fd = None
            if self._port is None:
                raise SerialException('Port must be configured before it can be used.')
            try:
                self.fd = os.open(self.portstr, os.O_RDWR | os.O_NOCTTY | os.O_NONBLOCK)
            except Exception as msg:
                self.fd = None
                raise SerialException('could not open port %s: %s' % (self._port, msg))

            try:
                self._reconfigurePort()
            except:
                try:
                    os.close(self.fd)
                except:
                    pass

                self.fd = None
                raise
            else:
                self._isOpen = True

            return

        def _reconfigurePort(self):
            """Set communication parameters on opened port."""
            if self.fd is None:
                raise SerialException('Can only operate on a valid file descriptor')
            custom_baud = None
            vmin = vtime = 0
            if self._interCharTimeout is not None:
                vmin = 1
                vtime = int(self._interCharTimeout * 10)
            try:
                iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(self.fd)
            except termios.error as msg:
                raise SerialException('Could not configure port: %s' % msg)

            cflag |= TERMIOS.CLOCAL | TERMIOS.CREAD
            lflag &= ~(TERMIOS.ICANON | TERMIOS.ECHO | TERMIOS.ECHOE | TERMIOS.ECHOK | TERMIOS.ECHONL | TERMIOS.ISIG | TERMIOS.IEXTEN)
            for flag in ('ECHOCTL', 'ECHOKE'):
                if hasattr(TERMIOS, flag):
                    lflag &= ~getattr(TERMIOS, flag)

            oflag &= ~TERMIOS.OPOST
            iflag &= ~(TERMIOS.INLCR | TERMIOS.IGNCR | TERMIOS.ICRNL | TERMIOS.IGNBRK)
            if hasattr(TERMIOS, 'IUCLC'):
                iflag &= ~TERMIOS.IUCLC
            if hasattr(TERMIOS, 'PARMRK'):
                iflag &= ~TERMIOS.PARMRK
            try:
                ispeed = ospeed = getattr(TERMIOS, 'B%s' % self._baudrate)
            except AttributeError:
                try:
                    ispeed = ospeed = baudrate_constants[self._baudrate]
                except KeyError:
                    ispeed = ospeed = getattr(TERMIOS, 'B38400')
                    try:
                        custom_baud = int(self._baudrate)
                    except ValueError:
                        raise ValueError('Invalid baud rate: %r' % self._baudrate)
                    else:
                        if custom_baud < 0:
                            raise ValueError('Invalid baud rate: %r' % self._baudrate)

            cflag &= ~TERMIOS.CSIZE
            if self._bytesize == 8:
                cflag |= TERMIOS.CS8
            elif self._bytesize == 7:
                cflag |= TERMIOS.CS7
            elif self._bytesize == 6:
                cflag |= TERMIOS.CS6
            elif self._bytesize == 5:
                cflag |= TERMIOS.CS5
            else:
                raise ValueError('Invalid char len: %r' % self._bytesize)
            if self._stopbits == STOPBITS_ONE:
                cflag &= ~TERMIOS.CSTOPB
            elif self._stopbits == STOPBITS_ONE_POINT_FIVE:
                cflag |= TERMIOS.CSTOPB
            elif self._stopbits == STOPBITS_TWO:
                cflag |= TERMIOS.CSTOPB
            else:
                raise ValueError('Invalid stop bit specification: %r' % self._stopbits)
            iflag &= ~(TERMIOS.INPCK | TERMIOS.ISTRIP)
            if self._parity == PARITY_NONE:
                cflag &= ~(TERMIOS.PARENB | TERMIOS.PARODD)
            elif self._parity == PARITY_EVEN:
                cflag &= ~TERMIOS.PARODD
                cflag |= TERMIOS.PARENB
            elif self._parity == PARITY_ODD:
                cflag |= TERMIOS.PARENB | TERMIOS.PARODD
            else:
                raise ValueError('Invalid parity: %r' % self._parity)
            if hasattr(TERMIOS, 'IXANY'):
                if self._xonxoff:
                    iflag |= TERMIOS.IXON | TERMIOS.IXOFF
                else:
                    iflag &= ~(TERMIOS.IXON | TERMIOS.IXOFF | TERMIOS.IXANY)
            elif self._xonxoff:
                iflag |= TERMIOS.IXON | TERMIOS.IXOFF
            else:
                iflag &= ~(TERMIOS.IXON | TERMIOS.IXOFF)
            if hasattr(TERMIOS, 'CRTSCTS'):
                if self._rtscts:
                    cflag |= TERMIOS.CRTSCTS
                else:
                    cflag &= ~TERMIOS.CRTSCTS
            elif hasattr(TERMIOS, 'CNEW_RTSCTS'):
                if self._rtscts:
                    cflag |= TERMIOS.CNEW_RTSCTS
                else:
                    cflag &= ~TERMIOS.CNEW_RTSCTS
            if vmin < 0 or vmin > 255:
                raise ValueError('Invalid vmin: %r ' % vmin)
            cc[TERMIOS.VMIN] = vmin
            if vtime < 0 or vtime > 255:
                raise ValueError('Invalid vtime: %r' % vtime)
            cc[TERMIOS.VTIME] = vtime
            termios.tcsetattr(self.fd, TERMIOS.TCSANOW, [iflag,
             oflag,
             cflag,
             lflag,
             ispeed,
             ospeed,
             cc])
            if custom_baud is not None:
                set_special_baudrate(self, custom_baud)
            return

        def close(self):
            """Close port"""
            if self._isOpen:
                if self.fd is not None:
                    os.close(self.fd)
                    self.fd = None
                self._isOpen = False
            return

        def makeDeviceName(self, port):
            return device(port)

        def inWaiting(self):
            """Return the number of characters currently in the input buffer."""
            s = fcntl.ioctl(self.fd, TIOCINQ, TIOCM_zero_str)
            return struct.unpack('I', s)[0]

        def read(self, size = 1):
            """Read size bytes from the serial port. If a timeout is set it may
            return less characters as requested. With no timeout it will block
            until the requested number of bytes is read."""
            if self.fd is None:
                raise portNotOpenError
            read = bytearray()
            while len(read) < size:
                ready, _, _ = select.select([self.fd], [], [], self._timeout)
                if not ready:
                    break
                buf = os.read(self.fd, size - len(read))
                if not buf:
                    raise SerialException('device reports readiness to read but returned no data (device disconnected?)')
                read.extend(buf)

            return bytes(read)

        def write(self, data):
            """Output the given string over the serial port."""
            if self.fd is None:
                raise portNotOpenError
            t = len(data)
            d = data
            if self._writeTimeout is not None and self._writeTimeout > 0:
                timeout = time.time() + self._writeTimeout
            else:
                timeout = None
            while t > 0:
                try:
                    n = os.write(self.fd, d)
                    if timeout:
                        timeleft = timeout - time.time()
                        if timeleft < 0:
                            raise writeTimeoutError
                        _, ready, _ = select.select([], [self.fd], [], timeleft)
                        if not ready:
                            raise writeTimeoutError
                    d = d[n:]
                    t = t - n
                except OSError as v:
                    if v.errno != errno.EAGAIN:
                        raise SerialException('write failed: %s' % (v,))

            return len(data)

        def flush(self):
            """Flush of file like objects. In this case, wait until all data
            is written."""
            self.drainOutput()

        def flushInput(self):
            """Clear input buffer, discarding all that is in the buffer."""
            if self.fd is None:
                raise portNotOpenError
            termios.tcflush(self.fd, TERMIOS.TCIFLUSH)
            return

        def flushOutput(self):
            """Clear output buffer, aborting the current output and
            discarding all that is in the buffer."""
            if self.fd is None:
                raise portNotOpenError
            termios.tcflush(self.fd, TERMIOS.TCOFLUSH)
            return

        def sendBreak(self, duration = 0.25):
            """Send break condition. Timed, returns to idle state after given duration."""
            if self.fd is None:
                raise portNotOpenError
            termios.tcsendbreak(self.fd, int(duration / 0.25))
            return

        def setBreak(self, level = 1):
            """Set break: Controls TXD. When active, no transmitting is possible."""
            if self.fd is None:
                raise portNotOpenError
            if level:
                fcntl.ioctl(self.fd, TIOCSBRK)
            else:
                fcntl.ioctl(self.fd, TIOCCBRK)
            return

        def setRTS(self, level = 1):
            """Set terminal status line: Request To Send"""
            if self.fd is None:
                raise portNotOpenError
            if level:
                fcntl.ioctl(self.fd, TIOCMBIS, TIOCM_RTS_str)
            else:
                fcntl.ioctl(self.fd, TIOCMBIC, TIOCM_RTS_str)
            return

        def setDTR(self, level = 1):
            """Set terminal status line: Data Terminal Ready"""
            if self.fd is None:
                raise portNotOpenError
            if level:
                fcntl.ioctl(self.fd, TIOCMBIS, TIOCM_DTR_str)
            else:
                fcntl.ioctl(self.fd, TIOCMBIC, TIOCM_DTR_str)
            return

        def getCTS(self):
            """Read terminal status line: Clear To Send"""
            if self.fd is None:
                raise portNotOpenError
            s = fcntl.ioctl(self.fd, TIOCMGET, TIOCM_zero_str)
            return struct.unpack('I', s)[0] & TIOCM_CTS != 0

        def getDSR(self):
            """Read terminal status line: Data Set Ready"""
            if self.fd is None:
                raise portNotOpenError
            s = fcntl.ioctl(self.fd, TIOCMGET, TIOCM_zero_str)
            return struct.unpack('I', s)[0] & TIOCM_DSR != 0

        def getRI(self):
            """Read terminal status line: Ring Indicator"""
            if self.fd is None:
                raise portNotOpenError
            s = fcntl.ioctl(self.fd, TIOCMGET, TIOCM_zero_str)
            return struct.unpack('I', s)[0] & TIOCM_RI != 0

        def getCD(self):
            """Read terminal status line: Carrier Detect"""
            if self.fd is None:
                raise portNotOpenError
            s = fcntl.ioctl(self.fd, TIOCMGET, TIOCM_zero_str)
            return struct.unpack('I', s)[0] & TIOCM_CD != 0

        def drainOutput(self):
            """internal - not portable!"""
            if self.fd is None:
                raise portNotOpenError
            termios.tcdrain(self.fd)
            return

        def nonblocking(self):
            """internal - not portable!"""
            if self.fd is None:
                raise portNotOpenError
            fcntl.fcntl(self.fd, FCNTL.F_SETFL, FCNTL.O_NONBLOCK)
            return

        def fileno(self):
            """For easier use of the serial port instance with select.
            WARNING: this function is not portable to different platforms!"""
            if self.fd is None:
                raise portNotOpenError
            return self.fd

        def flowControl(self, enable):
            """manually control flow - when hardware or software flow control is
            enabled"""
            if enable:
                termios.tcflow(self.fd, TERMIOS.TCION)
            else:
                termios.tcflow(self.fd, TERMIOS.TCIOFF)


    try:
        import io
    except ImportError:

        class Serial(PosixSerial, FileLike):
            pass


    else:

        class Serial(PosixSerial, io.RawIOBase):
            pass


    class PosixPollSerial(Serial):
        """poll based read implementation. not all systems support poll properly.
        however this one has better handling of errors, such as a device
        disconnecting while it's in use (e.g. USB-serial unplugged)"""

        def read(self, size = 1):
            """Read size bytes from the serial port. If a timeout is set it may
            return less characters as requested. With no timeout it will block
            until the requested number of bytes is read."""
            if self.fd is None:
                raise portNotOpenError
            read = bytearray()
            poll = select.poll()
            poll.register(self.fd, select.POLLIN | select.POLLERR | select.POLLHUP | select.POLLNVAL)
            if size > 0:
                while len(read) < size:
                    for fd, event in poll.poll(self._timeout):
                        if event & (select.POLLERR | select.POLLHUP | select.POLLNVAL):
                            raise SerialException('device reports error (poll)')

                    buf = os.read(self.fd, size - len(read))
                    read.extend(buf)
                    if (self._timeout is not None and self._timeout >= 0 or self._interCharTimeout is not None and self._interCharTimeout > 0) and not buf:
                        break

            return bytes(read)


    s = __name__ == '__main__' and Serial(0, baudrate=19200, bytesize=EIGHTBITS, parity=PARITY_EVEN, stopbits=STOPBITS_ONE, timeout=3, xonxoff=0, rtscts=0)
    s.setRTS(1)
    s.setDTR(1)
    s.flushInput()
    s.flushOutput()
    s.write('hello')
    sys.stdout.write('%r\n' % s.read(5))
    sys.stdout.write('%s\n' % s.inWaiting())
    del s