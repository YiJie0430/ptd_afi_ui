# Embedded file name: serial\serialutil.pyc
try:
    bytes
    bytearray
except (NameError, AttributeError):
    bytes = str

    class bytearray(list):

        def __str__(self):
            return ''.join(self)

        def append(self, item):
            if isinstance(item, str):
                list.append(self, item)
            else:
                list.append(self, chr(item))

        def __iadd__(self, other):
            for byte in other:
                self.append(byte)

            return self


def to_bytes(seq):
    """convert a sequence to a bytes type"""
    b = bytearray()
    for item in seq:
        b.append(item)

    return bytes(b)


XON = to_bytes([17])
XOFF = to_bytes([19])
PARITY_NONE, PARITY_EVEN, PARITY_ODD, PARITY_MARK, PARITY_SPACE = ('N',
 'E',
 'O',
 'M',
 'S')
STOPBITS_ONE, STOPBITS_ONE_POINT_FIVE, STOPBITS_TWO = (1, 1.5, 2)
FIVEBITS, SIXBITS, SEVENBITS, EIGHTBITS = (5,
 6,
 7,
 8)
PARITY_NAMES = {PARITY_NONE: 'None',
 PARITY_EVEN: 'Even',
 PARITY_ODD: 'Odd',
 PARITY_MARK: 'Mark',
 PARITY_SPACE: 'Space'}

class SerialException(IOError):
    """Base class for serial port related exceptions."""
    pass


class SerialTimeoutException(SerialException):
    """Write timeouts give an exception"""
    pass


writeTimeoutError = SerialTimeoutException('Write timeout')
portNotOpenError = ValueError('Attempting to use a port that is not open')

class FileLike(object):
    """An abstract file like class.
    
    This class implements readline and readlines based on read and
    writelines based on write.
    This class is used to provide the above functions for to Serial
    port objects.
    
    Note that when the serial port was opened with _NO_ timeout that
    readline blocks until it sees a newline (or the specified size is
    reached) and that readlines would never return and therefore
    refuses to work (it raises an exception in this case)!
    """

    def __init__(self):
        self.closed = True

    def close(self):
        self.closed = True

    def __del__(self):
        """Destructor.  Calls close()."""
        try:
            self.close()
        except:
            pass

    def writelines(self, sequence):
        for line in sequence:
            self.write(line)

    def flush(self):
        """flush of file like objects"""
        pass

    def next(self):
        line = self.readline()
        if not line:
            raise StopIteration
        return line

    def __iter__(self):
        return self

    def seek(self, pos, whence = 0):
        raise IOError('file is not seekable')

    def tell(self):
        raise IOError('file is not seekable')

    def truncate(self, n = None):
        raise IOError('file is not seekable')

    def isatty(self):
        return False


class SerialBase(object):
    """Serial port base class. Provides __init__ function and properties to
    get/set port settings."""
    BAUDRATES = (50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 500000, 576000, 921600, 1000000, 1152000, 1500000, 2000000, 2500000, 3000000, 3500000, 4000000)
    BYTESIZES = (FIVEBITS,
     SIXBITS,
     SEVENBITS,
     EIGHTBITS)
    PARITIES = (PARITY_NONE,
     PARITY_EVEN,
     PARITY_ODD,
     PARITY_MARK,
     PARITY_SPACE)
    STOPBITS = (STOPBITS_ONE, STOPBITS_ONE_POINT_FIVE, STOPBITS_TWO)

    def __init__(self, port = None, baudrate = 9600, bytesize = EIGHTBITS, parity = PARITY_NONE, stopbits = STOPBITS_ONE, timeout = None, xonxoff = 0, rtscts = 0, writeTimeout = None, dsrdtr = None, interCharTimeout = None):
        """Initialize comm port object. If a port is given, then the port will be
        opened immediately. Otherwise a Serial port object in closed state
        is returned."""
        self._isOpen = False
        self._port = None
        self._baudrate = None
        self._bytesize = None
        self._parity = None
        self._stopbits = None
        self._timeout = None
        self._writeTimeout = None
        self._xonxoff = None
        self._rtscts = None
        self._dsrdtr = None
        self._interCharTimeout = None
        self.port = port
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.parity = parity
        self.stopbits = stopbits
        self.timeout = timeout
        self.writeTimeout = writeTimeout
        self.xonxoff = xonxoff
        self.rtscts = rtscts
        self.dsrdtr = dsrdtr
        self.interCharTimeout = interCharTimeout
        if port is not None:
            self.open()
        return

    def isOpen(self):
        """Check if the port is opened."""
        return self._isOpen

    def getSupportedBaudrates(self):
        return [ (str(b), b) for b in self.BAUDRATES ]

    def getSupportedByteSizes(self):
        return [ (str(b), b) for b in self.BYTESIZES ]

    def getSupportedStopbits(self):
        return [ (str(b), b) for b in self.STOPBITS ]

    def getSupportedParities(self):
        return [ (PARITY_NAMES[b], b) for b in self.PARITIES ]

    def setPort(self, port):
        """Change the port. The attribute portstr is set to a string that
        contains the name of the port."""
        was_open = self._isOpen
        if was_open:
            self.close()
        if port is not None:
            if isinstance(port, basestring):
                self.portstr = port
            else:
                self.portstr = self.makeDeviceName(port)
        else:
            self.portstr = None
        self._port = port
        self.name = self.portstr
        if was_open:
            self.open()
        return

    def getPort(self):
        """Get the current port setting. The value that was passed on init or using
        setPort() is passed back. See also the attribute portstr which contains
        the name of the port as a string."""
        return self._port

    port = property(getPort, setPort, doc='Port setting')

    def setBaudrate(self, baudrate):
        """Change baud rate. It raises a ValueError if the port is open and the
        baud rate is not possible. If the port is closed, then the value is
        accepted and the exception is raised when the port is opened."""
        try:
            self._baudrate = int(baudrate)
        except TypeError:
            raise ValueError('Not a valid baudrate: %r' % (baudrate,))
        else:
            if self._isOpen:
                self._reconfigurePort()

    def getBaudrate(self):
        """Get the current baud rate setting."""
        return self._baudrate

    baudrate = property(getBaudrate, setBaudrate, doc='Baud rate setting')

    def setByteSize(self, bytesize):
        """Change byte size."""
        if bytesize not in self.BYTESIZES:
            raise ValueError('Not a valid byte size: %r' % (bytesize,))
        self._bytesize = bytesize
        if self._isOpen:
            self._reconfigurePort()

    def getByteSize(self):
        """Get the current byte size setting."""
        return self._bytesize

    bytesize = property(getByteSize, setByteSize, doc='Byte size setting')

    def setParity(self, parity):
        """Change parity setting."""
        if parity not in self.PARITIES:
            raise ValueError('Not a valid parity: %r' % (parity,))
        self._parity = parity
        if self._isOpen:
            self._reconfigurePort()

    def getParity(self):
        """Get the current parity setting."""
        return self._parity

    parity = property(getParity, setParity, doc='Parity setting')

    def setStopbits(self, stopbits):
        """Change stop bits size."""
        if stopbits not in self.STOPBITS:
            raise ValueError('Not a valid stop bit size: %r' % (stopbits,))
        self._stopbits = stopbits
        if self._isOpen:
            self._reconfigurePort()

    def getStopbits(self):
        """Get the current stop bits setting."""
        return self._stopbits

    stopbits = property(getStopbits, setStopbits, doc='Stop bits setting')

    def setTimeout(self, timeout):
        """Change timeout setting."""
        if timeout is not None:
            try:
                timeout + 1
            except TypeError:
                raise ValueError('Not a valid timeout: %r' % (timeout,))

            if timeout < 0:
                raise ValueError('Not a valid timeout: %r' % (timeout,))
        self._timeout = timeout
        if self._isOpen:
            self._reconfigurePort()
        return

    def getTimeout(self):
        """Get the current timeout setting."""
        return self._timeout

    timeout = property(getTimeout, setTimeout, doc='Timeout setting for read()')

    def setWriteTimeout(self, timeout):
        """Change timeout setting."""
        if timeout is not None:
            if timeout < 0:
                raise ValueError('Not a valid timeout: %r' % (timeout,))
            try:
                timeout + 1
            except TypeError:
                raise ValueError('Not a valid timeout: %r' % timeout)

        self._writeTimeout = timeout
        if self._isOpen:
            self._reconfigurePort()
        return

    def getWriteTimeout(self):
        """Get the current timeout setting."""
        return self._writeTimeout

    writeTimeout = property(getWriteTimeout, setWriteTimeout, doc='Timeout setting for write()')

    def setXonXoff(self, xonxoff):
        """Change XON/XOFF setting."""
        self._xonxoff = xonxoff
        if self._isOpen:
            self._reconfigurePort()

    def getXonXoff(self):
        """Get the current XON/XOFF setting."""
        return self._xonxoff

    xonxoff = property(getXonXoff, setXonXoff, doc='XON/XOFF setting')

    def setRtsCts(self, rtscts):
        """Change RTS/CTS flow control setting."""
        self._rtscts = rtscts
        if self._isOpen:
            self._reconfigurePort()

    def getRtsCts(self):
        """Get the current RTS/CTS flow control setting."""
        return self._rtscts

    rtscts = property(getRtsCts, setRtsCts, doc='RTS/CTS flow control setting')

    def setDsrDtr(self, dsrdtr = None):
        """Change DsrDtr flow control setting."""
        if dsrdtr is None:
            self._dsrdtr = self._rtscts
        else:
            self._dsrdtr = dsrdtr
        if self._isOpen:
            self._reconfigurePort()
        return

    def getDsrDtr(self):
        """Get the current DSR/DTR flow control setting."""
        return self._dsrdtr

    dsrdtr = property(getDsrDtr, setDsrDtr, 'DSR/DTR flow control setting')

    def setInterCharTimeout(self, interCharTimeout):
        """Change inter-character timeout setting."""
        if interCharTimeout is not None:
            if interCharTimeout < 0:
                raise ValueError('Not a valid timeout: %r' % interCharTimeout)
            try:
                interCharTimeout + 1
            except TypeError:
                raise ValueError('Not a valid timeout: %r' % interCharTimeout)

        self._interCharTimeout = interCharTimeout
        if self._isOpen:
            self._reconfigurePort()
        return

    def getInterCharTimeout(self):
        """Get the current inter-character timeout setting."""
        return self._interCharTimeout

    interCharTimeout = property(getInterCharTimeout, setInterCharTimeout, doc='Inter-character timeout setting for read()')
    _SETTINGS = ('baudrate', 'bytesize', 'parity', 'stopbits', 'xonxoff', 'dsrdtr', 'rtscts', 'timeout', 'writeTimeout', 'interCharTimeout')

    def getSettingsDict(self):
        """Get current port settings as a dictionary. For use with
        applySettingsDict"""
        return dict([ (key, getattr(self, '_' + key)) for key in self._SETTINGS ])

    def applySettingsDict(self, d):
        """apply stored settings from a dictionary returned from
        getSettingsDict. it's allowed to delete keys from the dictionary. these
        values will simply left unchanged."""
        for key in self._SETTINGS:
            if d[key] != getattr(self, '_' + key):
                setattr(self, key, d[key])

    def __repr__(self):
        """String representation of the current port settings and its state."""
        return '%s<id=0x%x, open=%s>(port=%r, baudrate=%r, bytesize=%r, parity=%r, stopbits=%r, timeout=%r, xonxoff=%r, rtscts=%r, dsrdtr=%r)' % (self.__class__.__name__,
         id(self),
         self._isOpen,
         self.portstr,
         self.baudrate,
         self.bytesize,
         self.parity,
         self.stopbits,
         self.timeout,
         self.xonxoff,
         self.rtscts,
         self.dsrdtr)

    def readline(self, size = None, eol = '\n'):
        """read a line which is terminated with end-of-line (eol) character
                ('
        ' by default) or until timeout"""
        line = ''
        while 1:
            c = self.read(1)
            if c:
                line += c
                if c == eol:
                    break
                if size is not None and len(line) >= size:
                    break
            else:
                break

        return bytes(line)

    def readlines(self, sizehint = None, eol = '\n'):
        """read a list of lines, until timeout
        sizehint is ignored"""
        if self.timeout is None:
            raise ValueError('Serial port MUST have enabled timeout for this function!')
        lines = []
        while 1:
            line = self.readline(eol=eol)
            if line:
                lines.append(line)
                if line[-1] != eol:
                    break
            else:
                break

        return lines

    def xreadlines(self, sizehint = None):
        """just call readlines - here for compatibility"""
        return self.readlines()

    def readable(self):
        return True

    def writable(self):
        return True

    def seekable(self):
        return False

    def readinto(self, b):
        data = self.read(len(b))
        n = len(data)
        try:
            b[:n] = data
        except TypeError as err:
            import array
            if not isinstance(b, array.array):
                raise err
            b[:n] = array.array('b', data)

        return n


if __name__ == '__main__':
    import sys
    s = SerialBase()
    sys.stdout.write('port name:  %s\n' % s.portstr)
    sys.stdout.write('baud rates: %s\n' % s.getSupportedBaudrates())
    sys.stdout.write('byte sizes: %s\n' % s.getSupportedByteSizes())
    sys.stdout.write('parities:   %s\n' % s.getSupportedParities())
    sys.stdout.write('stop bits:  %s\n' % s.getSupportedStopbits())
    sys.stdout.write('%s\n' % s)