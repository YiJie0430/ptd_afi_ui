# Embedded file name: serial\socket_connection.pyc
from serialutil import *
import time
import socket
import logging
LOGGER_LEVELS = {'debug': logging.DEBUG,
 'info': logging.INFO,
 'warning': logging.WARNING,
 'error': logging.ERROR}

class SocketSerial(SerialBase):
    """Serial port implementation for plain sockets."""
    BAUDRATES = (50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200)

    def open(self):
        """Open port with current settings. This may throw a SerialException
        if the port cannot be opened."""
        self.logger = None
        if self._port is None:
            raise SerialException('Port must be configured before it can be used.')
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.connect(self.fromURL(self.portstr))
        except Exception as msg:
            self._socket = None
            raise SerialException('Could not open port %s: %s' % (self.portstr, msg))

        self._socket.settimeout(2)
        self._reconfigurePort()
        self._isOpen = True
        if not self._rtscts:
            self.setRTS(True)
            self.setDTR(True)
        self.flushInput()
        self.flushOutput()
        return

    def _reconfigurePort(self):
        """Set communication parameters on opened port. for the socket://
        protocol all settings are ignored!"""
        if self._socket is None:
            raise SerialException('Can only operate on open ports')
        if self.logger:
            self.logger.info('ignored port configuration change')
        return

    def close(self):
        """Close port"""
        if self._isOpen:
            if self._socket:
                try:
                    self._socket.shutdown(socket.SHUT_RDWR)
                    self._socket.close()
                except:
                    pass

                self._socket = None
            self._isOpen = False
            time.sleep(0.3)
        return

    def makeDeviceName(self, port):
        raise SerialException('there is no sensible way to turn numbers into URLs')

    def fromURL(self, url):
        """extract host and port from an URL string"""
        if url.lower().startswith('socket://'):
            url = url[9:]
        try:
            if '/' in url:
                url, options = url.split('/', 1)
                for option in options.split('/'):
                    if '=' in option:
                        option, value = option.split('=', 1)
                    else:
                        value = None
                    if option == 'logging':
                        logging.basicConfig()
                        self.logger = logging.getLogger('pySerial.socket')
                        self.logger.setLevel(LOGGER_LEVELS[value])
                        self.logger.debug('enabled logging')
                    else:
                        raise ValueError('unknown option: %r' % (option,))

            host, port = url.split(':', 1)
            port = int(port)
            if not 0 <= port < 65536:
                raise ValueError('port not in range 0...65535')
        except ValueError as e:
            raise SerialException('expected a string in the form "[rfc2217://]<host>:<port>[/option[/option...]]": %s' % e)

        return (host, port)

    def inWaiting(self):
        """Return the number of characters currently in the input buffer."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.debug('WARNING: inWaiting returns dummy value')
        return 0

    def read(self, size = 1):
        """Read size bytes from the serial port. If a timeout is set it may
        return less characters as requested. With no timeout it will block
        until the requested number of bytes is read."""
        if not self._isOpen:
            raise portNotOpenError
        data = bytearray()
        timeout = time.time() + self._timeout
        while len(data) < size and time.time() < timeout:
            try:
                data = self._socket.recv(size - len(data))
            except socket.timeout:
                continue
            except socket.error as e:
                raise SerialException('connection failed (%s)' % e)

        return bytes(data)

    def write(self, data):
        """Output the given string over the serial port. Can block if the
        connection is blocked. May raise SerialException if the connection is
        closed."""
        if not self._isOpen:
            raise portNotOpenError
        try:
            self._socket.sendall(data)
        except socket.error as e:
            raise SerialException('socket connection failed: %s' % e)

        return len(data)

    def flushInput(self):
        """Clear input buffer, discarding all that is in the buffer."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('ignored flushInput')

    def flushOutput(self):
        """Clear output buffer, aborting the current output and
        discarding all that is in the buffer."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('ignored flushOutput')

    def sendBreak(self, duration = 0.25):
        """Send break condition. Timed, returns to idle state after given
        duration."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('ignored sendBreak(%r)' % (duration,))

    def setBreak(self, level = True):
        """Set break: Controls TXD. When active, to transmitting is
        possible."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('ignored setBreak(%r)' % (level,))

    def setRTS(self, level = True):
        """Set terminal status line: Request To Send"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('ignored setRTS(%r)' % (level,))

    def setDTR(self, level = True):
        """Set terminal status line: Data Terminal Ready"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('ignored setDTR(%r)' % (level,))

    def getCTS(self):
        """Read terminal status line: Clear To Send"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for getCTS()')
        return True

    def getDSR(self):
        """Read terminal status line: Data Set Ready"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for getDSR()')
        return True

    def getRI(self):
        """Read terminal status line: Ring Indicator"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for getRI()')
        return False

    def getCD(self):
        """Read terminal status line: Carrier Detect"""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('returning dummy for getCD()')
        return True


try:
    import io
except ImportError:

    class Serial(SocketSerial, FileLike):
        pass


else:

    class Serial(SocketSerial, io.RawIOBase):
        pass


if __name__ == '__main__':
    import sys
    s = Serial('socket://localhost:7000')
    sys.stdout.write('%s\n' % s)
    sys.stdout.write('write...\n')
    s.write('hello\n')
    s.flush()
    sys.stdout.write('read: %s\n' % s.read(5))
    s.close()