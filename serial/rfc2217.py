# Embedded file name: serial\rfc2217.pyc
from serialutil import *
import time
import struct
import socket
import threading
import Queue
import logging
LOGGER_LEVELS = {'debug': logging.DEBUG,
 'info': logging.INFO,
 'warning': logging.WARNING,
 'error': logging.ERROR}
IAC = to_bytes([255])
DONT = to_bytes([254])
DO = to_bytes([253])
WONT = to_bytes([252])
WILL = to_bytes([251])
IAC_DOUBLED = to_bytes([IAC, IAC])
SE = to_bytes([240])
NOP = to_bytes([241])
DM = to_bytes([242])
BRK = to_bytes([243])
IP = to_bytes([244])
AO = to_bytes([245])
AYT = to_bytes([246])
EC = to_bytes([247])
EL = to_bytes([248])
GA = to_bytes([249])
SB = to_bytes([250])
BINARY = to_bytes([0])
ECHO = to_bytes([1])
SGA = to_bytes([3])
COM_PORT_OPTION = to_bytes([44])
SET_BAUDRATE = to_bytes([1])
SET_DATASIZE = to_bytes([2])
SET_PARITY = to_bytes([3])
SET_STOPSIZE = to_bytes([4])
SET_CONTROL = to_bytes([5])
NOTIFY_LINESTATE = to_bytes([6])
NOTIFY_MODEMSTATE = to_bytes([7])
FLOWCONTROL_SUSPEND = to_bytes([8])
FLOWCONTROL_RESUME = to_bytes([9])
SET_LINESTATE_MASK = to_bytes([10])
SET_MODEMSTATE_MASK = to_bytes([11])
PURGE_DATA = to_bytes([12])
SERVER_SET_BAUDRATE = to_bytes([101])
SERVER_SET_DATASIZE = to_bytes([102])
SERVER_SET_PARITY = to_bytes([103])
SERVER_SET_STOPSIZE = to_bytes([104])
SERVER_SET_CONTROL = to_bytes([105])
SERVER_NOTIFY_LINESTATE = to_bytes([106])
SERVER_NOTIFY_MODEMSTATE = to_bytes([107])
SERVER_FLOWCONTROL_SUSPEND = to_bytes([108])
SERVER_FLOWCONTROL_RESUME = to_bytes([109])
SERVER_SET_LINESTATE_MASK = to_bytes([110])
SERVER_SET_MODEMSTATE_MASK = to_bytes([111])
SERVER_PURGE_DATA = to_bytes([112])
RFC2217_ANSWER_MAP = {SET_BAUDRATE: SERVER_SET_BAUDRATE,
 SET_DATASIZE: SERVER_SET_DATASIZE,
 SET_PARITY: SERVER_SET_PARITY,
 SET_STOPSIZE: SERVER_SET_STOPSIZE,
 SET_CONTROL: SERVER_SET_CONTROL,
 NOTIFY_LINESTATE: SERVER_NOTIFY_LINESTATE,
 NOTIFY_MODEMSTATE: SERVER_NOTIFY_MODEMSTATE,
 FLOWCONTROL_SUSPEND: SERVER_FLOWCONTROL_SUSPEND,
 FLOWCONTROL_RESUME: SERVER_FLOWCONTROL_RESUME,
 SET_LINESTATE_MASK: SERVER_SET_LINESTATE_MASK,
 SET_MODEMSTATE_MASK: SERVER_SET_MODEMSTATE_MASK,
 PURGE_DATA: SERVER_PURGE_DATA}
SET_CONTROL_REQ_FLOW_SETTING = to_bytes([0])
SET_CONTROL_USE_NO_FLOW_CONTROL = to_bytes([1])
SET_CONTROL_USE_SW_FLOW_CONTROL = to_bytes([2])
SET_CONTROL_USE_HW_FLOW_CONTROL = to_bytes([3])
SET_CONTROL_REQ_BREAK_STATE = to_bytes([4])
SET_CONTROL_BREAK_ON = to_bytes([5])
SET_CONTROL_BREAK_OFF = to_bytes([6])
SET_CONTROL_REQ_DTR = to_bytes([7])
SET_CONTROL_DTR_ON = to_bytes([8])
SET_CONTROL_DTR_OFF = to_bytes([9])
SET_CONTROL_REQ_RTS = to_bytes([10])
SET_CONTROL_RTS_ON = to_bytes([11])
SET_CONTROL_RTS_OFF = to_bytes([12])
SET_CONTROL_REQ_FLOW_SETTING_IN = to_bytes([13])
SET_CONTROL_USE_NO_FLOW_CONTROL_IN = to_bytes([14])
SET_CONTROL_USE_SW_FLOW_CONTOL_IN = to_bytes([15])
SET_CONTROL_USE_HW_FLOW_CONTOL_IN = to_bytes([16])
SET_CONTROL_USE_DCD_FLOW_CONTROL = to_bytes([17])
SET_CONTROL_USE_DTR_FLOW_CONTROL = to_bytes([18])
SET_CONTROL_USE_DSR_FLOW_CONTROL = to_bytes([19])
LINESTATE_MASK_TIMEOUT = 128
LINESTATE_MASK_SHIFTREG_EMPTY = 64
LINESTATE_MASK_TRANSREG_EMPTY = 32
LINESTATE_MASK_BREAK_DETECT = 16
LINESTATE_MASK_FRAMING_ERROR = 8
LINESTATE_MASK_PARTIY_ERROR = 4
LINESTATE_MASK_OVERRUN_ERROR = 2
LINESTATE_MASK_DATA_READY = 1
MODEMSTATE_MASK_CD = 128
MODEMSTATE_MASK_RI = 64
MODEMSTATE_MASK_DSR = 32
MODEMSTATE_MASK_CTS = 16
MODEMSTATE_MASK_CD_CHANGE = 8
MODEMSTATE_MASK_RI_CHANGE = 4
MODEMSTATE_MASK_DSR_CHANGE = 2
MODEMSTATE_MASK_CTS_CHANGE = 1
PURGE_RECEIVE_BUFFER = to_bytes([1])
PURGE_TRANSMIT_BUFFER = to_bytes([2])
PURGE_BOTH_BUFFERS = to_bytes([3])
RFC2217_PARITY_MAP = {PARITY_NONE: 1,
 PARITY_ODD: 2,
 PARITY_EVEN: 3,
 PARITY_MARK: 4,
 PARITY_SPACE: 5}
RFC2217_REVERSE_PARITY_MAP = dict(((v, k) for k, v in RFC2217_PARITY_MAP.items()))
RFC2217_STOPBIT_MAP = {STOPBITS_ONE: 1,
 STOPBITS_ONE_POINT_FIVE: 3,
 STOPBITS_TWO: 2}
RFC2217_REVERSE_STOPBIT_MAP = dict(((v, k) for k, v in RFC2217_STOPBIT_MAP.items()))
M_NORMAL = 0
M_IAC_SEEN = 1
M_NEGOTIATE = 2
REQUESTED = 'REQUESTED'
ACTIVE = 'ACTIVE'
INACTIVE = 'INACTIVE'
REALLY_INACTIVE = 'REALLY_INACTIVE'

class TelnetOption(object):
    """Manage a single telnet option, keeps track of DO/DONT WILL/WONT."""

    def __init__(self, connection, name, option, send_yes, send_no, ack_yes, ack_no, initial_state, activation_callback = None):
        """Init option.
        :param connection: connection used to transmit answers
        :param name: a readable name for debug outputs
        :param send_yes: what to send when option is to be enabled.
        :param send_no: what to send when option is to be disabled.
        :param ack_yes: what to expect when remote agrees on option.
        :param ack_no: what to expect when remote disagrees on option.
        :param initial_state: options initialized with REQUESTED are tried to
            be enabled on startup. use INACTIVE for all others.
        """
        self.connection = connection
        self.name = name
        self.option = option
        self.send_yes = send_yes
        self.send_no = send_no
        self.ack_yes = ack_yes
        self.ack_no = ack_no
        self.state = initial_state
        self.active = False
        self.activation_callback = activation_callback

    def __repr__(self):
        """String for debug outputs"""
        return '%s:%s(%s)' % (self.name, self.active, self.state)

    def process_incoming(self, command):
        """A DO/DONT/WILL/WONT was received for this option, update state and
        answer when needed."""
        if command == self.ack_yes:
            if self.state is REQUESTED:
                self.state = ACTIVE
                self.active = True
                if self.activation_callback is not None:
                    self.activation_callback()
            elif self.state is ACTIVE:
                pass
            elif self.state is INACTIVE:
                self.state = ACTIVE
                self.connection.telnetSendOption(self.send_yes, self.option)
                self.active = True
                if self.activation_callback is not None:
                    self.activation_callback()
            elif self.state is REALLY_INACTIVE:
                self.connection.telnetSendOption(self.send_no, self.option)
            else:
                raise ValueError('option in illegal state %r' % self)
        elif command == self.ack_no:
            if self.state is REQUESTED:
                self.state = INACTIVE
                self.active = False
            elif self.state is ACTIVE:
                self.state = INACTIVE
                self.connection.telnetSendOption(self.send_no, self.option)
                self.active = False
            elif self.state is INACTIVE:
                pass
            elif self.state is REALLY_INACTIVE:
                pass
            else:
                raise ValueError('option in illegal state %r' % self)
        return


class TelnetSubnegotiation(object):
    """A object to handle subnegotiation of options. In this case actually
    sub-sub options for RFC 2217. It is used to track com port options."""

    def __init__(self, connection, name, option, ack_option = None):
        if ack_option is None:
            ack_option = option
        self.connection = connection
        self.name = name
        self.option = option
        self.value = None
        self.ack_option = ack_option
        self.state = INACTIVE
        return

    def __repr__(self):
        """String for debug outputs."""
        return '%s:%s' % (self.name, self.state)

    def set(self, value):
        """request a change of the value. a request is sent to the server. if
        the client needs to know if the change is performed he has to check the
        state of this object."""
        self.value = value
        self.state = REQUESTED
        self.connection.rfc2217SendSubnegotiation(self.option, self.value)
        if self.connection.logger:
            self.connection.logger.debug('SB Requesting %s -> %r' % (self.name, self.value))

    def isReady(self):
        """check if answer from server has been received. when server rejects
        the change, raise a ValueError."""
        if self.state == REALLY_INACTIVE:
            raise ValueError('remote rejected value for option %r' % self.name)
        return self.state == ACTIVE

    active = property(isReady)

    def wait(self, timeout = 3):
        """wait until the subnegotiation has been acknowledged or timeout. It
        can also throw a value error when the answer from the server does not
        match the value sent."""
        timeout_time = time.time() + timeout
        while time.time() < timeout_time:
            time.sleep(0.05)
            if self.isReady():
                break
        else:
            raise SerialException('timeout while waiting for option %r' % self.name)

    def checkAnswer(self, suboption):
        """check an incoming subnegotiation block. the parameter already has
        cut off the header like sub option number and com port option value."""
        if self.value == suboption[:len(self.value)]:
            self.state = ACTIVE
        else:
            self.state = REALLY_INACTIVE
        if self.connection.logger:
            self.connection.logger.debug('SB Answer %s -> %r -> %s' % (self.name, suboption, self.state))


class RFC2217Serial(SerialBase):
    """Serial port implementation for RFC 2217 remote serial ports."""
    BAUDRATES = (50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200)

    def open(self):
        """Open port with current settings. This may throw a SerialException
        if the port cannot be opened."""
        self.logger = None
        self._ignore_set_control_answer = False
        self._poll_modem_state = False
        self._network_timeout = 3
        if self._port is None:
            raise SerialException('Port must be configured before it can be used.')
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.connect(self.fromURL(self.portstr))
            self._socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        except Exception as msg:
            self._socket = None
            raise SerialException('Could not open port %s: %s' % (self.portstr, msg))

        self._socket.settimeout(5)
        self._read_buffer = Queue.Queue()
        self._write_lock = threading.Lock()
        mandadory_options = [TelnetOption(self, 'we-BINARY', BINARY, WILL, WONT, DO, DONT, INACTIVE), TelnetOption(self, 'we-RFC2217', COM_PORT_OPTION, WILL, WONT, DO, DONT, REQUESTED)]
        self._telnet_options = [TelnetOption(self, 'ECHO', ECHO, DO, DONT, WILL, WONT, REQUESTED),
         TelnetOption(self, 'we-SGA', SGA, WILL, WONT, DO, DONT, REQUESTED),
         TelnetOption(self, 'they-SGA', SGA, DO, DONT, WILL, WONT, REQUESTED),
         TelnetOption(self, 'they-BINARY', BINARY, DO, DONT, WILL, WONT, INACTIVE),
         TelnetOption(self, 'they-RFC2217', COM_PORT_OPTION, DO, DONT, WILL, WONT, REQUESTED)] + mandadory_options
        self._rfc2217_port_settings = {'baudrate': TelnetSubnegotiation(self, 'baudrate', SET_BAUDRATE, SERVER_SET_BAUDRATE),
         'datasize': TelnetSubnegotiation(self, 'datasize', SET_DATASIZE, SERVER_SET_DATASIZE),
         'parity': TelnetSubnegotiation(self, 'parity', SET_PARITY, SERVER_SET_PARITY),
         'stopsize': TelnetSubnegotiation(self, 'stopsize', SET_STOPSIZE, SERVER_SET_STOPSIZE)}
        self._rfc2217_options = {'purge': TelnetSubnegotiation(self, 'purge', PURGE_DATA, SERVER_PURGE_DATA),
         'control': TelnetSubnegotiation(self, 'control', SET_CONTROL, SERVER_SET_CONTROL)}
        self._rfc2217_options.update(self._rfc2217_port_settings)
        self._linestate = 0
        self._modemstate = None
        self._modemstate_expires = 0
        self._remote_suspend_flow = False
        self._thread = threading.Thread(target=self._telnetReadLoop)
        self._thread.setDaemon(True)
        self._thread.setName('pySerial RFC 2217 reader thread for %s' % (self._port,))
        self._thread.start()
        for option in self._telnet_options:
            if option.state is REQUESTED:
                self.telnetSendOption(option.send_yes, option.option)

        timeout_time = time.time() + self._network_timeout
        while time.time() < timeout_time:
            time.sleep(0.05)
            if sum((o.active for o in mandadory_options)) == len(mandadory_options):
                break
        else:
            raise SerialException('Remote does not seem to support RFC2217 or BINARY mode %r' % mandadory_options)

        if self.logger:
            self.logger.info('Negotiated options: %s' % self._telnet_options)
        self._reconfigurePort()
        self._isOpen = True
        if not self._rtscts:
            self.setRTS(True)
            self.setDTR(True)
        self.flushInput()
        self.flushOutput()
        return

    def _reconfigurePort(self):
        """Set communication parameters on opened port."""
        if self._socket is None:
            raise SerialException('Can only operate on open ports')
        if self._writeTimeout is not None:
            raise NotImplementedError('writeTimeout is currently not supported')
        if not isinstance(self._baudrate, (int, long)) or not 0 < self._baudrate < 4294967296L:
            raise ValueError('invalid baudrate: %r' % self._baudrate)
        self._rfc2217_port_settings['baudrate'].set(struct.pack('!I', self._baudrate))
        self._rfc2217_port_settings['datasize'].set(struct.pack('!B', self._bytesize))
        self._rfc2217_port_settings['parity'].set(struct.pack('!B', RFC2217_PARITY_MAP[self._parity]))
        self._rfc2217_port_settings['stopsize'].set(struct.pack('!B', RFC2217_STOPBIT_MAP[self._stopbits]))
        items = self._rfc2217_port_settings.values()
        if self.logger:
            self.logger.debug('Negotiating settings: %s' % (items,))
        timeout_time = time.time() + self._network_timeout
        while time.time() < timeout_time:
            time.sleep(0.05)
            if sum((o.active for o in items)) == len(items):
                break
        else:
            raise SerialException('Remote does not accept parameter change (RFC2217): %r' % items)

        if self.logger:
            self.logger.info('Negotiated settings: %s' % (items,))
        if self._rtscts and self._xonxoff:
            raise ValueError('xonxoff and rtscts together are not supported')
        elif self._rtscts:
            self.rfc2217SetControl(SET_CONTROL_USE_HW_FLOW_CONTROL)
        elif self._xonxoff:
            self.rfc2217SetControl(SET_CONTROL_USE_SW_FLOW_CONTROL)
        else:
            self.rfc2217SetControl(SET_CONTROL_USE_NO_FLOW_CONTROL)
        return

    def close(self):
        """Close port"""
        if self._isOpen:
            if self._socket:
                try:
                    self._socket.shutdown(socket.SHUT_RDWR)
                    self._socket.close()
                except:
                    pass

                self._socket = None
            if self._thread:
                self._thread.join()
            self._isOpen = False
            time.sleep(0.3)
        return

    def makeDeviceName(self, port):
        raise SerialException('there is no sensible way to turn numbers into URLs')

    def fromURL(self, url):
        """extract host and port from an URL string"""
        if url.lower().startswith('rfc2217://'):
            url = url[10:]
        try:
            if '/' in url:
                url, options = url.split('/', 1)
                for option in options.split('/'):
                    if '=' in option:
                        option, value = option.split('=', 1)
                    else:
                        value = None
                    if option == 'logging':
                        logging.basicConfig()
                        self.logger = logging.getLogger('pySerial.rfc2217')
                        self.logger.setLevel(LOGGER_LEVELS[value])
                        self.logger.debug('enabled logging')
                    elif option == 'ign_set_control':
                        self._ignore_set_control_answer = True
                    elif option == 'poll_modem':
                        self._poll_modem_state = True
                    elif option == 'timeout':
                        self._network_timeout = float(value)
                    else:
                        raise ValueError('unknown option: %r' % (option,))

            host, port = url.split(':', 1)
            port = int(port)
            if not 0 <= port < 65536:
                raise ValueError('port not in range 0...65535')
        except ValueError as e:
            raise SerialException('expected a string in the form "[rfc2217://]<host>:<port>[/option[/option...]]": %s' % e)

        return (host, port)

    def inWaiting(self):
        """Return the number of characters currently in the input buffer."""
        if not self._isOpen:
            raise portNotOpenError
        return self._read_buffer.qsize()

    def read(self, size = 1):
        """Read size bytes from the serial port. If a timeout is set it may
        return less characters as requested. With no timeout it will block
        until the requested number of bytes is read."""
        if not self._isOpen:
            raise portNotOpenError
        data = bytearray()
        try:
            while len(data) < size:
                if self._thread is None:
                    raise SerialException('connection failed (reader thread died)')
                data.append(self._read_buffer.get(True, self._timeout))

        except Queue.Empty:
            pass

        return bytes(data)

    def write(self, data):
        """Output the given string over the serial port. Can block if the
        connection is blocked. May raise SerialException if the connection is
        closed."""
        if not self._isOpen:
            raise portNotOpenError
        self._write_lock.acquire()
        try:
            self._socket.sendall(data.replace(IAC, IAC_DOUBLED))
        except socket.error as e:
            raise SerialException('socket connection failed: %s' % e)
        finally:
            self._write_lock.release()

        return len(data)

    def flushInput(self):
        """Clear input buffer, discarding all that is in the buffer."""
        if not self._isOpen:
            raise portNotOpenError
        self.rfc2217SendPurge(PURGE_RECEIVE_BUFFER)
        while self._read_buffer.qsize():
            self._read_buffer.get(False)

    def flushOutput(self):
        """Clear output buffer, aborting the current output and
        discarding all that is in the buffer."""
        if not self._isOpen:
            raise portNotOpenError
        self.rfc2217SendPurge(PURGE_TRANSMIT_BUFFER)

    def sendBreak(self, duration = 0.25):
        """Send break condition. Timed, returns to idle state after given
        duration."""
        if not self._isOpen:
            raise portNotOpenError
        self.setBreak(True)
        time.sleep(duration)
        self.setBreak(False)

    def setBreak(self, level = True):
        """Set break: Controls TXD. When active, to transmitting is
        possible."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('set BREAK to %s' % ('inactive', 'active')[bool(level)])
        if level:
            self.rfc2217SetControl(SET_CONTROL_BREAK_ON)
        else:
            self.rfc2217SetControl(SET_CONTROL_BREAK_OFF)

    def setRTS(self, level = True):
        """Set terminal status line: Request To Send."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('set RTS to %s' % ('inactive', 'active')[bool(level)])
        if level:
            self.rfc2217SetControl(SET_CONTROL_RTS_ON)
        else:
            self.rfc2217SetControl(SET_CONTROL_RTS_OFF)

    def setDTR(self, level = True):
        """Set terminal status line: Data Terminal Ready."""
        if not self._isOpen:
            raise portNotOpenError
        if self.logger:
            self.logger.info('set DTR to %s' % ('inactive', 'active')[bool(level)])
        if level:
            self.rfc2217SetControl(SET_CONTROL_DTR_ON)
        else:
            self.rfc2217SetControl(SET_CONTROL_DTR_OFF)

    def getCTS(self):
        """Read terminal status line: Clear To Send."""
        if not self._isOpen:
            raise portNotOpenError
        return bool(self.getModemState() & MODEMSTATE_MASK_CTS)

    def getDSR(self):
        """Read terminal status line: Data Set Ready."""
        if not self._isOpen:
            raise portNotOpenError
        return bool(self.getModemState() & MODEMSTATE_MASK_DSR)

    def getRI(self):
        """Read terminal status line: Ring Indicator."""
        if not self._isOpen:
            raise portNotOpenError
        return bool(self.getModemState() & MODEMSTATE_MASK_RI)

    def getCD(self):
        """Read terminal status line: Carrier Detect."""
        if not self._isOpen:
            raise portNotOpenError
        return bool(self.getModemState() & MODEMSTATE_MASK_CD)

    def _telnetReadLoop(self):
        """read loop for the socket."""
        mode = M_NORMAL
        suboption = None
        try:
            while self._socket is not None:
                try:
                    data = self._socket.recv(1024)
                except socket.timeout:
                    continue
                except socket.error:
                    break

                for byte in data:
                    if mode == M_NORMAL:
                        if byte == IAC:
                            mode = M_IAC_SEEN
                        elif suboption is not None:
                            suboption.append(byte)
                        else:
                            self._read_buffer.put(byte)
                    elif mode == M_IAC_SEEN:
                        if byte == IAC:
                            if suboption is not None:
                                suboption.append(IAC)
                            else:
                                self._read_buffer.put(IAC)
                            mode = M_NORMAL
                        elif byte == SB:
                            suboption = bytearray()
                            mode = M_NORMAL
                        elif byte == SE:
                            self._telnetProcessSubnegotiation(bytes(suboption))
                            suboption = None
                            mode = M_NORMAL
                        elif byte in (DO,
                         DONT,
                         WILL,
                         WONT):
                            telnet_command = byte
                            mode = M_NEGOTIATE
                        else:
                            self._telnetProcessCommand(byte)
                            mode = M_NORMAL
                    elif mode == M_NEGOTIATE:
                        self._telnetNegotiateOption(telnet_command, byte)
                        mode = M_NORMAL

        finally:
            self._thread = None
            if self.logger:
                self.logger.debug('read thread terminated')

        return

    def _telnetProcessCommand(self, command):
        """Process commands other than DO, DONT, WILL, WONT."""
        if self.logger:
            self.logger.warning('ignoring Telnet command: %r' % (command,))

    def _telnetNegotiateOption(self, command, option):
        """Process incoming DO, DONT, WILL, WONT."""
        known = False
        for item in self._telnet_options:
            if item.option == option:
                item.process_incoming(command)
                known = True

        if not known:
            if command == WILL or command == DO:
                self.telnetSendOption(command == WILL and DONT or WONT, option)
                if self.logger:
                    self.logger.warning('rejected Telnet option: %r' % (option,))

    def _telnetProcessSubnegotiation(self, suboption):
        """Process subnegotiation, the data between IAC SB and IAC SE."""
        if suboption[0:1] == COM_PORT_OPTION:
            if suboption[1:2] == SERVER_NOTIFY_LINESTATE and len(suboption) >= 3:
                self._linestate = ord(suboption[2:3])
                if self.logger:
                    self.logger.info('NOTIFY_LINESTATE: %s' % self._linestate)
            elif suboption[1:2] == SERVER_NOTIFY_MODEMSTATE and len(suboption) >= 3:
                self._modemstate = ord(suboption[2:3])
                if self.logger:
                    self.logger.info('NOTIFY_MODEMSTATE: %s' % self._modemstate)
                self._modemstate_expires = time.time() + 0.3
            elif suboption[1:2] == FLOWCONTROL_SUSPEND:
                self._remote_suspend_flow = True
            elif suboption[1:2] == FLOWCONTROL_RESUME:
                self._remote_suspend_flow = False
            else:
                for item in self._rfc2217_options.values():
                    if item.ack_option == suboption[1:2]:
                        item.checkAnswer(bytes(suboption[2:]))
                        break
                else:
                    if self.logger:
                        self.logger.warning('ignoring COM_PORT_OPTION: %r' % (suboption,))
        elif self.logger:
            self.logger.warning('ignoring subnegotiation: %r' % (suboption,))

    def _internal_raw_write(self, data):
        """internal socket write with no data escaping. used to send telnet stuff."""
        self._write_lock.acquire()
        try:
            self._socket.sendall(data)
        finally:
            self._write_lock.release()

    def telnetSendOption(self, action, option):
        """Send DO, DONT, WILL, WONT."""
        self._internal_raw_write(to_bytes([IAC, action, option]))

    def rfc2217SendSubnegotiation(self, option, value = ''):
        """Subnegotiation of RFC2217 parameters."""
        value = value.replace(IAC, IAC_DOUBLED)
        self._internal_raw_write(to_bytes([IAC,
         SB,
         COM_PORT_OPTION,
         option] + list(value) + [IAC, SE]))

    def rfc2217SendPurge(self, value):
        item = self._rfc2217_options['purge']
        item.set(value)
        item.wait(self._network_timeout)

    def rfc2217SetControl(self, value):
        item = self._rfc2217_options['control']
        item.set(value)
        if self._ignore_set_control_answer:
            time.sleep(0.1)
        else:
            item.wait(self._network_timeout)

    def rfc2217FlowServerReady(self):
        """check if server is ready to receive data. block for some time when
        not."""
        pass

    def getModemState(self):
        """get last modem state (cached value. if value is "old", request a new
        one. this cache helps that we don't issue to many requests when e.g. all
        status lines, one after the other is queried by te user (getCTS, getDSR
        etc.)"""
        if self._poll_modem_state and self._modemstate_expires < time.time():
            if self.logger:
                self.logger.debug('polling modem state')
            self.rfc2217SendSubnegotiation(NOTIFY_MODEMSTATE)
            timeout_time = time.time() + self._network_timeout
            while time.time() < timeout_time:
                time.sleep(0.05)
                if self._modemstate_expires > time.time():
                    if self.logger:
                        self.logger.warning('poll for modem state failed')
                    break

        if self._modemstate is not None:
            if self.logger:
                self.logger.debug('using cached modem state')
            return self._modemstate
        else:
            raise SerialException('remote sends no NOTIFY_MODEMSTATE')
        return


try:
    import io
except ImportError:

    class Serial(RFC2217Serial, FileLike):
        pass


else:

    class Serial(RFC2217Serial, io.RawIOBase):
        pass


class PortManager(object):
    """This class manages the state of Telnet and RFC 2217. It needs a serial
    instance and a connection to work with. connection is expected to implement
    a (thread safe) write function, that writes the string to the network."""

    def __init__(self, serial_port, connection, logger = None):
        self.serial = serial_port
        self.connection = connection
        self.logger = logger
        self._client_is_rfc2217 = False
        self.mode = M_NORMAL
        self.suboption = None
        self.telnet_command = None
        self.modemstate_mask = 255
        self.last_modemstate = None
        self.linstate_mask = 0
        self._telnet_options = [TelnetOption(self, 'ECHO', ECHO, WILL, WONT, DO, DONT, REQUESTED),
         TelnetOption(self, 'we-SGA', SGA, WILL, WONT, DO, DONT, REQUESTED),
         TelnetOption(self, 'they-SGA', SGA, DO, DONT, WILL, WONT, INACTIVE),
         TelnetOption(self, 'we-BINARY', BINARY, WILL, WONT, DO, DONT, INACTIVE),
         TelnetOption(self, 'they-BINARY', BINARY, DO, DONT, WILL, WONT, REQUESTED),
         TelnetOption(self, 'we-RFC2217', COM_PORT_OPTION, WILL, WONT, DO, DONT, REQUESTED, self._client_ok),
         TelnetOption(self, 'they-RFC2217', COM_PORT_OPTION, DO, DONT, WILL, WONT, INACTIVE, self._client_ok)]
        if self.logger:
            self.logger.debug('requesting initial Telnet/RFC 2217 options')
        for option in self._telnet_options:
            if option.state is REQUESTED:
                self.telnetSendOption(option.send_yes, option.option)

        return

    def _client_ok(self):
        """callback of telnet option. it gets called when option is activated.
        this one here is used to detect when the client agrees on RFC 2217. a
        flag is set so that other functions like check_modem_lines know if the
        client is ok."""
        self._client_is_rfc2217 = True
        if self.logger:
            self.logger.info('client accepts RFC 2217')
        self.check_modem_lines(force_notification=True)

    def telnetSendOption(self, action, option):
        """Send DO, DONT, WILL, WONT."""
        self.connection.write(to_bytes([IAC, action, option]))

    def rfc2217SendSubnegotiation(self, option, value = ''):
        """Subnegotiation of RFC 2217 parameters."""
        value = value.replace(IAC, IAC_DOUBLED)
        self.connection.write(to_bytes([IAC,
         SB,
         COM_PORT_OPTION,
         option] + list(value) + [IAC, SE]))

    def check_modem_lines(self, force_notification = False):
        if self.serial.getCTS():
            modemstate = MODEMSTATE_MASK_CTS | (self.serial.getDSR() and MODEMSTATE_MASK_DSR) | (self.serial.getRI() and MODEMSTATE_MASK_RI) | (self.serial.getCD() and MODEMSTATE_MASK_CD)
            deltas = modemstate ^ (self.last_modemstate or 0)
            if deltas & MODEMSTATE_MASK_CTS:
                modemstate |= MODEMSTATE_MASK_CTS_CHANGE
            if deltas & MODEMSTATE_MASK_DSR:
                modemstate |= MODEMSTATE_MASK_DSR_CHANGE
            if deltas & MODEMSTATE_MASK_RI:
                modemstate |= MODEMSTATE_MASK_RI_CHANGE
            if deltas & MODEMSTATE_MASK_CD:
                modemstate |= MODEMSTATE_MASK_CD_CHANGE
            if modemstate != self.last_modemstate or force_notification:
                if self._client_is_rfc2217 and modemstate & self.modemstate_mask or force_notification:
                    self.rfc2217SendSubnegotiation(SERVER_NOTIFY_MODEMSTATE, to_bytes([modemstate & self.modemstate_mask]))
                    self.logger and self.logger.info('NOTIFY_MODEMSTATE: %s' % (modemstate,))
            self.last_modemstate = modemstate & 240

    def escape(self, data):
        """this function is for the user. all outgoing data has to be properly
        escaped, so that no IAC character in the data stream messes up the
        Telnet state machine in the server.
        
        socket.sendall(escape(data))
        """
        for byte in data:
            if byte == IAC:
                yield IAC
                yield IAC
            else:
                yield byte

    def filter(self, data):
        """handle a bunch of incoming bytes. this is a generator. it will yield
        all characters not of interest for Telnet/RFC 2217.
        
        The idea is that the reader thread pushes data from the socket through
        this filter:
        
        for byte in filter(socket.recv(1024)):
            # do things like CR/LF conversion/whatever
            # and write data to the serial port
            serial.write(byte)
        
        (socket error handling code left as exercise for the reader)
        """
        for byte in data:
            if self.mode == M_NORMAL:
                if byte == IAC:
                    self.mode = M_IAC_SEEN
                elif self.suboption is not None:
                    self.suboption.append(byte)
                else:
                    yield byte
            elif self.mode == M_IAC_SEEN:
                if byte == IAC:
                    if self.suboption is not None:
                        self.suboption.append(byte)
                    else:
                        yield byte
                    self.mode = M_NORMAL
                elif byte == SB:
                    self.suboption = bytearray()
                    self.mode = M_NORMAL
                elif byte == SE:
                    self._telnetProcessSubnegotiation(bytes(self.suboption))
                    self.suboption = None
                    self.mode = M_NORMAL
                elif byte in (DO,
                 DONT,
                 WILL,
                 WONT):
                    self.telnet_command = byte
                    self.mode = M_NEGOTIATE
                else:
                    self._telnetProcessCommand(byte)
                    self.mode = M_NORMAL
            elif self.mode == M_NEGOTIATE:
                self._telnetNegotiateOption(self.telnet_command, byte)
                self.mode = M_NORMAL

        return

    def _telnetProcessCommand(self, command):
        """Process commands other than DO, DONT, WILL, WONT."""
        if self.logger:
            self.logger.warning('ignoring Telnet command: %r' % (command,))

    def _telnetNegotiateOption(self, command, option):
        """Process incoming DO, DONT, WILL, WONT."""
        known = False
        for item in self._telnet_options:
            if item.option == option:
                item.process_incoming(command)
                known = True

        if not known:
            if command == WILL or command == DO:
                self.telnetSendOption(command == WILL and DONT or WONT, option)
                if self.logger:
                    self.logger.warning('rejected Telnet option: %r' % (option,))

    def _telnetProcessSubnegotiation(self, suboption):
        """Process subnegotiation, the data between IAC SB and IAC SE."""
        if suboption[0:1] == COM_PORT_OPTION:
            if self.logger:
                self.logger.debug('received COM_PORT_OPTION: %r' % (suboption,))
            if suboption[1:2] == SET_BAUDRATE:
                backup = self.serial.baudrate
                try:
                    self.serial.baudrate, = struct.unpack('!I', suboption[2:6])
                except ValueError as e:
                    if self.logger:
                        self.logger.error('failed to set baud rate: %s' % (e,))
                    self.serial.baudrate = backup
                else:
                    if self.logger:
                        self.logger.info('changed baud rate: %s' % (self.serial.baudrate,))

                self.rfc2217SendSubnegotiation(SERVER_SET_BAUDRATE, struct.pack('!I', self.serial.baudrate))
            elif suboption[1:2] == SET_DATASIZE:
                backup = self.serial.bytesize
                try:
                    self.serial.bytesize, = struct.unpack('!B', suboption[2:3])
                except ValueError as e:
                    if self.logger:
                        self.logger.error('failed to set data size: %s' % (e,))
                    self.serial.bytesize = backup
                else:
                    if self.logger:
                        self.logger.info('changed data size: %s' % (self.serial.bytesize,))

                self.rfc2217SendSubnegotiation(SERVER_SET_DATASIZE, struct.pack('!B', self.serial.bytesize))
            elif suboption[1:2] == SET_PARITY:
                backup = self.serial.parity
                try:
                    self.serial.parity = RFC2217_REVERSE_PARITY_MAP[struct.unpack('!B', suboption[2:3])[0]]
                except ValueError as e:
                    if self.logger:
                        self.logger.error('failed to set parity: %s' % (e,))
                    self.serial.parity = backup
                else:
                    if self.logger:
                        self.logger.info('changed parity: %s' % (self.serial.parity,))

                self.rfc2217SendSubnegotiation(SERVER_SET_PARITY, struct.pack('!B', RFC2217_PARITY_MAP[self.serial.parity]))
            elif suboption[1:2] == SET_STOPSIZE:
                backup = self.serial.stopbits
                try:
                    self.serial.stopbits = RFC2217_REVERSE_STOPBIT_MAP[struct.unpack('!B', suboption[2:3])[0]]
                except ValueError as e:
                    if self.logger:
                        self.logger.error('failed to set stop bits: %s' % (e,))
                    self.serial.stopbits = backup
                else:
                    if self.logger:
                        self.logger.info('changed stop bits: %s' % (self.serial.stopbits,))

                self.rfc2217SendSubnegotiation(SERVER_SET_STOPSIZE, struct.pack('!B', RFC2217_STOPBIT_MAP[self.serial.stopbits]))
            elif suboption[1:2] == SET_CONTROL:
                if suboption[2:3] == SET_CONTROL_REQ_FLOW_SETTING:
                    if self.serial.xonxoff:
                        self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_USE_SW_FLOW_CONTROL)
                    elif self.serial.rtscts:
                        self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_USE_HW_FLOW_CONTROL)
                    else:
                        self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_USE_NO_FLOW_CONTROL)
                elif suboption[2:3] == SET_CONTROL_USE_NO_FLOW_CONTROL:
                    self.serial.xonxoff = False
                    self.serial.rtscts = False
                    if self.logger:
                        self.logger.info('changed flow control to None')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_USE_NO_FLOW_CONTROL)
                elif suboption[2:3] == SET_CONTROL_USE_SW_FLOW_CONTROL:
                    self.serial.xonxoff = True
                    if self.logger:
                        self.logger.info('changed flow control to XON/XOFF')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_USE_SW_FLOW_CONTROL)
                elif suboption[2:3] == SET_CONTROL_USE_HW_FLOW_CONTROL:
                    self.serial.rtscts = True
                    if self.logger:
                        self.logger.info('changed flow control to RTS/CTS')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_USE_HW_FLOW_CONTROL)
                elif suboption[2:3] == SET_CONTROL_REQ_BREAK_STATE:
                    if self.logger:
                        self.logger.warning('requested break state - not implemented')
                elif suboption[2:3] == SET_CONTROL_BREAK_ON:
                    self.serial.setBreak(True)
                    if self.logger:
                        self.logger.info('changed BREAK to active')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_BREAK_ON)
                elif suboption[2:3] == SET_CONTROL_BREAK_OFF:
                    self.serial.setBreak(False)
                    if self.logger:
                        self.logger.info('changed BREAK to inactive')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_BREAK_OFF)
                elif suboption[2:3] == SET_CONTROL_REQ_DTR:
                    if self.logger:
                        self.logger.warning('requested DTR state - not implemented')
                elif suboption[2:3] == SET_CONTROL_DTR_ON:
                    self.serial.setDTR(True)
                    if self.logger:
                        self.logger.info('changed DTR to active')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_DTR_ON)
                elif suboption[2:3] == SET_CONTROL_DTR_OFF:
                    self.serial.setDTR(False)
                    if self.logger:
                        self.logger.info('changed DTR to inactive')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_DTR_OFF)
                elif suboption[2:3] == SET_CONTROL_REQ_RTS:
                    if self.logger:
                        self.logger.warning('requested RTS state - not implemented')
                elif suboption[2:3] == SET_CONTROL_RTS_ON:
                    self.serial.setRTS(True)
                    if self.logger:
                        self.logger.info('changed RTS to active')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_RTS_ON)
                elif suboption[2:3] == SET_CONTROL_RTS_OFF:
                    self.serial.setRTS(False)
                    if self.logger:
                        self.logger.info('changed RTS to inactive')
                    self.rfc2217SendSubnegotiation(SERVER_SET_CONTROL, SET_CONTROL_RTS_OFF)
            elif suboption[1:2] == NOTIFY_LINESTATE:
                self.rfc2217SendSubnegotiation(SERVER_NOTIFY_LINESTATE, to_bytes([0]))
            elif suboption[1:2] == NOTIFY_MODEMSTATE:
                if self.logger:
                    self.logger.info('request for modem state')
                self.check_modem_lines(force_notification=True)
            elif suboption[1:2] == FLOWCONTROL_SUSPEND:
                if self.logger:
                    self.logger.info('suspend')
                self._remote_suspend_flow = True
            elif suboption[1:2] == FLOWCONTROL_RESUME:
                if self.logger:
                    self.logger.info('resume')
                self._remote_suspend_flow = False
            elif suboption[1:2] == SET_LINESTATE_MASK:
                self.linstate_mask = ord(suboption[2:3])
                if self.logger:
                    self.logger.info('line state mask: 0x%02x' % (self.linstate_mask,))
            elif suboption[1:2] == SET_MODEMSTATE_MASK:
                self.modemstate_mask = ord(suboption[2:3])
                if self.logger:
                    self.logger.info('modem state mask: 0x%02x' % (self.modemstate_mask,))
            elif suboption[1:2] == PURGE_DATA:
                if suboption[2:3] == PURGE_RECEIVE_BUFFER:
                    self.serial.flushInput()
                    if self.logger:
                        self.logger.info('purge in')
                    self.rfc2217SendSubnegotiation(SERVER_PURGE_DATA, PURGE_RECEIVE_BUFFER)
                elif suboption[2:3] == PURGE_TRANSMIT_BUFFER:
                    self.serial.flushOutput()
                    if self.logger:
                        self.logger.info('purge out')
                    self.rfc2217SendSubnegotiation(SERVER_PURGE_DATA, PURGE_TRANSMIT_BUFFER)
                elif suboption[2:3] == PURGE_BOTH_BUFFERS:
                    self.serial.flushInput()
                    self.serial.flushOutput()
                    if self.logger:
                        self.logger.info('purge both')
                    self.rfc2217SendSubnegotiation(SERVER_PURGE_DATA, PURGE_BOTH_BUFFERS)
                elif self.logger:
                    self.logger.error('undefined PURGE_DATA: %r' % list(suboption[2:]))
            elif self.logger:
                self.logger.error('undefined COM_PORT_OPTION: %r' % list(suboption[1:]))
        elif self.logger:
            self.logger.warning('unknown subnegotiation: %r' % (suboption,))


if __name__ == '__main__':
    import sys
    s = Serial('rfc2217://localhost:7000', 115200)
    sys.stdout.write('%s\n' % s)
    sys.stdout.write('write...\n')
    s.write('hello\n')
    s.flush()
    sys.stdout.write('read: %s\n' % s.read(5))
    s.close()