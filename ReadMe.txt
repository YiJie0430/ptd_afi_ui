Version: 1.0.8 (2020.11.08)
Author:  YiJie Wang (jasonwang@hc.hitrontech.com)

Release Note:
1. add logging with 'debug', 'info', 'warning', 'error/exception' and 'critical' level can be setup in \MainGui\loggin.ini, the log is named as 'AFI-PT.log' shall be saved in AFI script folder where user executed.
2. mitigate the freeze on MainGui
3. fix the pcbaDetect deamon with invalid port issue
4. fix AFI statemachine cannot fetch the exception form raspi
5. re-define panel colors to indecate what term in each status
6. re-define the "detect and running" algorithm

Error Code:
0000 - UART character lost
0001 - Program execution exception
0002 - Build BPI key error
0003 - read dscal table file error
0004 - write ds table failed
0005 - MES or LoGDB connect failed
0006 - get wan ip failed
0007 - get snmp failed
0008 - check information failed
0009 - check HtMsoFreq  failed
0010 - OP Scan label error
0011 - ENV Set error

101016 - Loop Current normal Fail
101017 - IdleState Voltage Fail
101018 - RingSource Fail
107003 - Upstream Power not in the range
107012 - Measure Upstream Emission Fail
107014 - Measure Downstream Emission Fail
107024 - Upstream Power incorrect
108065 - Downstream Calibration Confirm Fail
112006 - Signal Measurcement Fail
112009 - MSE value not in th range
204029 - Attenuator value measure fail
207037 - Tx Correction Fail
211030 - Telnet didn't disable
211039 - Factory Mode Enable
214027 - Check certificate
214031 - Hardware version not Match
214032 - Software version not Match
214035 - Label MAC doesn't match NVM MAC
214036 - SNMP sysDescr.0 incorrect
214072 - Wrong EEPROM version
214077 - Label SN doesn't match  NVM SN
216073 - Wireless Mac Address not Match
309040 - Command Fail
309068 - Install Vendor Fail
309069 - Install HW version Failed
309070 - Install Cable MAC Failed
311041 - Can not login
315080 - Invalid Manufacture Serial Number
317086 - Can't open SNMP private mode
402053 - LED test Fail
403042 - can not get Telematry value
405061 - Xconnect test Failed
406057 - Reset Button test Fail
414043 - Can't get mac address
414044 - Can't get SerialNunber
414051 - Check CM MAC certificate Fail
414054 - Length of MAC addr is wrong
414056 - NVM Write after Download can't finished
414063 - Set Password Error
414084 - Certificate Access time Error
418081 - USB interface traffic fail
510048 - Can't connect to gateway
510085 - Can't disconnect to target
615083 - CA key confirm Fail
E00125 - Registration Timeout
E00136 - Uspower not in Specified value
E00137 - SNR Power not in Specified value
E00138 - wireless card can't connect
E00139 - Wireless card LED Fail
E00145 - LED Failed
E00146 - USB Failed
E00147 - MAC Failed
E00148 - Upstream power to low
E00149 - Upstream power to high
E00150 - Downstream power to low
E00151 - Downstream power to high
E00153 - No Console
E00154 - Can't starting up
E00162 - Prod cbccm
E00165 - Program Fail
E00166 - LED can't blink
E00167 - Install CA Key Failed
E00170 - No such file CA name
E00175 - US Calibration table
E00176 - DHCP Demon
E00183 - EtherSwitch speed Failed
E00189 - Init fail
E00194 - Startup failed
E00206 - Password Error
E00213 - reboot fail
E00214 - RF failed
E00215 - Cable not link
E00216 - D/L Failed
E00220 - TV Code Fail
E00222 - Reset Funtion
E00225 - Reset Button fail
E00233 - MAC Check fail
E00242 - 4x4DS lock check fail
E00244 - Wireless card function fail
E00247 - USB LED fail
E00248 - Get WPA&WifiKey digit fail  
