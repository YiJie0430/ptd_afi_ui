#-*- coding:utf-8 -*-
import os
import sys
#import paramiko
import socket
import time

#sys.setdefaultencoding('utf8')

class mySSH(object):

    def __init__(self,hostname,port,username,password):
        self.ssh = paramiko.SSHClient()        
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        #self.sftp_T = paramiko.Transport((hostname,port)) 
        self.hostname = hostname
        self.port = port
        self.username = username
        self.password = password

    def send(self,command):
        try:            
        
            #print("comment = '%s'"%command)
            stdin, stdout, stderr = self.ssh.exec_command(command)
            res ,err = stdout.read(), stderr.read()
            result = res if res else err      
            return str(result)
        except Exception as msg:
            print(str(msg))
    
    def send_muti(self,command):
        try:                    
            for i in command:
                #print("comment = '%s'"%i)
                stdin, stdout, stderr = self.ssh.exec_command(i)           
            res ,err = stdout.read(), stderr.read()
            result = res if res else err
            #print(result)
            return str(result)
        except Exception as msg:
            print(str(msg))
    
    
    def outputGet(self):
        result = None
        try:
            self.ssh.connect(hostname=self.hostname, port=self.port, username=self.username, password=self.password)
            command = 'cat ~/barcode/output'
            result = self.send(command)
            command = 'echo '' > ~/barcode/output'
            self.send(command)
            self.ssh.close()
        except ValueError, msg:
            print msg
    
        return result

    
    def reBoot(self):
        self.ssh.connect(hostname=self.hostname, port=self.port, username=self.username, password=self.password)
        command = 'sudo reboot'
        result = self.send(command)
        self.ssh.close()
    
    ''' def sftpGet(self,FILEPATH,PCPATH):
        try:
            username = 'pi'
            password = 'stopbugs'
            self.sftp_T.connect(username=username,password=password)            
            self.sftp = paramiko.SFTPClient.from_transport(self.sftp_T) 
            
            #Copy a remote file (remotepath) from the SFTP server to the local host
            self.sftp.get(FILEPATH,PCPATH)            
            self.sftp_T.close()
    
        except Exception as msg:
            print(str(msg))
    

    def sftpUpLoad(self,FILEPATH,PCPATH):
        try:
            username = 'pi'
            password = 'stopbugs'
            self.sftp_T.connect(username=username, password=password)
            sftp = paramiko.SFTPClient.from_transport(self.sftp_T)
            sftp.put(PCPATH, FILEPATH)
            self.sftp_T.close()

        except Exception as msg:
            print(str(msg)) '''

class TcpSocket(object):

    def __init__(self,hostname,port):
        self.client = None
        self.hostname = hostname
        self.port = port
        self.prompt = '{}:{} listening'.format(hostname, port) 
        self.data = None

    def send(self, cmd, prompt='', timeout=30):
        self.client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.client.connect((self.hostname, self.port)) 
        
        if  self.client.recv(1024) == self.prompt:
            st = time.time()
            #self.client.sendall(cmd.encode('utf-8'))
            self.client.sendall(cmd)
            while (time.time() - st) <= timeout:
                self.data = self.client.recv(1024)
                if prompt in self.data:
                    break
            #print('{}:{}'.format(self.hostname, self.data))
            self.client.close()
        return self.data

    def read(self, timeout=1):
        self.client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.client.connect((self.hostname, self.port)) 
        if  self.client.recv(1024) == self.prompt:
            self.data = self.client.recv(1024)
            #print('recv:', self.data)
            self.client.close()
        return self.data


def main():
    term = TcpSocket('192.168.70.11', 9191)
    term.send('pcbaDetect A 192.168.70.50:27017', 'success', 1)
    #term.send('fixture MACHINE:HOME 15', 'READY}')    
    '''for i in range(10):
        _term = TcpSocket('192.168.70.70', 9179)
        _term.send('fixture HOLDERA:ON 5', '1}')    
        _term.send('fixture MOTOR:180 5', '1}')    
        _term.send('fixture CYLINDER:ON 5', '1}')    
        _term.send('fixture HOLDERB:ON 5', '1}')    
        _term.send('fixture CYLINDER:OFF 5', '1}')    
        _term.send('fixture MOTOR:0 5', '1}')    
        _term.send('fixture HOLDERA:OFF 5', '1}')    
        _term.send('fixture HOLDERB:OFF 5', '1}')'''
    '''for i in range(10):
        _term = TcpSocket('192.168.70.70', 9179)
        _term.send('fixture CYLINDER:ON 5', '1}')    
        time.sleep(3)
        _term.send('fixture CYLINDER:OFF 5', '1}')    
        time.sleep(3)
    term.send('fixture LIGHT1YELLOW:OFF 5', 'ASS}')'''

if __name__ == '__main__':
    main()