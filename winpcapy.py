# Embedded file name: winpcapy.pyc
from ctypes import *
from ctypes.util import find_library
import sys, os
import socket as sk
from ctypes import *
WIN32 = False
HAVE_REMOTE = False
if sys.platform.startswith('win'):
    WIN32 = True
    HAVE_REMOTE = True
if WIN32:
    SOCKET = c_uint
    _lib = CDLL('wpcap.dll')
else:
    SOCKET = c_int
    _lib = CDLL(find_library('pcap'))
u_short = c_ushort
bpf_int32 = c_int
u_int = c_int
bpf_u_int32 = u_int
pcap = c_void_p
pcap_dumper = c_void_p
u_char = c_ubyte
FILE = c_void_p
STRING = c_char_p

class bpf_insn(Structure):
    _fields_ = [('code', c_ushort),
     ('jt', c_ubyte),
     ('jf', c_ubyte),
     ('k', bpf_u_int32)]


class bpf_program(Structure):
    pass


bpf_program._fields_ = [('bf_len', u_int), ('bf_insns', POINTER(bpf_insn))]

class bpf_version(Structure):
    _fields_ = [('bv_major', c_ushort), ('bv_minor', c_ushort)]


class timeval(Structure):
    pass


timeval._fields_ = [('tv_sec', c_long), ('tv_usec', c_long)]

class sockaddr(Structure):
    _fields_ = [('sa_family', c_ushort), ('sa_data', c_char * 14)]


class pcap_file_header(Structure):
    _fields_ = [('magic', bpf_u_int32),
     ('version_major', u_short),
     ('version_minor', u_short),
     ('thiszone', bpf_int32),
     ('sigfigs', bpf_u_int32),
     ('snaplen', bpf_u_int32),
     ('linktype', bpf_u_int32)]


class pcap_pkthdr(Structure):
    _fields_ = [('ts', timeval), ('caplen', bpf_u_int32), ('len', bpf_u_int32)]


class pcap_stat(Structure):
    pass


_tmpList = []
_tmpList.append(('ps_recv', c_uint))
_tmpList.append(('ps_drop', c_uint))
_tmpList.append(('ps_ifdrop', c_uint))
if HAVE_REMOTE:
    _tmpList.append(('ps_capt', c_uint))
    _tmpList.append(('ps_sent', c_uint))
    _tmpList.append(('ps_netdrop', c_uint))
pcap_stat._fields_ = _tmpList

class pcap_addr(Structure):
    pass


pcap_addr._fields_ = [('next', POINTER(pcap_addr)),
 ('addr', POINTER(sockaddr)),
 ('netmask', POINTER(sockaddr)),
 ('broadaddr', POINTER(sockaddr)),
 ('dstaddr', POINTER(sockaddr))]

class pcap_if(Structure):
    pass


pcap_if._fields_ = [('next', POINTER(pcap_if)),
 ('name', STRING),
 ('description', STRING),
 ('addresses', POINTER(pcap_addr)),
 ('flags', bpf_u_int32)]
PCAP_VERSION_MAJOR = 2
PCAP_VERSION_MINOR = 4
PCAP_ERRBUF_SIZE = 256
PCAP_IF_LOOPBACK = 1
MODE_CAPT = 0
MODE_STAT = 1
pcap_t = pcap
pcap_dumper_t = pcap_dumper
pcap_if_t = pcap_if
pcap_addr_t = pcap_addr

class S_un_b(Structure):
    _fields_ = [('s_b1', c_ubyte),
     ('s_b2', c_ubyte),
     ('s_b3', c_ubyte),
     ('s_b4', c_ubyte)]


class S_un_w(Structure):
    _fields_ = [('s_wl', c_ushort), ('s_w2', c_ushort)]


class S_un(Union):
    _fields_ = [('S_un_b', S_un_b), ('S_un_w', S_un_w), ('S_addr', c_ulong)]


class in_addr(Structure):
    _fields_ = [('S_un', S_un)]


class sockaddr_in(Structure):
    _fields_ = [('sin_family', c_ushort),
     ('sin_port', c_ushort),
     ('sin_addr', in_addr),
     ('sin_zero', c_char * 8)]


class _S6_un(Union):
    _fields_ = [('_S6_u8', c_ubyte * 16), ('_S6_u16', c_ushort * 8), ('_S6_u32', c_ulong * 4)]


class in6_addr(Structure):
    _fields_ = [('_S6_un', _S6_un)]


class sockaddr_in6(Structure):
    _fields_ = [('sin6_family', c_short),
     ('sin6_port', c_ushort),
     ('sin6_flowinfo', c_ulong),
     ('sin6_addr', in6_addr),
     ('sin6_scope_id', c_ulong)]


def iptos(in_):
    return '%d.%d.%d.%d' % (in_.s_b1,
     in_.s_b2,
     in_.s_b3,
     in_.s_b4)


def ip6tos(in_):
    addr = in_.contents.sin6_addr._S6_un._S6_u16
    vals = []
    for x in range(0, 8):
        vals.append(sk.ntohs(addr[x]))

    host = '%x:%x:%x:%x:%x:%x:%x:%x' % tuple(vals)
    port = 0
    flowinfo = in_.contents.sin6_flowinfo
    scopeid = in_.contents.sin6_scope_id
    flags = sk.NI_NUMERICHOST | sk.NI_NUMERICSERV
    retAddr, retPort = sk.getnameinfo((host,
     port,
     flowinfo,
     scopeid), flags)
    return retAddr


pcap_handler = CFUNCTYPE(None, POINTER(c_ubyte), POINTER(pcap_pkthdr), POINTER(c_ubyte))
pcap_open_live = _lib.pcap_open_live
pcap_open_live.restype = POINTER(pcap_t)
pcap_open_live.argtypes = [STRING,
 c_int,
 c_int,
 c_int,
 STRING]
pcap_open_dead = _lib.pcap_open_dead
pcap_open_dead.restype = POINTER(pcap_t)
pcap_open_dead.argtypes = [c_int, c_int]
pcap_open_offline = _lib.pcap_open_offline
pcap_open_offline.restype = POINTER(pcap_t)
pcap_open_offline.argtypes = [STRING, STRING]
pcap_dump_open = _lib.pcap_dump_open
pcap_dump_open.restype = POINTER(pcap_dumper_t)
pcap_dump_open.argtypes = [POINTER(pcap_t), STRING]
pcap_setnonblock = _lib.pcap_setnonblock
pcap_setnonblock.restype = c_int
pcap_setnonblock.argtypes = [POINTER(pcap_t), c_int, STRING]
pcap_getnonblock = _lib.pcap_getnonblock
pcap_getnonblock.restype = c_int
pcap_getnonblock.argtypes = [POINTER(pcap_t), STRING]
pcap_findalldevs = _lib.pcap_findalldevs
pcap_findalldevs.restype = c_int
pcap_findalldevs.argtypes = [POINTER(POINTER(pcap_if_t)), STRING]
pcap_freealldevs = _lib.pcap_freealldevs
pcap_freealldevs.restype = None
pcap_freealldevs.argtypes = [POINTER(pcap_if_t)]
pcap_lookupdev = _lib.pcap_lookupdev
pcap_lookupdev.restype = STRING
pcap_lookupdev.argtypes = [STRING]
pcap_lookupnet = _lib.pcap_lookupnet
pcap_lookupnet.restype = c_int
pcap_lookupnet.argtypes = [STRING,
 POINTER(bpf_u_int32),
 POINTER(bpf_u_int32),
 STRING]
pcap_dispatch = _lib.pcap_dispatch
pcap_dispatch.restype = c_int
pcap_dispatch.argtypes = [POINTER(pcap_t),
 c_int,
 pcap_handler,
 POINTER(u_char)]
pcap_loop = _lib.pcap_loop
pcap_loop.restype = c_int
pcap_loop.argtypes = [POINTER(pcap_t),
 c_int,
 pcap_handler,
 POINTER(u_char)]
pcap_next = _lib.pcap_next
pcap_next.restype = POINTER(u_char)
pcap_next.argtypes = [POINTER(pcap_t), POINTER(pcap_pkthdr)]
pcap_next_ex = _lib.pcap_next_ex
pcap_next_ex.restype = c_int
pcap_next_ex.argtypes = [POINTER(pcap_t), POINTER(POINTER(pcap_pkthdr)), POINTER(POINTER(u_char))]
pcap_breakloop = _lib.pcap_breakloop
pcap_breakloop.restype = None
pcap_breakloop.argtypes = [POINTER(pcap_t)]
pcap_sendpacket = _lib.pcap_sendpacket
pcap_sendpacket.restype = c_int
pcap_sendpacket.argtypes = [POINTER(pcap_t), POINTER(u_char), c_int]
pcap_dump = _lib.pcap_dump
pcap_dump.restype = None
pcap_dump.argtypes = [POINTER(pcap_dumper_t), POINTER(pcap_pkthdr), POINTER(u_char)]
pcap_dump_ftell = _lib.pcap_dump_ftell
pcap_dump_ftell.restype = c_long
pcap_dump_ftell.argtypes = [POINTER(pcap_dumper_t)]
pcap_compile = _lib.pcap_compile
pcap_compile.restype = c_int
pcap_compile.argtypes = [POINTER(pcap_t),
 POINTER(bpf_program),
 STRING,
 c_int,
 bpf_u_int32]
pcap_compile_nopcap = _lib.pcap_compile_nopcap
pcap_compile_nopcap.restype = c_int
pcap_compile_nopcap.argtypes = [c_int,
 c_int,
 POINTER(bpf_program),
 STRING,
 c_int,
 bpf_u_int32]
pcap_setfilter = _lib.pcap_setfilter
pcap_setfilter.restype = c_int
pcap_setfilter.argtypes = [POINTER(pcap_t), POINTER(bpf_program)]
pcap_freecode = _lib.pcap_freecode
pcap_freecode.restype = None
pcap_freecode.argtypes = [POINTER(bpf_program)]
pcap_datalink = _lib.pcap_datalink
pcap_datalink.restype = c_int
pcap_datalink.argtypes = [POINTER(pcap_t)]
pcap_list_datalinks = _lib.pcap_list_datalinks
pcap_list_datalinks.restype = c_int
pcap_set_datalink = _lib.pcap_set_datalink
pcap_set_datalink.restype = c_int
pcap_set_datalink.argtypes = [POINTER(pcap_t), c_int]
pcap_datalink_name_to_val = _lib.pcap_datalink_name_to_val
pcap_datalink_name_to_val.restype = c_int
pcap_datalink_name_to_val.argtypes = [STRING]
pcap_datalink_val_to_name = _lib.pcap_datalink_val_to_name
pcap_datalink_val_to_name.restype = STRING
pcap_datalink_val_to_name.argtypes = [c_int]
pcap_datalink_val_to_description = _lib.pcap_datalink_val_to_description
pcap_datalink_val_to_description.restype = STRING
pcap_datalink_val_to_description.argtypes = [c_int]
pcap_snapshot = _lib.pcap_snapshot
pcap_snapshot.restype = c_int
pcap_snapshot.argtypes = [POINTER(pcap_t)]
pcap_is_swapped = _lib.pcap_is_swapped
pcap_is_swapped.restype = c_int
pcap_is_swapped.argtypes = [POINTER(pcap_t)]
pcap_major_version = _lib.pcap_major_version
pcap_major_version.restype = c_int
pcap_major_version.argtypes = [POINTER(pcap_t)]
pcap_minor_version = _lib.pcap_minor_version
pcap_minor_version.restype = c_int
pcap_minor_version.argtypes = [POINTER(pcap_t)]
pcap_file = _lib.pcap_file
pcap_file.restype = FILE
pcap_file.argtypes = [POINTER(pcap_t)]
pcap_stats = _lib.pcap_stats
pcap_stats.restype = c_int
pcap_stats.argtypes = [POINTER(pcap_t), POINTER(pcap_stat)]
pcap_perror = _lib.pcap_perror
pcap_perror.restype = None
pcap_perror.argtypes = [POINTER(pcap_t), STRING]
pcap_geterr = _lib.pcap_geterr
pcap_geterr.restype = STRING
pcap_geterr.argtypes = [POINTER(pcap_t)]
pcap_strerror = _lib.pcap_strerror
pcap_strerror.restype = STRING
pcap_strerror.argtypes = [c_int]
pcap_lib_version = _lib.pcap_lib_version
pcap_lib_version.restype = STRING
pcap_lib_version.argtypes = []
pcap_close = _lib.pcap_close
pcap_close.restype = None
pcap_close.argtypes = [POINTER(pcap_t)]
pcap_dump_file = _lib.pcap_dump_file
pcap_dump_file.restype = FILE
pcap_dump_file.argtypes = [POINTER(pcap_dumper_t)]
pcap_dump_flush = _lib.pcap_dump_flush
pcap_dump_flush.restype = c_int
pcap_dump_flush.argtypes = [POINTER(pcap_dumper_t)]
pcap_dump_close = _lib.pcap_dump_close
pcap_dump_close.restype = None
pcap_dump_close.argtypes = [POINTER(pcap_dumper_t)]
if WIN32:
    HANDLE = c_void_p
    PCAP_SRC_FILE = 2
    PCAP_SRC_IFLOCAL = 3
    PCAP_SRC_IFREMOTE = 4
    PCAP_SRC_FILE_STRING = 'file://'
    PCAP_SRC_IF_STRING = 'rpcap://'
    PCAP_OPENFLAG_PROMISCUOUS = 1
    PCAP_OPENFLAG_DATATX_UDP = 2
    PCAP_OPENFLAG_NOCAPTURE_RPCAP = 4
    PCAP_OPENFLAG_NOCAPTURE_LOCAL = 8
    PCAP_OPENFLAG_MAX_RESPONSIVENESS = 16
    PCAP_SAMP_NOSAMP = 0
    PCAP_SAMP_1_EVERY_N = 1
    PCAP_SAMP_FIRST_AFTER_N_MS = 2
    RPCAP_RMTAUTH_NULL = 0
    RPCAP_RMTAUTH_PWD = 1
    PCAP_BUF_SIZE = 1024
    RPCAP_HOSTLIST_SIZE = 1024

    class pcap_send_queue(Structure):
        _fields_ = [('maxlen', c_uint), ('len', c_uint), ('buffer', c_char_p)]


    class pcap_rmtauth(Structure):
        _fields_ = [('type', c_int), ('username', c_char_p), ('password', c_char_p)]


    class pcap_samp(Structure):
        _fields_ = [('method', c_int), ('value', c_int)]


    pcap_offline_filter = _lib.pcap_offline_filter
    pcap_offline_filter.restype = c_short
    pcap_offline_filter.argtypes = [POINTER(bpf_program), POINTER(pcap_pkthdr), POINTER(u_char)]
    pcap_live_dump = _lib.pcap_live_dump
    pcap_live_dump.restype = c_int
    pcap_live_dump.argtypes = [POINTER(pcap_t),
     POINTER(c_char),
     c_int,
     c_int]
    pcap_live_dump_ended = _lib.pcap_live_dump_ended
    pcap_live_dump_ended.restype = c_int
    pcap_live_dump_ended.argtypes = [POINTER(pcap_t), c_int]
    pcap_stats_ex = _lib.pcap_stats_ex
    pcap_stats_ex.restype = POINTER(pcap_stat)
    pcap_stats_ex.argtypes = [POINTER(pcap_t), POINTER(c_int)]
    pcap_setbuff = _lib.pcap_setbuff
    pcap_setbuff.restype = c_int
    pcap_setbuff.argtypes = [POINTER(pcap_t), c_int]
    pcap_setmode = _lib.pcap_setmode
    pcap_setmode.restype = c_int
    pcap_setmode.argtypes = [POINTER(pcap_t), c_int]
    pcap_setmintocopy = _lib.pcap_setmintocopy
    pcap_setmintocopy.restype = c_int
    pcap_setmintocopy.argtype = [POINTER(pcap_t), c_int]
    pcap_getevent = _lib.pcap_getevent
    pcap_getevent.restype = HANDLE
    pcap_getevent.argtypes = [POINTER(pcap_t)]
    pcap_sendqueue_alloc = _lib.pcap_sendqueue_alloc
    pcap_sendqueue_alloc.restype = POINTER(pcap_send_queue)
    pcap_sendqueue_alloc.argtypes = [c_uint]
    pcap_sendqueue_destroy = _lib.pcap_sendqueue_destroy
    pcap_sendqueue_destroy.restype = None
    pcap_sendqueue_destroy.argtypes = [POINTER(pcap_send_queue)]
    pcap_sendqueue_queue = _lib.pcap_sendqueue_queue
    pcap_sendqueue_queue.restype = c_int
    pcap_sendqueue_queue.argtypes = [POINTER(pcap_send_queue), POINTER(pcap_pkthdr), POINTER(u_char)]
    pcap_sendqueue_transmit = _lib.pcap_sendqueue_transmit
    pcap_sendqueue_transmit.retype = u_int
    pcap_sendqueue_transmit.argtypes = [POINTER(pcap_t), POINTER(pcap_send_queue), c_int]
    pcap_findalldevs_ex = _lib.pcap_findalldevs_ex
    pcap_findalldevs_ex.retype = c_int
    pcap_findalldevs_ex.argtypes = [STRING,
     POINTER(pcap_rmtauth),
     POINTER(POINTER(pcap_if_t)),
     STRING]
    pcap_createsrcstr = _lib.pcap_createsrcstr
    pcap_createsrcstr.restype = c_int
    pcap_createsrcstr.argtypes = [STRING,
     c_int,
     STRING,
     STRING,
     STRING,
     STRING]
    pcap_parsesrcstr = _lib.pcap_parsesrcstr
    pcap_parsesrcstr.retype = c_int
    pcap_parsesrcstr.argtypes = [STRING,
     POINTER(c_int),
     STRING,
     STRING,
     STRING,
     STRING]
    pcap_open = _lib.pcap_open
    pcap_open.restype = POINTER(pcap_t)
    pcap_open.argtypes = [STRING,
     c_int,
     c_int,
     c_int,
     POINTER(pcap_rmtauth),
     STRING]
    pcap_setsampling = _lib.pcap_setsampling
    pcap_setsampling.restype = POINTER(pcap_samp)
    pcap_setsampling.argtypes = [POINTER(pcap_t)]
    pcap_remoteact_accept = _lib.pcap_remoteact_accept
    pcap_remoteact_accept.restype = SOCKET
    pcap_remoteact_accept.argtypes = [STRING,
     STRING,
     STRING,
     STRING,
     POINTER(pcap_rmtauth),
     STRING]
    pcap_remoteact_close = _lib.pcap_remoteact_close
    pcap_remoteact_close.restypes = c_int
    pcap_remoteact_close.argtypes = [STRING, STRING]
    pcap_remoteact_cleanup = _lib.pcap_remoteact_cleanup
    pcap_remoteact_cleanup.restypes = None
    pcap_remoteact_cleanup.argtypes = []
    pcap_remoteact_list = _lib.pcap_remoteact_list
    pcap_remoteact_list.restype = c_int
    pcap_remoteact_list.argtypes = [STRING,
     c_char,
     c_int,
     STRING]
alldevs = POINTER(pcap_if_t)()
errbuf = create_string_buffer(PCAP_ERRBUF_SIZE)
fcode = bpf_program()
_type = (18, 52)
dst_mac = [[255] * 6, [255] * 6]
src_mac = []

def GetNIC():
    devs = []
    if pcap_findalldevs(byref(alldevs), errbuf) != -1:
        d = alldevs.contents
        while d:
            if d.addresses:
                a = d.addresses.contents
                if a.addr.contents.sa_family == sk.AF_INET:
                    mysockaddr_in = sockaddr_in
                    c = ''
                    if a.netmask:
                        aTmp = cast(a.netmask, POINTER(mysockaddr_in))
                        for i in iptos(aTmp.contents.sin_addr.S_un.S_un_b).split('.'):
                            c = c + '%.2x' % int(i)

                        netmask = int(c, 16)
                    if a.addr:
                        aTmp = cast(a.addr, POINTER(mysockaddr_in))
                        devs.append((d.name, iptos(aTmp.contents.sin_addr.S_un.S_un_b), netmask))
            if d.next:
                d = d.next.contents
            else:
                d = False

        return devs


def compiles(tn, netmask, compstr):
    err1 = pcap_compile(tn, byref(fcode), compstr, 1, netmask)
    err2 = pcap_setfilter(tn, byref(fcode))
    if err1 or err2:
        return 0
    return 1


def GetSrcMac(ip):
    global src_mac
    val = os.popen('ipconfig -all').read().upper()
    if ip not in val:
        return 0
    val = val.split(ip)[0].split('\n')[-4].strip().split(':')[-1].strip().split('-')
    src_mac = []
    try:
        for mac in val:
            src_mac.append(int(mac, 16))

    except:
        src_mac = [0] * 6

    if len(src_mac) != 6:
        src_mac = [0] * 6
    return src_mac


def open_live(dev):
    tn = pcap_open_live(dev[0], 65536, 1, 100, errbuf)
    GetSrcMac(dev[1])
    if not tn:
        return 'Open nic failed : %s' % errbuf
    if not compiles(tn, dev[2], '(ether proto 0x%x%x) and' % _type + '(ether dst host %02x:%02x:%02x:%02x:%02x:%02x)' % tuple(src_mac)):
        return 'Set ether filter failed'
    return tn


def Recived(tn):
    header = POINTER(pcap_pkthdr)()
    pkt_data = POINTER(c_ubyte)()
    if pcap_next_ex(tn, byref(header), byref(pkt_data)) > 0:
        return (header, pkt_data)
    return 0


def CreatePacket(value = '', dst = [255] * 6):
    p_len = len(value) + 15
    packet = (c_ubyte * p_len)()
    packet[:14] = dst + src_mac + list(_type)
    for i in range(len(value)):
        packet[i + 14] = ord(value[i])

    packet[-1] = 0
    return packet


def sendto(tn, cmd, index):
    packet = CreatePacket(cmd, dst_mac[index])
    if pcap_sendpacket(tn, packet, len(packet)) > 0:
        return len(cmd)
    return 0


def close(tn):
    pcap_close(tn)


if __name__ == '__main__':
    dev = ('\\Device\\NPF_{938072D3-3C07-49CE-8AE2-5DE751846ED1}', '172.28.101.59', 4294967040L)
    tn = open_live(dev)
    sendto(tn, 'info', 0)
    data = Recived(tn)
    if data:
        for i in range(data[0].contents.len):
            print '%02X' % data[1][i],