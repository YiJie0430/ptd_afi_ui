import os
import sys
import socket
import pymongo
import wx
import time
import traceback
import logging
import logging.config

logging.config.fileConfig(os.path.dirname(sys.argv[0])+'\\logging.ini', disable_existing_loggers=False)
logger = logging.getLogger(__name__)

def connect(parent):
    def status_conn(parent):
        if parent.DBServerState.GetBackgroundColour() == parent.YellowColor:
            parent.DBServerState.SetBackgroundColour( wx.Colour( 197, 224, 180 ) ) 
        else:
            parent.DBServerState.SetBackgroundColour(parent.YellowColor)
        parent.DBServerState.Refresh()
        
    def status_fail(parent):
        parent.DBServerState.SetBackgroundColour( wx.Colour( 255, 102, 102 ) )
        parent.DBServerState.Refresh()
        #parent._int_msg('Mongo Server Disconnection')

    while True:
        try:
            tcpclient=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            tcpclient.settimeout(1)
            tcpclient.connect(('%s'%parent.DB_ip,int('%s'%parent.DB_port)))

        except:
            msg=str(traceback.format_exc())
            logger.error('MongoDB connect fail: {}'.format(msg))
            status_fail(parent)
            error_class = msg.__class__.__name__
            cl, exc, tb = sys.exc_info()
            lastCallStack = traceback.extract_tb(tb)[-1]
            fileName = lastCallStack[0] 
            lineNum = lastCallStack[1]
            funcName = lastCallStack[2]
            errMsg = "File \"{}\", line {}, in {}: [{}]".format(fileName, lineNum, funcName, error_class)
            logger.error('{}'.format(errMsg))   
            time.sleep(5)
            #print("DB Server not available")
        else:
            status_conn(parent)
            time.sleep(5)
            #print("DB")
                
