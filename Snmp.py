# Embedded file name: Snmp.pyc
from pysnmp import asn1, v1, v2c
from pysnmp import role

def SnmpGet(agent, oidList, version = '1', community = 'public', port = 161, timeout = 1, retries = 5):
#def SnmpGet(agent, oidList, version = '1', community = 'public123', port = 161, timeout = 1, retries = 5):
    if type(oidList) is not list:
        oidList = [oidList]
    client = role.manager((agent, port))
    client.timeout = timeout
    client.retries = retries
    try:
        req = eval('v' + version).GETREQUEST()
        rsp = eval('v' + version).GETRESPONSE()
    except (NameError, AttributeError):
        print 'Unsupported SNMP protocol version: %s\n%s' % (version, usage)
        return

    answer, src = client.send_and_receive(req.encode(community=community, encoded_oids=map(asn1.OBJECTID().encode, oidList)))
    rsp.decode(answer)
    if req != rsp:
        print 'Unmatched response: %s vs %s' % (str(req), str(rsp))
        return
    oids = map(lambda x: x[0], map(asn1.OBJECTID().decode, rsp['encoded_oids']))
    vals = map(lambda x: x[0](), map(asn1.decode, rsp['encoded_vals']))
    if rsp['error_status']:
        print 'SNMP error #' + str(rsp['error_status']) + ' for OID #' + str(rsp['error_index'])
        return
    result = {}
    for oid, val in map(None, oids, vals):
        result[oid] = val

    return result


def SnmpSet(agent, set_oid, format = 'i', value = 0, version = '1', community = 'public', port = 161, timeout = 1, retries = 5):
    format_dict = {'i': 'INTEGER',
     'u': 'UNSIGNED32',
     't': 'TIMETICKS',
     'a': 'IPADDRESS',
     'o': 'OBJECTID',
     's': 'OCTETSTRING',
     'U': 'COUNTER64'}
    if type(set_oid) is list:
        varargs = set_oid
    else:
        varargs = [(set_oid, format_dict[format], value)]
    client = role.manager((agent, port))
    client.timeout = timeout
    client.retries = retries
    try:
        req = eval('v' + version).SETREQUEST()
        rsp = eval('v' + version).GETRESPONSE()
    except (NameError, AttributeError):
        print 'Unsupported SNMP protocol version: %s\n%s' % (version, usage)
        return

    encoded_oids = []
    encoded_vals = []
    for oid, typ, val in varargs:
        encoded_oids.append(asn1.OBJECTID().encode(oid))
        encoded_vals.append(eval('asn1.' + typ + '()').encode(val))

    answer, src = client.send_and_receive(req.encode(community=community, encoded_oids=encoded_oids, encoded_vals=encoded_vals))
    rsp.decode(answer)
    if req != rsp:
        print 'Unmatched response: %s vs %s' % (str(req), str(rsp))
        return
    oids = map(lambda x: x[0], map(asn1.OBJECTID().decode, rsp['encoded_oids']))
    vals = map(lambda x: x[0](), map(asn1.decode, rsp['encoded_vals']))
    if rsp['error_status']:
        print 'SNMP error #' + str(rsp['error_status']) + ' for OID #' + str(rsp['error_index'])
        return
    result = {}
    for oid, val in map(None, oids, vals):
        result[oid] = val

    return result


def SnmpWalk(agent, oidList, version = '1', community = 'public', port = 161, timeout = 1, retries = 5):
    if type(oidList) is not list:
        oidList = [oidList]
    client = role.manager((agent, port))
    client.timeout = timeout
    client.retries = retries
    try:
        req = eval('v' + version).GETREQUEST()
        nextReq = eval('v' + version).GETNEXTREQUEST()
        rsp = eval('v' + version).GETRESPONSE()
    except (NameError, AttributeError):
        print 'Unsupported SNMP protocol version: %s\n%s' % (version, usage)
        return

    head_oids = oidList
    encoded_oids = map(asn1.OBJECTID().encode, head_oids)
    result = {}
    while 1:
        answer, src = client.send_and_receive(req.encode(community=community, encoded_oids=encoded_oids))
        rsp.decode(answer)
        if req != rsp:
            print 'Unmatched response: %s vs %s' % (str(req), str(rsp))
            return
        oids = map(lambda x: x[0], map(asn1.OBJECTID().decode, rsp['encoded_oids']))
        vals = map(lambda x: x[0](), map(asn1.decode, rsp['encoded_vals']))
        if rsp['error_status']:
            if rsp['error_status'] == 2:
                if req is not nextReq:
                    req = nextReq
                    continue
                for l in (oids, vals, head_oids):
                    del l[rsp['error_index'] - 1]

            else:
                print 'SNMP error #' + str(rsp['error_status']) + ' for OID #' + str(rsp['error_index'])
                return
        while 1:
            for idx in range(len(head_oids)):
                if not asn1.OBJECTID(head_oids[idx]).isaprefix(oids[idx]):
                    for l in (oids, vals, head_oids):
                        del l[idx]

                    break
            else:
                break

        if not head_oids:
            break
        for oid, val in map(None, oids, vals):
            result[oid] = val

        encoded_oids = map(asn1.OBJECTID().encode, oids)
        req['request_id'] = req['request_id'] + 1
        if req is not nextReq:
            req = nextReq

    return result