# Embedded file name: buildkey\build.pyc
import sys
import string
import os
import os.path
import binascii
import inspect
mta_path = 'c:\\AFIMainGui\\buildkey'

def LengthValue(data):
    return chr(len(data) >> 8 & 255) + chr(len(data) & 255) + data


def GetCustomerName(datas):
    for data in string.split(datas, '\n'):
        if string.find(data, 'O_value') != -1:
            index = string.find(data, '=') + 1
            data = string.strip(data[index:])
            for i in xrange(len(data)):
                if data[i] in ' \t,.':
                    return data[:i]

            return data


def Cleanall():
    os.system('del %s\\*.tmp /Q' % mta_path)
    os.system('del %s\\*.old /Q' % mta_path)
    os.system('del %s\\*.mac /Q' % mta_path)
    os.system('del %s\\out\\*.* /Q' % mta_path)
    os.system('copy %s\\index.0 %s\\index.txt /Y ' % (mta_path, mta_path))


def copy(openssl_path, mac):
    if not os.path.isfile('%s\\Euro-PacketCable_Service_Provider_Root_CA.509.cer' % openssl_path):
        os.system('copy  %s\\Euro-PacketCable_Service_Provider_Root_CA.509.cer %s' % (mta_path, openssl_path))
    if not os.path.isfile('%s\\Hitron_EuroPacketCable_CA.509.cer' % openssl_path):
        os.system('copy  %s\\Hitron_EuroPacketCable_CA.509.cer %s' % (mta_path, openssl_path))
    if not os.path.isfile('%s\\%s.der' % (openssl_path, mac)):
        os.system('copy %s\\out\\%s.der %s' % (mta_path, mac, openssl_path))
    if not os.path.isfile('%s\\%s_private.der' % (openssl_path, mac)):
        os.system('copy %s\\out\\%s_private.der %s' % (mta_path, mac, openssl_path))
    if not os.path.isfile('%s\\%s_public.der' % (openssl_path, mac)):
        os.system('copy %s\\out\\%s_public.der %s' % (mta_path, mac, openssl_path))


def PublicKey(mac_addr):
    in_f = open('%s\\publicKey.tmp' % mta_path, 'r')
    data = in_f.readline()
    in_f.close()
    if data[:8] == 'Modulus=':
        data = string.strip(data)
        publicKey = binascii.a2b_hex(data[8:])
        begin_str = '0\x81\x89\x02\x81\x81\x00'
        tail_str = '\x02\x03\x01\x00\x01'
        out_f = open('%s\\out\\%s_public.der' % (mta_path, mac_addr), 'wb')
        out_f.write(begin_str)
        out_f.write(publicKey)
        out_f.write(tail_str)
        out_f.close()
        return 0
    else:
        return 'public key format unkown!!'


def PEM2Der(rfile, ofile, private = ''):
    start_encode = 0
    private_key = ''
    in_f = open('%s\\%s' % (mta_path, rfile), 'r')
    while 1:
        line = in_f.readline()
        if not line:
            break
        if line[:5] == '-----':
            if start_encode == 0:
                start_encode = 1
                continue
            else:
                break
        if start_encode:
            private_key += binascii.a2b_base64(line)

    if start_encode == 0:
        in_f.seek(0)
        private_key = binascii.a2b_base64(in_f.read())
    in_f.close()
    if not private:
        out_f = open('%s\\out\\%s' % (mta_path, ofile), 'wb')
        out_f.write(private_key)
        out_f.close()
    else:
        length = len(private_key) + 22
        begin_str = '0\x82' + chr(length >> 8 & 255) + chr(length & 255)
        other_header = '\x02\x01\x000\r\x06\t*\x86H\x86\xf7\r\x01\x01\x01\x05\x00\x04\x82'
        out_f = open('%s\\out\\%s' % (mta_path, ofile), 'wb')
        out_f.write(begin_str)
        length = len(private_key)
        out_f.write(other_header + chr(length >> 8 & 255) + chr(length & 255))
        out_f.write(private_key)
        out_f.close()
    print 'Transfer PEM %s to DER format %s' % (rfile, ofile)


def Create(openss_path, mac):
    for i in xrange(0, 12):
        if mac[i] not in string.hexdigits:
            return 'MAC address hex number incorrect!!'

    Cleanall()
    mac_addr = mac.upper()
    out_f = open('%s\\serial.mac' % mta_path, 'wb')
    out_f.write(binascii.b2a_hex(mac_addr))
    out_f.close()
    in_f = open('%s\\MtaReqDesc.cfg' % mta_path)
    req_head = in_f.read()
    in_f.close()
    CN_value = mac_addr[0:2] + ':' + mac_addr[2:4] + ':' + mac_addr[4:6] + ':' + mac_addr[6:8] + ':' + mac_addr[8:10] + ':' + mac_addr[10:]
    out_f = open('%s\\MtaReq.cfg' % mta_path, 'w')
    out_f.write(req_head)
    out_f.write('CN_value               = ' + CN_value + '\n')
    out_f.close()
    ext_name = 'Mta'
    os.system('cd %s && openssl genrsa -out key.tmp 1024' % mta_path)
    os.system('cd %s && openssl req -new -key key.tmp -out req.tmp -config MtaReq.cfg' % mta_path)
    os.system('cd %s && openssl ca -in req.tmp -notext -batch -config MtaCa.cfg -out CA_pem.tmp ' % mta_path)
    os.system('del %s\\' % mta_path + binascii.b2a_hex(mac_addr) + '.pem ')
    os.system('cd %s && openssl x509 -inform PEM -noout -modulus -in CA_pem.tmp > publicKey.tmp ' % mta_path)
    msg = PublicKey(mac_addr)
    if msg:
        return msg
    PEM2Der('Key.tmp', mac_addr + '_private.der', 'private')
    PEM2Der('CA_pem.tmp', mac_addr + '.der')
    in_f = open('%s\\Hitron_EuroPacketCable_CA.509.cer' % mta_path, 'rb')
    mta_mfg_ca_cert = in_f.read()
    in_f.close()
    in_f = open('%s\\out\\' % mta_path + mac_addr + '.der', 'rb')
    mta_dev_cert = in_f.read()
    in_f.close()
    in_f = open('%s\\out\\' % mta_path + mac_addr + '_private.der', 'rb')
    mta_dev_private_key = in_f.read()
    in_f.close()
    in_f = open('%s\\Euro-PacketCable_Service_Provider_Root_CA.509.cer' % mta_path, 'rb')
    sp_root_ca_cert = in_f.read()
    in_f.close()
    out_f = open('%s\\CMCA\\%s.%s' % (mta_path, mac_addr, ext_name), 'wb')
    out_f.write(LengthValue(mta_mfg_ca_cert))
    out_f.write(LengthValue(mta_dev_cert))
    out_f.write(LengthValue(mta_dev_private_key))
    out_f.write(LengthValue(sp_root_ca_cert))
    out_f.close()
    print 'Build PacketCable Certificates file :%s\\%s.%s' % (os.path.abspath('CMCA'), mac_addr, ext_name)
    copy(openss_path, mac_addr)
    return 0