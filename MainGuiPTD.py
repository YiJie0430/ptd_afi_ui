# Embedded file name: MainGui.py
import wx, ConfigParser
import wx.xrc
import wx.richtext
import wx.lib.scrolledpanel as scrolled
from gui.StateMachine import StateMachine
from gui.BufferConsole import *
from gui.sysVars import *
import sys, os
import threading
from gui.refstation import afiStation, fixtureStatus
import time
import traceback
import logging
import logging.config
#import multiprocessing as mp
lock = threading.Lock()

GUIDirectory=os.path.dirname(sys.argv[0])
if not GUIDirectory:
    GUIDirectory=os.getcwd()

sys.path.append(GUIDirectory)
execfile(GUIDirectory+'\\GUI.ini')

logging.config.fileConfig(GUIDirectory+'\\logging.ini', disable_existing_loggers=False)
logger = logging.getLogger(__name__)

logger.debug('PT AFI1 GUI: {}'.format(GUIDirectory))


def strip(s):
    return s.strip()

class MainGUI(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u'PT-AFI | Build {}'.format(VERSION), pos=wx.DefaultPosition, size=(800,600), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        self.SetFont(wx.Font(10, 74, 90, 90, False, 'Arial'))
        self.SetIcon(wx.Icon(u'icon/logo.ico', wx.BITMAP_TYPE_ICO))
        self.SetBackgroundColour(wx.Colour(245, 245, 245))
        self.DisableColor = wx.Colour(245, 245, 245)
        self.YellowColor = wx.Colour(255, 255, 147)
        self.RedColor = wx.Colour(255, 102, 102)
        self.BlueColor = wx.Colour(51, 153, 255)
        self.LimeColor = wx.Colour(153, 255, 153)
        self.ActionColor = wx.Colour(255, 255, 204)
        self.UnworkColor = wx.Colour(224, 224, 224)
        self.fixtureUnworkColor = wx.Colour(222, 222, 190)
        self.m_menubar1 = wx.MenuBar(0)
        self.M_Debug = wx.Menu()
        self.m_menu2 = wx.Menu()
        self.M_L_CCU = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'CCU Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_CCU)
        self.M_L_S0 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S0 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S0)
        self.M_L_S1 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S1 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S1)
        self.M_L_S2 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S2 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S2)
        self.M_L_S3 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S3 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S3)
        self.M_L_S4 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S4 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S4)
        self.M_L_S5 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S5 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S5)
        self.M_L_S6 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'S6 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_S6)
        self.m_menu2.AppendSeparator()
        self.M_L_DUT0 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT0 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT0)
        self.M_L_DUT1 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT1 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT1)
        self.M_L_DUT2 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT2 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT2)
        self.M_L_DUT3 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT3 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT3)
        self.M_L_DUT4 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT4 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT4)
        self.M_L_DUT5 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT5 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT5)
        self.M_L_DUT6 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT6 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT6)
        self.M_L_DUT7 = wx.MenuItem(self.m_menu2, wx.ID_ANY, u'DUT7 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.M_L_DUT7)
        self.M_Debug.AppendSubMenu(self.m_menu2, u'Left')
        self.m_menu21 = wx.Menu()
        self.M_R_CCU = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'CCU Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_CCU)
        self.M_R_S0 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S0 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S0)
        self.M_R_S1 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S1 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S1)
        self.M_R_S2 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S2 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S2)
        self.M_R_S3 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S3 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S3)
        self.M_R_S4 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S4 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S4)
        self.M_R_S5 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S5 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S5)
        self.M_R_S6 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'S6 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_S6)
        self.m_menu21.AppendSeparator()
        self.M_R_DUT0 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT0 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT0)
        self.M_R_DUT1 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT1 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT1)
        self.M_R_DUT2 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT2 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT2)
        self.M_R_DUT3 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT3 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT3)
        self.M_R_DUT4 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT4 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT4)
        self.M_R_DUT5 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT5 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT5)
        self.M_R_DUT6 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT6 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT6)
        self.M_R_DUT7 = wx.MenuItem(self.m_menu21, wx.ID_ANY, u'DUT7 Console', wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu21.AppendItem(self.M_R_DUT7)
        self.M_Debug.AppendSubMenu(self.m_menu21, u'RIGHT')
        self.m_menubar1.Append(self.M_Debug, u'Debug')
        self.M_Config = wx.Menu()
        self.m_menubar1.Append(self.M_Config, u'Config')
        self.Process = self.M_Config.Append(-1, 'Process')
        self.M_MES = wx.Menu()
        self.m_menubar1.Append(self.M_MES, u'MES')
        self.Repair = self.M_MES.Append(-1, 'To Repair')
        self.m_tools = wx.Menu()
        self.m_menubar1.Append(self.m_tools, u'Tools')
        self.m_calibration = self.m_tools.Append(-1, 'Calibration')
        self.M_Help = wx.Menu()
        self.m_menubar1.Append(self.M_Help, u'Help')
        self.About = self.M_Help.Append(-1, 'About')
        self.SetMenuBar(self.m_menubar1)

        bSizer1 = wx.BoxSizer(wx.VERTICAL)
        if newlayout:
            bSizer2 = wx.BoxSizer(wx.HORIZONTAL)
        else:
            bSizer2 = wx.BoxSizer(wx.VERTICAL)
        #border
        self.m_staticline0 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_HORIZONTAL)
        bSizer1.Add(self.m_staticline0, 0, wx.EXPAND | wx.ALL, 0)
        
        #Init sector
        bSizerINIT = wx.BoxSizer(wx.VERTICAL) # |
        
        #diag_layer= wx.StaticBoxSizer( wx.StaticBox( self, -1, u"DIAGNOSTIC" ), wx.VERTICAL )
        
        #AFI status
        bSizerAFI = wx.BoxSizer(wx.HORIZONTAL) # <->
        if AFI:
            #bSizerAFI = wx.BoxSizer(wx.HORIZONTAL) # <->
            self.m_staticText211 = wx.StaticText(self, wx.ID_ANY, u'AFI     ', wx.DefaultPosition, wx.DefaultSize, 10)
            self.m_staticText211.Wrap(-1)
            self.m_staticText211.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
            bSizerAFI.Add(self.m_staticText211, 0, wx.ALL | wx.EXPAND, 5)
            
            self.AFIServerState = wx.StaticText(self, wx.ID_ANY, u'     ', wx.DefaultPosition, wx.DefaultSize, 10)
            self.AFIServerState.Wrap(-1)
            self.AFIServerState.SetBackgroundColour(self.DisableColor)
            bSizerAFI.Add(self.AFIServerState, 0, wx.ALL | wx.EXPAND, 5)
            
            self.m_staticline1 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_VERTICAL)
            bSizerAFI.Add(self.m_staticline1, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_BOTTOM | wx.EXPAND, 10)
            
        self.m_staticText71 = wx.StaticText(self, wx.ID_ANY, u'PN', wx.DefaultPosition, wx.DefaultSize, 10)
        self.m_staticText71.Wrap(-1)
        self.m_staticText71.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizerAFI.Add(self.m_staticText71, 0, wx.ALL | wx.EXPAND, 5)
        
        self.Partnum = wx.StaticText(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, 0)
        self.Partnum.Wrap(-1)
        self.Partnum.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        #self.ModelName.SetBackgroundColour(self.DisableColor)
        bSizerAFI.Add(self.Partnum, 1, wx.ALL | wx.EXPAND, 5)

        self.m_staticline1_1 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_VERTICAL)
        bSizerAFI.Add(self.m_staticline1_1, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_BOTTOM | wx.EXPAND, 10)
        
        self.m_staticText7 = wx.StaticText(self, wx.ID_ANY, u'MODEL', wx.DefaultPosition, wx.DefaultSize, 10)
        self.m_staticText7.Wrap(-1)
        self.m_staticText7.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizerAFI.Add(self.m_staticText7, 0, wx.ALL | wx.EXPAND, 5)
        
        self.ModelName = wx.StaticText(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, 0)
        self.ModelName.Wrap(-1)
        self.ModelName.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        #self.ModelName.SetBackgroundColour(self.DisableColor)
        bSizerAFI.Add(self.ModelName, 1, wx.ALL | wx.EXPAND, 5)
        
        self.m_staticline1_2 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_VERTICAL)
        bSizerAFI.Add(self.m_staticline1_2, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_BOTTOM | wx.EXPAND, 10)
        
        self.m_staticText71 = wx.StaticText(self, wx.ID_ANY, u'STATION', wx.DefaultPosition, wx.DefaultSize, 10)
        self.m_staticText71.Wrap(-1)
        self.m_staticText71.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizerAFI.Add(self.m_staticText71, 0, wx.ALL | wx.EXPAND, 5)
        
        self.StationName = wx.StaticText(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, 0)
        self.StationName.Wrap(-1)
        self.StationName.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        #self.StationName.SetBackgroundColour(self.DisableColor)
        bSizerAFI.Add(self.StationName, 1, wx.ALL | wx.EXPAND, 5)
        
        bSizerINIT.Add(bSizerAFI, 0, wx.ALL | wx.EXPAND, 5)
        
        #border
        #self.m_staticline2 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_VERTICAL)
        #bSizerINIT.Add(self.m_staticline2, 0, wx.EXPAND, 5)
        
        #TFTP status
        if TFTP:
            bSizerTFTP = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText21 = wx.StaticText(self, wx.ID_ANY, u'TFTP  ', wx.DefaultPosition, wx.DefaultSize, 10)
            self.m_staticText21.Wrap(-1)
            self.m_staticText21.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
            bSizerTFTP.Add(self.m_staticText21, 0, wx.ALL | wx.EXPAND, 5)
            
            self.TFTPServerState = wx.StaticText(self, wx.ID_ANY, u'     ', wx.DefaultPosition, wx.DefaultSize, 10)
            self.TFTPServerState.Wrap(-1)
            self.TFTPServerState.SetBackgroundColour(self.DisableColor)
            bSizerTFTP.Add(self.TFTPServerState, 0, wx.ALL | wx.EXPAND, 5)
            
            self.m_staticline3_1 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
            bSizerTFTP.Add(self.m_staticline3_1, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_BOTTOM | wx.EXPAND, 10)
            
            self.m_staticText72 = wx.StaticText(self, wx.ID_ANY, u'PATH', wx.DefaultPosition, wx.DefaultSize, 10)
            self.m_staticText72.Wrap(-1)
            self.m_staticText72.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
            bSizerTFTP.Add(self.m_staticText72, 0, wx.ALL | wx.EXPAND, 5)
            
            self.TFTPPath = wx.StaticText(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, 0)
            self.TFTPPath.Wrap(-1)
            self.TFTPPath.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
            self.TFTPPath.SetBackgroundColour(self.DisableColor)
            #self.TFTPPath.SetForegroundColour(wx.Colour(0, 128, 255))
            bSizerTFTP.Add(self.TFTPPath, 1, wx.ALL | wx.EXPAND, 5)
            
            #self.m_staticline3_2 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
            #bSizerTFTP.Add(self.m_staticline3_2, 0, wx.RIGHT | wx.LEFT | wx.EXPAND, 10)
            
            self.m_staticText711 = wx.StaticText(self, wx.ID_ANY, u'STATUS', wx.DefaultPosition, wx.DefaultSize, 10)
            self.m_staticText711.Wrap(-1)
            self.m_staticText711.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
            bSizerTFTP.Add(self.m_staticText711, 0, wx.ALL | wx.EXPAND, 5)
            
            self.TFTPTransferFile = wx.StaticText(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, 0)
            self.TFTPTransferFile.Wrap(-1)
            self.TFTPTransferFile.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
            self.TFTPTransferFile.SetBackgroundColour(self.DisableColor)
            #self.TFTPTransferFile.SetForegroundColour(wx.Colour(0, 128, 255))
            bSizerTFTP.Add(self.TFTPTransferFile, 1, wx.ALL | wx.EXPAND, 5)
            
            bSizerINIT.Add(bSizerTFTP, 0, wx.EXPAND | wx.ALL, 5)
            
            #self.m_staticline4 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
            #bSizerINIT.Add(self.m_staticline4, 0, wx.RIGHT | wx.LEFT | wx.EXPAND, 5)

        if DB:
            #MongoDB status
            bSizerDB = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText22 = wx.StaticText(self, wx.ID_ANY, u'Mongo', wx.DefaultPosition, wx.DefaultSize, 10)
            self.m_staticText22.Wrap(-1)
            self.m_staticText22.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
            bSizerDB.Add(self.m_staticText22, 0, wx.ALL | wx.EXPAND, 5)
            
            self.DBServerState = wx.StaticText(self, wx.ID_ANY, u'     ', wx.DefaultPosition, wx.DefaultSize, 10)
            self.DBServerState.Wrap(-1)
            self.DBServerState.SetBackgroundColour(self.DisableColor)
            bSizerDB.Add(self.DBServerState, 0, wx.ALL | wx.EXPAND, 5)
            
            self.m_staticline3_1 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
            bSizerDB.Add(self.m_staticline3_1, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_BOTTOM | wx.EXPAND, 10)
            
            self.m_staticTextDB = wx.StaticText(self, wx.ID_ANY, u'DB IP', wx.DefaultPosition, wx.DefaultSize, 10)
            self.m_staticTextDB.Wrap(-1)
            self.m_staticTextDB.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
            bSizerDB.Add(self.m_staticTextDB, 0, wx.ALL | wx.EXPAND, 5)
            
            self.DBip = wx.StaticText(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, 0)
            self.DBip.Wrap(-1)
            self.DBip.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
            self.DBip.SetBackgroundColour(self.DisableColor)
            #self.DBip.SetForegroundColour(wx.Colour(0, 128, 255))
            bSizerDB.Add(self.DBip, 1, wx.ALL | wx.EXPAND, 5)
            
            #self.m_staticline3_2 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
            #bSizerDB.Add(self.m_staticline3_2, 0, wx.RIGHT | wx.LEFT | wx.EXPAND, 10)
            
            bSizerINIT.Add(bSizerDB, 0, wx.EXPAND | wx.ALL, 5)
            
            #self.m_staticline4 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL)
            #bSizerINIT.Add(self.m_staticline4, 0, wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            
        bSizer1.Add(bSizerINIT, 0,  wx.EXPAND | wx.ALL, 0)
        #diag_layer.Add(bSizerINIT, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
        #bSizer1.Add(diag_layer, 0, wx.EXPAND | wx.ALL, 0)
        
        
        #DUT status
        bSizer3 = wx.BoxSizer(wx.VERTICAL)
        
        work_layer= wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"TEST AREA", style=wx.LC_REPORT), wx.VERTICAL )
        #self.m_staticline7 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_HORIZONTAL)
        #bSizer3.Add(self.m_staticline7, 0, wx.EXPAND | wx.ALL, 5)
        
        #Scan
        bSizerSCAN = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText6 = wx.StaticText(self, wx.ID_ANY, u'SCAN', wx.DefaultPosition, wx.DefaultSize, 10)
        self.m_staticText6.Wrap(-1)
        self.m_staticText6.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizerSCAN.Add(self.m_staticText6, 0,  wx.ALL, 5)
        
        self.ScanLabel = wx.TextCtrl(self, wx.ID_ANY, u'', wx.DefaultPosition, (60, 20), style=wx.TE_PROCESS_ENTER)
        self.ScanLabel.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        self.ScanLabel.SetBackgroundColour(wx.Colour(192, 192, 192))
        bSizerSCAN.Add(self.ScanLabel, 0, wx.ALL, 5)
        
        #self.m_staticline5 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1), wx.LI_VERTICAL)
        #bSizerSCAN.Add(self.m_staticline5, 0, wx.RIGHT | wx.LEFT | wx.ALIGN_BOTTOM | wx.EXPAND, 10)
        
        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u'LABEL', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        self.m_staticText4.SetFont(wx.Font(10, 74, 90, 92, False, wx.EmptyString))
        bSizerSCAN.Add(self.m_staticText4, 0, wx.ALL, 5)
        
        self.LabelName = wx.StaticText(self, wx.ID_ANY, u'MFG', wx.DefaultPosition, wx.DefaultSize, 0)
        
        self.demo = demo
        if self.demo:
           self.LabelName.SetLabel('DEMO')
        
        self.LabelName.Wrap(-1)
        self.LabelName.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        self.LabelName.SetBackgroundColour(self.DisableColor)
        bSizerSCAN.Add(self.LabelName, 0, wx.ALL, 5)
        #bSizerINIT.Add(bSizerSCAN, 0, wx.ALL | wx.EXPAND, 5)
        bSizer3.Add(bSizerSCAN, 0, wx.ALL | wx.EXPAND, 5)
        
        #DUT panel
        gSizerProgress = wx.GridSizer(0, 4, 0, 0)
        
        #DUT1A
        self.Panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel1.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel1.SetBackgroundColour(self.DisableColor)
        #self.Panel1.SetToolTipString(u'Failed:ERROR')
        _Group1 = wx.StaticBox(self.Panel1, wx.ID_ANY, u'DUT 1')
        Group1 = wx.StaticBoxSizer(_Group1, wx.VERTICAL)
        bSizer25 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText17 = wx.StaticText(self.Panel1, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText17.Wrap(-1)
        self.m_staticText17.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer25.Add(self.m_staticText17, 0, wx.ALL, 5)
        self.Barcode11 = wx.TextCtrl(self.Panel1, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode11.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer25.Add(self.Barcode11, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group1.Add(bSizer25, 1, wx.EXPAND, 5)
        bSizer253 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText173 = wx.StaticText(self.Panel1, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText173.Wrap(-1)
        self.m_staticText173.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer253.Add(self.m_staticText173, 0, wx.ALL, 5)
        self.Barcode12 = wx.TextCtrl(self.Panel1, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode12.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer253.Add(self.Barcode12, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group1.Add(bSizer253, 1, wx.EXPAND, 5)
        bSizer254 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText174 = wx.StaticText(self.Panel1, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText174.Wrap(-1)
        self.m_staticText174.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer254.Add(self.m_staticText174, 0, wx.ALL, 5)
        self.Process1 = wx.TextCtrl(self.Panel1, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process1.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer254.Add(self.Process1, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group1.Add(bSizer254, 1, wx.EXPAND, 5)
        #self.Gauge1 = wx.Gauge(self.Panel1, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        #self.Gauge1.SetValue(98)
        self.Gauge1 = wx.Gauge(self.Panel1, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge1.SetValue(0)
        self.Gauge1.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group1.Add(self.Gauge1, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel1.SetSizer(Group1)
        self.Panel1.Layout()
        Group1.Fit(self.Panel1)
        #gSizerProgress.Add(self.Panel1, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)

        #DUT2A
        self.Panel2 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel2.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel2.SetBackgroundColour(self.DisableColor)
        #self.Panel2.SetToolTipString(u'Failed:ERROR')
        _Group2 = wx.StaticBox(self.Panel2, wx.ID_ANY, u'DUT 2')
        Group2 = wx.StaticBoxSizer(_Group2, wx.VERTICAL)
        bSizer256 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText176 = wx.StaticText(self.Panel2, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText176.Wrap(-1)
        self.m_staticText176.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer256.Add(self.m_staticText176, 0, wx.ALL, 5)
        self.Barcode21 = wx.TextCtrl(self.Panel2, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode21.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer256.Add(self.Barcode21, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group2.Add(bSizer256, 1, wx.EXPAND, 5)
        bSizer2534 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1734 = wx.StaticText(self.Panel2, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1734.Wrap(-1)
        self.m_staticText1734.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2534.Add(self.m_staticText1734, 0, wx.ALL, 5)
        self.Barcode22 = wx.TextCtrl(self.Panel2, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode22.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2534.Add(self.Barcode22, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group2.Add(bSizer2534, 1, wx.EXPAND, 5)
        bSizer2544 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1744 = wx.StaticText(self.Panel2, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1744.Wrap(-1)
        self.m_staticText1744.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2544.Add(self.m_staticText1744, 0, wx.ALL, 5)
        self.Process2 = wx.TextCtrl(self.Panel2, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process2.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2544.Add(self.Process2, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group2.Add(bSizer2544, 1, wx.EXPAND, 5)
        #self.Gauge2 = wx.Gauge(self.Panel2, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        #self.Gauge2.SetValue(98)
        self.Gauge2 = wx.Gauge(self.Panel2, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge2.SetValue(0)
        self.Gauge2.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group2.Add(self.Gauge2, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel2.SetSizer(Group2)
        self.Panel2.Layout()
        Group2.Fit(self.Panel2)
        #gSizerProgress.Add(self.Panel2, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
        
        #DUT3A
        self.Panel3 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel3.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel3.SetBackgroundColour(self.DisableColor)
        #self.Panel3.SetToolTipString(u'Failed:ERROR')
        _Group3 = wx.StaticBox(self.Panel3, wx.ID_ANY, u'DUT 3')
        Group3 = wx.StaticBoxSizer(_Group3, wx.VERTICAL)
        bSizer255 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText175 = wx.StaticText(self.Panel3, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText175.Wrap(-1)
        self.m_staticText175.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer255.Add(self.m_staticText175, 0, wx.ALL, 5)
        self.Barcode31 = wx.TextCtrl(self.Panel3, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode31.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer255.Add(self.Barcode31, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group3.Add(bSizer255, 1, wx.EXPAND, 5)
        bSizer2531 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1731 = wx.StaticText(self.Panel3, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1731.Wrap(-1)
        self.m_staticText1731.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2531.Add(self.m_staticText1731, 0, wx.ALL, 5)
        self.Barcode32 = wx.TextCtrl(self.Panel3, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode32.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2531.Add(self.Barcode32, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group3.Add(bSizer2531, 1, wx.EXPAND, 5)
        bSizer2541 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1741 = wx.StaticText(self.Panel3, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1741.Wrap(-1)
        self.m_staticText1741.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2541.Add(self.m_staticText1741, 0, wx.ALL, 5)
        self.Process3 = wx.TextCtrl(self.Panel3, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process3.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2541.Add(self.Process3, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group3.Add(bSizer2541, 1, wx.EXPAND, 5)
        #self.Gauge3 = wx.Gauge(self.Panel3, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        #self.Gauge3.SetValue(98)
        self.Gauge3 = wx.Gauge(self.Panel3, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge3.SetValue(0)
        self.Gauge3.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group3.Add(self.Gauge3, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel3.SetSizer(Group3)
        self.Panel3.Layout()
        Group3.Fit(self.Panel3)
        #gSizerProgress.Add(self.Panel3, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)

        #DUT4A
        self.Panel4 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel4.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel4.SetBackgroundColour(self.DisableColor)
        #self.Panel4.SetToolTipString(u'Failed:ERROR')
        _Group4 = wx.StaticBox(self.Panel4, wx.ID_ANY, u'DUT 4')
        Group4 = wx.StaticBoxSizer(_Group4, wx.VERTICAL)
        bSizer257 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText177 = wx.StaticText(self.Panel4, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText177.Wrap(-1)
        self.m_staticText177.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer257.Add(self.m_staticText177, 0, wx.ALL, 5)
        self.Barcode41 = wx.TextCtrl(self.Panel4, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode41.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer257.Add(self.Barcode41, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group4.Add(bSizer257, 1, wx.EXPAND, 5)
        bSizer2535 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1735 = wx.StaticText(self.Panel4, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1735.Wrap(-1)
        self.m_staticText1735.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2535.Add(self.m_staticText1735, 0, wx.ALL, 5)
        self.Barcode42 = wx.TextCtrl(self.Panel4, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode42.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2535.Add(self.Barcode42, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group4.Add(bSizer2535, 1, wx.EXPAND, 5)
        bSizer2545 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1745 = wx.StaticText(self.Panel4, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1745.Wrap(-1)
        self.m_staticText1745.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2545.Add(self.m_staticText1745, 0, wx.ALL, 5)
        self.Process4 = wx.TextCtrl(self.Panel4, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process4.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2545.Add(self.Process4, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group4.Add(bSizer2545, 1, wx.EXPAND, 5)
        #self.Gauge4 = wx.Gauge(self.Panel4, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        #self.Gauge4.SetValue(98)
        self.Gauge4 = wx.Gauge(self.Panel4, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge4.SetValue(0)
        self.Gauge4.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group4.Add(self.Gauge4, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel4.SetSizer(Group4)
        self.Panel4.Layout()
        Group4.Fit(self.Panel4)
        #gSizerProgress.Add(self.Panel4, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
        
        #DUT5A
        self.Panel5 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel5.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel5.SetBackgroundColour(self.DisableColor)
        #self.Panel5.SetToolTipString(u'Failed:ERROR')
        _Group5 = wx.StaticBox(self.Panel5, wx.ID_ANY, u'DUT 5')
        Group5 = wx.StaticBoxSizer(_Group5, wx.VERTICAL)
        bSizer251 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText171 = wx.StaticText(self.Panel5, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText171.Wrap(-1)
        self.m_staticText171.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer251.Add(self.m_staticText171, 0, wx.ALL, 5)
        self.Barcode51 = wx.TextCtrl(self.Panel5, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode51.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer251.Add(self.Barcode51, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group5.Add(bSizer251, 1, wx.EXPAND, 5)
        bSizer2532 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1732 = wx.StaticText(self.Panel5, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1732.Wrap(-1)
        self.m_staticText1732.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2532.Add(self.m_staticText1732, 0, wx.ALL, 5)
        self.Barcode52 = wx.TextCtrl(self.Panel5, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode52.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2532.Add(self.Barcode52, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group5.Add(bSizer2532, 1, wx.EXPAND, 5)
        bSizer2542 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1742 = wx.StaticText(self.Panel5, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1742.Wrap(-1)
        self.m_staticText1742.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2542.Add(self.m_staticText1742, 0, wx.ALL, 5)
        self.Process5 = wx.TextCtrl(self.Panel5, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process5.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2542.Add(self.Process5, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group5.Add(bSizer2542, 1, wx.EXPAND, 5)
        #self.Gauge5 = wx.Gauge(self.Panel5, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        #self.Gauge5.SetValue(98)
        self.Gauge5 = wx.Gauge(self.Panel5, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge5.SetValue(0)
        self.Gauge5.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group5.Add(self.Gauge5, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel5.SetSizer(Group5)
        self.Panel5.Layout()
        Group5.Fit(self.Panel5)
        #gSizerProgress.Add(self.Panel5, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)

        #DUT6A
        self.Panel6 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel6.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel6.SetBackgroundColour(self.DisableColor)
        #self.Panel6.SetToolTipString(u'Failed:ERROR')
        _Group6 = wx.StaticBox(self.Panel6, wx.ID_ANY, u'DUT 6')
        Group6 = wx.StaticBoxSizer(_Group6, wx.VERTICAL)
        bSizer258 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText178 = wx.StaticText(self.Panel6, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText178.Wrap(-1)
        self.m_staticText178.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer258.Add(self.m_staticText178, 0, wx.ALL, 5)
        self.Barcode61 = wx.TextCtrl(self.Panel6, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode61.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer258.Add(self.Barcode61, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group6.Add(bSizer258, 1, wx.EXPAND, 5)
        bSizer2536 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1736 = wx.StaticText(self.Panel6, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1736.Wrap(-1)
        self.m_staticText1736.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2536.Add(self.m_staticText1736, 0, wx.ALL, 5)
        self.Barcode62 = wx.TextCtrl(self.Panel6, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode62.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2536.Add(self.Barcode62, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group6.Add(bSizer2536, 1, wx.EXPAND, 5)
        bSizer2546 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1746 = wx.StaticText(self.Panel6, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1746.Wrap(-1)
        self.m_staticText1746.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2546.Add(self.m_staticText1746, 0, wx.ALL, 5)
        self.Process6 = wx.TextCtrl(self.Panel6, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process6.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2546.Add(self.Process6, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group6.Add(bSizer2546, 1, wx.EXPAND, 5)
        #self.Gauge6 = wx.Gauge(self.Panel6, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        #self.Gauge6.SetValue(98)
        self.Gauge6 = wx.Gauge(self.Panel6, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge6.SetValue(0)
        self.Gauge6.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group6.Add(self.Gauge6, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel6.SetSizer(Group6)
        self.Panel6.Layout()
        Group6.Fit(self.Panel6)
        #gSizerProgress.Add(self.Panel6, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)

        #DUT7A
        self.Panel7 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel7.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel7.SetBackgroundColour(self.DisableColor)
        #self.Panel7.SetToolTipString(u'')
        _Group7 = wx.StaticBox(self.Panel7, wx.ID_ANY, u'DUT 7')
        Group7 = wx.StaticBoxSizer(_Group7, wx.VERTICAL)
        bSizer252 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText172 = wx.StaticText(self.Panel7, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText172.Wrap(-1)
        self.m_staticText172.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer252.Add(self.m_staticText172, 0, wx.ALL, 5)
        self.Barcode71 = wx.TextCtrl(self.Panel7, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode71.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer252.Add(self.Barcode71, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group7.Add(bSizer252, 1, wx.EXPAND, 5)
        bSizer2533 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1733 = wx.StaticText(self.Panel7, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1733.Wrap(-1)
        self.m_staticText1733.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2533.Add(self.m_staticText1733, 0, wx.ALL, 5)
        self.Barcode72 = wx.TextCtrl(self.Panel7, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode72.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2533.Add(self.Barcode72, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group7.Add(bSizer2533, 1, wx.EXPAND, 5)
        bSizer2543 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1743 = wx.StaticText(self.Panel7, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1743.Wrap(-1)
        self.m_staticText1743.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2543.Add(self.m_staticText1743, 0, wx.ALL, 5)
        self.Process7 = wx.TextCtrl(self.Panel7, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process7.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2543.Add(self.Process7, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group7.Add(bSizer2543, 1, wx.EXPAND, 5)
        #self.Gauge7 = wx.Gauge(self.Panel7, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        #self.Gauge7.SetValue(98)
        self.Gauge7 = wx.Gauge(self.Panel7, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge7.SetValue(0)
        self.Gauge7.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group7.Add(self.Gauge7, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel7.SetSizer(Group7)
        self.Panel7.Layout()
        Group7.Fit(self.Panel7)
        #gSizerProgress.Add(self.Panel7, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
        
        #DUT8A
        self.Panel8 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Panel8.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
        self.Panel8.SetBackgroundColour(wx.Colour(255, 255, 50))
        #self.Panel8.SetToolTipString(u'Failed:ERROR')
        _Group8 = wx.StaticBox(self.Panel8, wx.ID_ANY, u'DUT 8')
        Group8 = wx.StaticBoxSizer(_Group8, wx.VERTICAL)
        bSizer259 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText179 = wx.StaticText(self.Panel8, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText179.Wrap(-1)
        self.m_staticText179.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer259.Add(self.m_staticText179, 0, wx.ALL, 5)
        self.Barcode81 = wx.TextCtrl(self.Panel8, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode81.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer259.Add(self.Barcode81, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group8.Add(bSizer259, 1, wx.EXPAND, 5)
        bSizer2537 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1737 = wx.StaticText(self.Panel8, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1737.Wrap(-1)
        self.m_staticText1737.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2537.Add(self.m_staticText1737, 0, wx.ALL, 5)
        self.Barcode82 = wx.TextCtrl(self.Panel8, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Barcode82.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2537.Add(self.Barcode82, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group8.Add(bSizer2537, 1, wx.EXPAND, 5)
        bSizer2547 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText1747 = wx.StaticText(self.Panel8, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1747.Wrap(-1)
        self.m_staticText1747.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
        bSizer2547.Add(self.m_staticText1747, 0, wx.ALL, 5)
        self.Process8 = wx.TextCtrl(self.Panel8, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
        self.Process8.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer2547.Add(self.Process8, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
        Group8.Add(bSizer2547, 1, wx.EXPAND, 5)
        #self.Gauge8 = wx.Gauge(self.Panel8, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        #self.Gauge8.SetValue(98)
        self.Gauge8 = wx.Gauge(self.Panel8, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
        self.Gauge8.SetValue(0)
        self.Gauge8.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
        Group8.Add(self.Gauge8, 0, wx.ALL | wx.EXPAND, 5)
        self.Panel8.SetSizer(Group8)
        self.Panel8.Layout()
        Group8.Fit(self.Panel8)
        #gSizerProgress.Add(self.Panel8, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
        
        if newlayout:
            #DUT1B
            self.Panel1b = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            self.Panel1b.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
            self.Panel1b.SetBackgroundColour(self.DisableColor)
            #self.Panel1.SetToolTipString(u'Failed:ERROR')
            _Group1b = wx.StaticBox(self.Panel1b, wx.ID_ANY, u'DUT 1b')
            Group1b = wx.StaticBoxSizer(_Group1b, wx.VERTICAL)
            bSizer25b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText17b = wx.StaticText(self.Panel1b, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText17b.Wrap(-1)
            self.m_staticText17b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer25b.Add(self.m_staticText17b, 0, wx.ALL, 5)
            self.Barcode11b = wx.TextCtrl(self.Panel1b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode11b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer25b.Add(self.Barcode11b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group1b.Add(bSizer25b, 1, wx.EXPAND, 5)
            bSizer253b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText173b = wx.StaticText(self.Panel1b, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText173b.Wrap(-1)
            self.m_staticText173b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer253b.Add(self.m_staticText173b, 0, wx.ALL, 5)
            self.Barcode12b = wx.TextCtrl(self.Panel1b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode12b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer253b.Add(self.Barcode12b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group1b.Add(bSizer253b, 1, wx.EXPAND, 5)
            bSizer254b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText174b = wx.StaticText(self.Panel1b, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText174b.Wrap(-1)
            self.m_staticText174b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer254b.Add(self.m_staticText174b, 0, wx.ALL, 5)
            self.Process1b = wx.TextCtrl(self.Panel1b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Process1b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer254b.Add(self.Process1b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group1b.Add(bSizer254b, 1, wx.EXPAND, 5)
            #self.Gauge1b = wx.Gauge(self.Panel1b, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            #self.Gauge1b.SetValue(98)
            self.Gauge1b = wx.Gauge(self.Panel1b, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            self.Gauge1b.SetValue(0)
            self.Gauge1b.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
            Group1b.Add(self.Gauge1b, 0, wx.ALL | wx.EXPAND, 5)
            self.Panel1b.SetSizer(Group1b)
            self.Panel1b.Layout()
            Group1b.Fit(self.Panel1b)
            #gSizerProgress.Add(self.Panel1b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            
            #DUT2B
            self.Panel2b = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            self.Panel2b.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
            self.Panel2b.SetBackgroundColour(self.DisableColor)
            #self.Panel1.SetToolTipString(u'Failed:ERROR')
            _Group2b = wx.StaticBox(self.Panel2b, wx.ID_ANY, u'DUT 2b')
            Group2b = wx.StaticBoxSizer(_Group2b, wx.VERTICAL)
            bSizer2b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText2b = wx.StaticText(self.Panel2b, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText2b.Wrap(-1)
            self.m_staticText2b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer2b.Add(self.m_staticText2b, 0, wx.ALL, 5)
            self.Barcode21b = wx.TextCtrl(self.Panel2b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode21b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer2b.Add(self.Barcode21b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group2b.Add(bSizer2b, 1, wx.EXPAND, 5)
            bSizer2_1b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText21b = wx.StaticText(self.Panel2b, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText21b.Wrap(-1)
            self.m_staticText21b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer2_1b.Add(self.m_staticText21b, 0, wx.ALL, 5)
            self.Barcode22b = wx.TextCtrl(self.Panel2b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode22b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer2_1b.Add(self.Barcode22b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group2b.Add(bSizer2_1b, 1, wx.EXPAND, 5)
            bSizer2_2b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText23b = wx.StaticText(self.Panel2b, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText23b.Wrap(-1)
            self.m_staticText23b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer2_2b.Add(self.m_staticText23b, 0, wx.ALL, 5)
            self.Process2b = wx.TextCtrl(self.Panel2b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Process2b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer2_2b.Add(self.Process2b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group2b.Add(bSizer2_2b, 1, wx.EXPAND, 5)
            #self.Gauge2b = wx.Gauge(self.Panel2b, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            #self.Gauge2b.SetValue(98)
            self.Gauge2b = wx.Gauge(self.Panel2b, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            self.Gauge2b.SetValue(0)
            self.Gauge2b.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
            Group2b.Add(self.Gauge2b, 0, wx.ALL | wx.EXPAND, 5)
            self.Panel2b.SetSizer(Group2b)
            self.Panel2b.Layout()
            Group2b.Fit(self.Panel2b)
            #gSizerProgress.Add(self.Panel2b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)

            #DUT3B
            self.Panel3b = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            self.Panel3b.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
            self.Panel3b.SetBackgroundColour(self.DisableColor)
            #self.Panel1.SetToolTipString(u'Failed:ERROR')
            _Group3b = wx.StaticBox(self.Panel3b, wx.ID_ANY, u'DUT 3b')
            Group3b = wx.StaticBoxSizer(_Group3b, wx.VERTICAL)
            bSizer3b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText3b = wx.StaticText(self.Panel3b, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText3b.Wrap(-1)
            self.m_staticText3b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer3b.Add(self.m_staticText3b, 0, wx.ALL, 5)
            self.Barcode31b = wx.TextCtrl(self.Panel3b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode31b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer3b.Add(self.Barcode31b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group3b.Add(bSizer3b, 1, wx.EXPAND, 5)
            bSizer3_1b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText31b = wx.StaticText(self.Panel3b, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText31b.Wrap(-1)
            self.m_staticText31b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer3_1b.Add(self.m_staticText31b, 0, wx.ALL, 5)
            self.Barcode32b = wx.TextCtrl(self.Panel3b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode32b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer3_1b.Add(self.Barcode32b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group3b.Add(bSizer3_1b, 1, wx.EXPAND, 5)
            bSizer3_2b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText33b = wx.StaticText(self.Panel3b, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText33b.Wrap(-1)
            self.m_staticText33b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer3_2b.Add(self.m_staticText33b, 0, wx.ALL, 5)
            self.Process3b = wx.TextCtrl(self.Panel3b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Process3b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer3_2b.Add(self.Process3b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group3b.Add(bSizer3_2b, 1, wx.EXPAND, 5)
            #self.Gauge3b = wx.Gauge(self.Panel3b, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            #self.Gauge3b.SetValue(98)
            self.Gauge3b = wx.Gauge(self.Panel3b, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            self.Gauge3b.SetValue(0)
            self.Gauge3b.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
            Group3b.Add(self.Gauge3b, 0, wx.ALL | wx.EXPAND, 5)
            self.Panel3b.SetSizer(Group3b)
            self.Panel3b.Layout()
            Group3b.Fit(self.Panel3b)
            #gSizerProgress.Add(self.Panel3b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)

            #DUT4B
            self.Panel4b = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            self.Panel4b.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
            self.Panel4b.SetBackgroundColour(self.DisableColor)
            #self.Panel1.SetToolTipString(u'Failed:ERROR')
            _Group4b = wx.StaticBox(self.Panel4b, wx.ID_ANY, u'DUT 4b')
            Group4b = wx.StaticBoxSizer(_Group4b, wx.VERTICAL)
            bSizer4b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText4b = wx.StaticText(self.Panel4b, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText4b.Wrap(-1)
            self.m_staticText4b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer4b.Add(self.m_staticText4b, 0, wx.ALL, 5)
            self.Barcode41b = wx.TextCtrl(self.Panel4b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode41b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer4b.Add(self.Barcode41b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group4b.Add(bSizer4b, 1, wx.EXPAND, 5)
            bSizer4_1b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText41b = wx.StaticText(self.Panel4b, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText41b.Wrap(-1)
            self.m_staticText41b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer4_1b.Add(self.m_staticText41b, 0, wx.ALL, 5)
            self.Barcode42b = wx.TextCtrl(self.Panel4b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode42b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer4_1b.Add(self.Barcode42b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group4b.Add(bSizer4_1b, 1, wx.EXPAND, 5)
            bSizer4_2b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText43b = wx.StaticText(self.Panel4b, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText43b.Wrap(-1)
            self.m_staticText43b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer4_2b.Add(self.m_staticText43b, 0, wx.ALL, 5)
            self.Process4b = wx.TextCtrl(self.Panel4b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Process4b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer4_2b.Add(self.Process4b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group4b.Add(bSizer4_2b, 1, wx.EXPAND, 5)
            #self.Gauge4b = wx.Gauge(self.Panel4b, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            #self.Gauge4b.SetValue(98)
            self.Gauge4b = wx.Gauge(self.Panel4b, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            self.Gauge4b.SetValue(0)
            self.Gauge4b.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
            Group4b.Add(self.Gauge4b, 0, wx.ALL | wx.EXPAND, 5)
            self.Panel4b.SetSizer(Group4b)
            self.Panel4b.Layout()
            Group4b.Fit(self.Panel4b)
            #gSizerProgress.Add(self.Panel4b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)


            #DUT5B
            self.Panel5b = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            self.Panel5b.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
            self.Panel5b.SetBackgroundColour(self.DisableColor)
            #self.Panel1.SetToolTipString(u'Failed:ERROR')
            _Group5b = wx.StaticBox(self.Panel5b, wx.ID_ANY, u'DUT 5b')
            Group5b = wx.StaticBoxSizer(_Group5b, wx.VERTICAL)
            bSizer5b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText5b = wx.StaticText(self.Panel5b, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText5b.Wrap(-1)
            self.m_staticText5b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer5b.Add(self.m_staticText5b, 0, wx.ALL, 5)
            self.Barcode51b = wx.TextCtrl(self.Panel5b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode51b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer5b.Add(self.Barcode51b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group5b.Add(bSizer5b, 1, wx.EXPAND, 5)
            bSizer5_1b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText51b = wx.StaticText(self.Panel5b, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText51b.Wrap(-1)
            self.m_staticText51b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer5_1b.Add(self.m_staticText51b, 0, wx.ALL, 5)
            self.Barcode52b = wx.TextCtrl(self.Panel5b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode52b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer5_1b.Add(self.Barcode52b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group5b.Add(bSizer5_1b, 1, wx.EXPAND, 5)
            bSizer5_2b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText53b = wx.StaticText(self.Panel5b, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText53b.Wrap(-1)
            self.m_staticText53b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer5_2b.Add(self.m_staticText53b, 0, wx.ALL, 5)
            self.Process5b = wx.TextCtrl(self.Panel5b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Process5b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer5_2b.Add(self.Process5b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group5b.Add(bSizer5_2b, 1, wx.EXPAND, 5)
            #self.Gauge5b = wx.Gauge(self.Panel5b, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            #self.Gauge5b.SetValue(98)
            self.Gauge5b = wx.Gauge(self.Panel5b, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            self.Gauge5b.SetValue(0)
            self.Gauge5b.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
            Group5b.Add(self.Gauge5b, 0, wx.ALL | wx.EXPAND, 5)
            self.Panel5b.SetSizer(Group5b)
            self.Panel5b.Layout()
            Group5b.Fit(self.Panel5b)
            #gSizerProgress.Add(self.Panel5b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            
            #DUT6B
            self.Panel6b = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            self.Panel6b.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
            self.Panel6b.SetBackgroundColour(self.DisableColor)
            #self.Panel1.SetToolTipString(u'Failed:ERROR')
            _Group6b = wx.StaticBox(self.Panel6b, wx.ID_ANY, u'DUT 6b')
            Group6b = wx.StaticBoxSizer(_Group6b, wx.VERTICAL)
            bSizer6b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText6b = wx.StaticText(self.Panel6b, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText6b.Wrap(-1)
            self.m_staticText6b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer6b.Add(self.m_staticText6b, 0, wx.ALL, 5)
            self.Barcode61b = wx.TextCtrl(self.Panel6b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode61b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer6b.Add(self.Barcode61b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group6b.Add(bSizer6b, 1, wx.EXPAND, 5)
            bSizer6_1b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText61b = wx.StaticText(self.Panel6b, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText61b.Wrap(-1)
            self.m_staticText61b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer6_1b.Add(self.m_staticText61b, 0, wx.ALL, 5)
            self.Barcode62b = wx.TextCtrl(self.Panel6b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode62b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer6_1b.Add(self.Barcode62b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group6b.Add(bSizer6_1b, 1, wx.EXPAND, 5)
            bSizer6_2b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText63b = wx.StaticText(self.Panel6b, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText63b.Wrap(-1)
            self.m_staticText63b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer6_2b.Add(self.m_staticText63b, 0, wx.ALL, 5)
            self.Process6b = wx.TextCtrl(self.Panel6b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Process6b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer6_2b.Add(self.Process6b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group6b.Add(bSizer6_2b, 1, wx.EXPAND, 5)
            #self.Gauge6b = wx.Gauge(self.Panel6b, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            #self.Gauge6b.SetValue(98)
            self.Gauge6b = wx.Gauge(self.Panel6b, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            self.Gauge6b.SetValue(0)
            self.Gauge6b.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
            Group6b.Add(self.Gauge6b, 0, wx.ALL | wx.EXPAND, 5)
            self.Panel6b.SetSizer(Group6b)
            self.Panel6b.Layout()
            Group6b.Fit(self.Panel6b)
            #gSizerProgress.Add(self.Panel6b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)

            #DUT7B
            self.Panel7b = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            self.Panel7b.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
            self.Panel7b.SetBackgroundColour(self.DisableColor)
            #self.Panel1.SetToolTipString(u'Failed:ERROR')
            _Group7b = wx.StaticBox(self.Panel7b, wx.ID_ANY, u'DUT 7b')
            Group7b = wx.StaticBoxSizer(_Group7b, wx.VERTICAL)
            bSizer7b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText7b = wx.StaticText(self.Panel7b, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText7b.Wrap(-1)
            self.m_staticText7b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer7b.Add(self.m_staticText7b, 0, wx.ALL, 5)
            self.Barcode71b = wx.TextCtrl(self.Panel7b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode71b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer7b.Add(self.Barcode71b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group7b.Add(bSizer7b, 1, wx.EXPAND, 5)
            bSizer7_1b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText71b = wx.StaticText(self.Panel7b, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText71b.Wrap(-1)
            self.m_staticText71b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer7_1b.Add(self.m_staticText71b, 0, wx.ALL, 5)
            self.Barcode72b = wx.TextCtrl(self.Panel7b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode72b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer7_1b.Add(self.Barcode72b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group7b.Add(bSizer7_1b, 1, wx.EXPAND, 5)
            bSizer7_2b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText73b = wx.StaticText(self.Panel7b, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText73b.Wrap(-1)
            self.m_staticText73b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer7_2b.Add(self.m_staticText73b, 0, wx.ALL, 5)
            self.Process7b = wx.TextCtrl(self.Panel7b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Process7b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer7_2b.Add(self.Process7b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group7b.Add(bSizer7_2b, 1, wx.EXPAND, 5)
            #self.Gauge7b = wx.Gauge(self.Panel7b, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            #self.Gauge7b.SetValue(98)
            self.Gauge7b = wx.Gauge(self.Panel7b, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            self.Gauge7b.SetValue(0)
            self.Gauge7b.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
            Group7b.Add(self.Gauge7b, 0, wx.ALL | wx.EXPAND, 5)
            self.Panel7b.SetSizer(Group7b)
            self.Panel7b.Layout()
            Group7b.Fit(self.Panel7b)
            #gSizerProgress.Add(self.Panel7b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)

            #DUT8B
            self.Panel8b = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            self.Panel8b.SetFont(wx.Font(12, 74, 93, 92, False, 'Arial'))
            self.Panel8b.SetBackgroundColour(self.DisableColor)
            #self.Panel1.SetToolTipString(u'Failed:ERROR')
            _Group8b = wx.StaticBox(self.Panel8b, wx.ID_ANY, u'DUT 8b')
            Group8b = wx.StaticBoxSizer(_Group8b, wx.VERTICAL)
            bSizer8b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText8b = wx.StaticText(self.Panel8b, wx.ID_ANY, u'Barcode1', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText8b.Wrap(-1)
            self.m_staticText8b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer8b.Add(self.m_staticText8b, 0, wx.ALL, 5)
            self.Barcode81b = wx.TextCtrl(self.Panel8b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode81b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer8b.Add(self.Barcode81b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group8b.Add(bSizer8b, 1, wx.EXPAND, 5)
            bSizer8_1b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText81b = wx.StaticText(self.Panel8b, wx.ID_ANY, u'Barcode2', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText81b.Wrap(-1)
            self.m_staticText81b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer8_1b.Add(self.m_staticText81b, 0, wx.ALL, 5)
            self.Barcode82b = wx.TextCtrl(self.Panel8b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Barcode82b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer8_1b.Add(self.Barcode82b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group8b.Add(bSizer8_1b, 1, wx.EXPAND, 5)
            bSizer8_2b = wx.BoxSizer(wx.HORIZONTAL)
            self.m_staticText83b = wx.StaticText(self.Panel8b, wx.ID_ANY, u'Process  ', wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_staticText83b.Wrap(-1)
            self.m_staticText83b.SetFont(wx.Font(9, 74, 90, 90, False, wx.EmptyString))
            bSizer8_2b.Add(self.m_staticText83b, 0, wx.ALL, 5)
            self.Process8b = wx.TextCtrl(self.Panel8b, wx.ID_ANY, u'BCBCBCBCBCBC', wx.DefaultPosition, wx.Size(120, -1), wx.TE_READONLY)
            self.Process8b.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
            bSizer8_2b.Add(self.Process8b, 1, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)
            Group8b.Add(bSizer8_2b, 1, wx.EXPAND, 5)
            #self.Gauge8b = wx.Gauge(self.Panel8b, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            #self.Gauge8b.SetValue(98)
            self.Gauge8b = wx.Gauge(self.Panel8b, wx.ID_ANY, 0, wx.DefaultPosition, wx.Size(-1, 10), wx.GA_SMOOTH)
            self.Gauge8b.SetValue(0)
            self.Gauge8b.SetFont(wx.Font(8, 74, 90, 90, False, wx.EmptyString))
            Group8b.Add(self.Gauge8b, 0, wx.ALL | wx.EXPAND, 5)
            self.Panel8b.SetSizer(Group8b)
            self.Panel8b.Layout()
            Group8b.Fit(self.Panel8b)
            #gSizerProgress.Add(self.Panel8b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
        
        if newlayout:
            gSizerProgress.Add(self.Panel1, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel5, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel1b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel5b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel2, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel6, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel2b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel6b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel3, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel7, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel3b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel7b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel4, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel8, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel4b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel8b, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            bSizer3.Add(gSizerProgress, 0, wx.EXPAND, 0)
        else:
            gSizerProgress.Add(self.Panel1, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel2, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel3, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel4, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel5, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel6, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel7, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            gSizerProgress.Add(self.Panel8, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.FIXED_MINSIZE | wx.RESERVE_SPACE_EVEN_IF_HIDDEN | wx.EXPAND, 5)
            bSizer3.Add(gSizerProgress, 1, wx.EXPAND, 0)
        
        work_layer.Add(bSizer3, 1, wx.EXPAND, 5)
        bSizer2.Add(work_layer, 1, wx.EXPAND, 5)

        #LOG AREA
        LogLayer = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"DUT LOG", style=wx.SUNKEN_BORDER), wx.VERTICAL )
        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)
        
        #DUT detail log
        self.m_notebook2 = wx.Notebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_notebook2.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        
        self.m_notebook3 = wx.Notebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_notebook3.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))


        #DUT1A log
        self.m_panel1 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog1a = wx.BoxSizer(wx.VERTICAL)
        self.Message1 = wx.TextCtrl(self.m_panel1, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message1.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message1.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog1a.Add(self.Message1, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel1.SetSizer(bSizerlog1a)
        self.m_panel1.Layout()
        bSizerlog1a.Fit(self.m_panel1)
        self.m_notebook2.AddPage(self.m_panel1, u'DUT 1A', True)
        
        
        #DUT1B log
        self.m_panel1b = wx.Panel(self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog1b = wx.BoxSizer(wx.VERTICAL)
        self.Message1b = wx.TextCtrl(self.m_panel1b, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message1b.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message1b.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog1b.Add(self.Message1b, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel1b.SetSizer(bSizerlog1b)
        self.m_panel1b.Layout()
        bSizerlog1b.Fit(self.m_panel1b)
        self.m_notebook3.AddPage(self.m_panel1b, u'DUT 1B', False)
        
        
        #DUT2A log
        self.m_panel2 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog2a = wx.BoxSizer(wx.VERTICAL)
        self.Message2 = wx.TextCtrl(self.m_panel2, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message2.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message2.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog2a.Add(self.Message2, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel2.SetSizer(bSizerlog2a)
        self.m_panel2.Layout()
        bSizerlog2a.Fit(self.m_panel2)
        self.m_notebook2.AddPage(self.m_panel2, u'DUT 2A', False)
        
        #DUT2B log
        self.m_panel2b = wx.Panel(self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog2b = wx.BoxSizer(wx.VERTICAL)
        self.Message2b = wx.TextCtrl(self.m_panel2b, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message2b.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message2b.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog2b.Add(self.Message2b, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel2b.SetSizer(bSizerlog2b)
        self.m_panel2b.Layout()
        bSizerlog2b.Fit(self.m_panel2b)
        self.m_notebook3.AddPage(self.m_panel2b, u'DUT 2B', False)
        
        #DUT3A log
        self.m_panel3 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog3a = wx.BoxSizer(wx.VERTICAL)
        self.Message3 = wx.TextCtrl(self.m_panel3, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message3.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message3.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog3a.Add(self.Message3, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel3.SetSizer(bSizerlog3a)
        self.m_panel3.Layout()
        bSizerlog3a.Fit(self.m_panel3)
        self.m_notebook2.AddPage(self.m_panel3, u'DUT 3A', False)
        
        #DUT3B log
        self.m_panel3b = wx.Panel(self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog3b = wx.BoxSizer(wx.VERTICAL)
        self.Message3b = wx.TextCtrl(self.m_panel3b, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message3b.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message3b.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog3b.Add(self.Message3b, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel3b.SetSizer(bSizerlog3b)
        self.m_panel3b.Layout()
        bSizerlog3b.Fit(self.m_panel3b)
        self.m_notebook3.AddPage(self.m_panel3b, u'DUT 3B', False)
        
        #DUT4A log
        self.m_panel4 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog4a = wx.BoxSizer(wx.VERTICAL)
        self.Message4 = wx.TextCtrl(self.m_panel4, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message4.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message4.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog4a.Add(self.Message4, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel4.SetSizer(bSizerlog4a)
        self.m_panel4.Layout()
        bSizerlog4a.Fit(self.m_panel4)
        self.m_notebook2.AddPage(self.m_panel4, u'DUT 4A', False)
        
        #DUT4B log
        self.m_panel4b = wx.Panel(self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog4b = wx.BoxSizer(wx.VERTICAL)
        self.Message4b = wx.TextCtrl(self.m_panel4b, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message4b.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message4b.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog4b.Add(self.Message4b, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel4b.SetSizer(bSizerlog4b)
        self.m_panel4b.Layout()
        bSizerlog4b.Fit(self.m_panel4b)
        self.m_notebook3.AddPage(self.m_panel4b, u'DUT 4B', False)
        
        
        #DUT5A log
        self.m_panel5 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog5a = wx.BoxSizer(wx.VERTICAL)
        self.Message5 = wx.TextCtrl(self.m_panel5, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message5.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message5.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog5a.Add(self.Message5, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel5.SetSizer(bSizerlog5a)
        self.m_panel5.Layout()
        bSizerlog5a.Fit(self.m_panel5)
        self.m_notebook2.AddPage(self.m_panel5, u'DUT 5A', False)
        
        #DUT5B log
        self.m_panel5b = wx.Panel(self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog5b = wx.BoxSizer(wx.VERTICAL)
        self.Message5b = wx.TextCtrl(self.m_panel5b, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message5b.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message5b.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog5b.Add(self.Message5b, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel5b.SetSizer(bSizerlog5b)
        self.m_panel5b.Layout()
        bSizerlog5b.Fit(self.m_panel5b)
        self.m_notebook3.AddPage(self.m_panel5b, u'DUT 5B', False)
        
        #DUT6A log
        self.m_panel6 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog6a = wx.BoxSizer(wx.VERTICAL)
        self.Message6 = wx.TextCtrl(self.m_panel6, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message6.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message6.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog6a.Add(self.Message6, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel6.SetSizer(bSizerlog6a)
        self.m_panel6.Layout()
        bSizerlog6a.Fit(self.m_panel6)
        self.m_notebook2.AddPage(self.m_panel6, u'DUT 6A', False)
        
        #DUT6B log
        self.m_panel6b = wx.Panel(self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog6b = wx.BoxSizer(wx.VERTICAL)
        self.Message6b = wx.TextCtrl(self.m_panel6b, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message6b.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message6b.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog6b.Add(self.Message6b, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel6b.SetSizer(bSizerlog6b)
        self.m_panel6b.Layout()
        bSizerlog6b.Fit(self.m_panel6b)
        self.m_notebook3.AddPage(self.m_panel6b, u'DUT 6B', False)
        
        #DUT7A log
        self.m_panel7 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog7a = wx.BoxSizer(wx.VERTICAL)
        self.Message7 = wx.TextCtrl(self.m_panel7, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message7.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message7.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog7a.Add(self.Message7, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel7.SetSizer(bSizerlog7a)
        self.m_panel7.Layout()
        bSizerlog7a.Fit(self.m_panel7)
        self.m_notebook2.AddPage(self.m_panel7, u'DUT 7A', False)
        
        #DUT7B log
        self.m_panel7b = wx.Panel(self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog7b = wx.BoxSizer(wx.VERTICAL)
        self.Message7b = wx.TextCtrl(self.m_panel7b, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message7b.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message7b.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog7b.Add(self.Message7b, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel7b.SetSizer(bSizerlog7b)
        self.m_panel7b.Layout()
        bSizerlog7b.Fit(self.m_panel7b)
        self.m_notebook3.AddPage(self.m_panel7b, u'DUT 7B', False)
        
        #DUT8A log
        self.m_panel8 = wx.Panel(self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog8a = wx.BoxSizer(wx.VERTICAL)
        self.Message8 = wx.TextCtrl(self.m_panel8, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message8.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message8.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog8a.Add(self.Message8, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel8.SetSizer(bSizerlog8a)
        self.m_panel8.Layout()
        bSizerlog8a.Fit(self.m_panel8)
        self.m_notebook2.AddPage(self.m_panel8, u'DUT 8A', False)
        
        #DUT8B log
        self.m_panel8b = wx.Panel(self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizerlog8b = wx.BoxSizer(wx.VERTICAL)
        self.Message8b = wx.TextCtrl(self.m_panel8b, wx.ID_ANY, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2 )
        self.Message8b.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        self.Message8b.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BACKGROUND))
        bSizerlog8b.Add(self.Message8b, 1, wx.EXPAND | wx.ALL, 5)
        self.m_panel8b.SetSizer(bSizerlog8b)
        self.m_panel8b.Layout()
        bSizerlog8b.Fit(self.m_panel8b)
        self.m_notebook3.AddPage(self.m_panel8b, u'DUT 8B', False)
        
        bSizer4.Add(self.m_notebook2, 1, wx.EXPAND | wx.ALL, 5)
        if newlayout:
            bSizer4.Add(self.m_notebook3, 1, wx.EXPAND | wx.ALL, 5)
        LogLayer.Add(bSizer4, 1, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )

        bSizer2.Add(LogLayer, 1, wx.EXPAND, 5)


        bSizer1.Add(bSizer2, 1, wx.EXPAND, 5)

        
        self.SetSizer(bSizer1)
        self.SetAutoLayout(True)

        bSizer1.Fit(self)
        


        #self.StatusBar = self.CreateStatusBar(3, wx.ST_SIZEGRIP, wx.ID_ANY)
        self.statusBar = self.CreateStatusBar(style=0)
        self.StatusBar.SetFont(wx.Font(10, 74, 93, 92, False, 'Arial'))
        #self.StatusBar.SetStatusWidths([-1, 100, 150])
        self.Centre(wx.BOTH)

        self.CheckLED_Dialog = CheckMACLED_Dialog(self)
        
        if newlayout:
            self.DUT_LIST = (
            (self.Panel1,
            _Group1,
            self.Barcode11,
            self.Barcode12,
            self.Process1,
            self.Gauge1,
            self.Message1,
            '1A'),
            (self.Panel2,
            _Group2,
            self.Barcode21,
            self.Barcode22,
            self.Process2,
            self.Gauge2,
            self.Message2,
            '2A'),
            (self.Panel3,
            _Group3,
            self.Barcode31,
            self.Barcode32,
            self.Process3,
            self.Gauge3,
            self.Message3,
            '3A'),
            (self.Panel4,
            _Group4,
            self.Barcode41,
            self.Barcode42,
            self.Process4,
            self.Gauge4,
            self.Message4,
            '4A'),
            (self.Panel5,
            _Group5,
            self.Barcode51,
            self.Barcode52,
            self.Process5,
            self.Gauge5,
            self.Message5,
            '5A'),
            (self.Panel6,
            _Group6,
            self.Barcode61,
            self.Barcode62,
            self.Process6,
            self.Gauge6,
            self.Message6,
            '6A'),
            (self.Panel7,
            _Group7,
            self.Barcode71,
            self.Barcode72,
            self.Process7,
            self.Gauge7,
            self.Message7,
            '7A'),
            (self.Panel8,
            _Group8,
            self.Barcode81,
            self.Barcode82,
            self.Process8,
            self.Gauge8,
            self.Message8,
            '8A'),
            (self.Panel1b,
            _Group1b,
            self.Barcode11b,
            self.Barcode12b,
            self.Process1b,
            self.Gauge1b,
            self.Message1b,
            '1B'),
            (self.Panel2b,
            _Group2b,
            self.Barcode21b,
            self.Barcode22b,
            self.Process2b,
            self.Gauge2b,
            self.Message2b,
            '2B'),
            (self.Panel3b,
            _Group3b,
            self.Barcode31b,
            self.Barcode32b,
            self.Process3b,
            self.Gauge3b,
            self.Message3b,
            '3B'),
            (self.Panel4b,
            _Group4b,
            self.Barcode41b,
            self.Barcode42b,
            self.Process4b,
            self.Gauge4b,
            self.Message4b,
            '4B'),
            (self.Panel5b,
            _Group5b,
            self.Barcode51b,
            self.Barcode52b,
            self.Process5b,
            self.Gauge5b,
            self.Message5b,
            '5B'),
            (self.Panel6b,
            _Group6b,
            self.Barcode61b,
            self.Barcode62b,
            self.Process6b,
            self.Gauge6b,
            self.Message6b,
            '6B'),
            (self.Panel7b,
            _Group7b,
            self.Barcode71b,
            self.Barcode72b,
            self.Process7b,
            self.Gauge7b,
            self.Message7b,
            '7B'),
            (self.Panel8b,
            _Group8b,
            self.Barcode81b,
            self.Barcode82b,
            self.Process8b,
            self.Gauge8b,
            self.Message8b,
            '8B'))
        else:
            self.DUT_LIST = (
            (self.Panel1,
            _Group1,
            self.Barcode11,
            self.Barcode12,
            self.Process1,
            self.Gauge1,
            self.Message1,
            '1'),
            (self.Panel2,
            _Group2,
            self.Barcode21,
            self.Barcode22,
            self.Process2,
            self.Gauge2,
            self.Message2,
            '2'),
            (self.Panel3,
            _Group3,
            self.Barcode31,
            self.Barcode32,
            self.Process3,
            self.Gauge3,
            self.Message3,
            '3'),
            (self.Panel4,
            _Group4,
            self.Barcode41,
            self.Barcode42,
            self.Process4,
            self.Gauge4,
            self.Message4,
            '4'),
            (self.Panel5,
            _Group5,
            self.Barcode51,
            self.Barcode52,
            self.Process5,
            self.Gauge5,
            self.Message5,
            '5'),
            (self.Panel6,
            _Group6,
            self.Barcode61,
            self.Barcode62,
            self.Process6,
            self.Gauge6,
            self.Message6,
            '6'),
            (self.Panel7,
            _Group7,
            self.Barcode71,
            self.Barcode72,
            self.Process7,
            self.Gauge7,
            self.Message7,
            '7'),
            (self.Panel8,
            _Group8,
            self.Barcode81,
            self.Barcode82,
            self.Process8,
            self.Gauge8,
            self.Message8,
            '8'))
        dutid = 1
        for dut in self.DUT_LIST:
            dut[0].SetBackgroundColour(self.DisableColor)
            dut[0].SetToolTipString('')
            dut[1].SetLabel('DUT %s' % dut[7])
            dut[2].SetValue('')
            dut[3].SetValue('')
            dut[4].SetValue('')
            dut[5].SetValue(0)
            dut[6].SetBackgroundColour(wx.BLACK)
            dut[6].SetForegroundColour(wx.WHITE)
            dut[2].SetBackgroundColour(wx.Colour(245, 245, 245))
            dut[3].SetBackgroundColour(wx.Colour(245, 245, 245))
            dut[4].SetBackgroundColour(wx.Colour(245, 245, 245))

        if newlayout:
            self.dutFormat = {'1':(0, 8),
                            '2':(1, 9),
                            '3':(2, 10),
                            '4':(3, 11),
                            '5':(4, 12),
                            '6':(5, 13),
                            '7':(6, 14),
                            '8':(7, 15)}
        else:
            self.dutFormat = {'1':(0, 0),
                            '2':(1, 1),
                            '3':(2, 2),
                            '4':(3, 3),
                            '5':(4, 4),
                            '6':(5, 5),
                            '7':(6, 6),
                            '8':(7, 7)}

        self.AFI = AFI
        self.station = station
        self.StatusBar.SetStatusText('AFI STAND BY')
        self.emp = '00000'
        self.pn = ''
        if len(sys.argv) > 1:
            self.pn = sys.argv[1]
        self.ScanLabel.SetFocus()
        self.ScanLabel.SelectAll()
        self.ConfigStateMachine = StateMachine(self)
        self.InitStr = 'INITIALIZING'
        self.socketPort = socket_port
        self.TFTP = TFTP
        self.Mongo = self.DB = DB
        if self.TFTP:
            self.TFTPServer = ''
        
        self.DB_ip = DB_ip
        logger.debug('MongoDB IP: {}'.format(self.DB_ip))
        self.DB_port = DB_port
        logger.debug('MongoDB Port: {}'.format(self.DB_port))

        self.configfile = ''
        self.Process.Enable(False)
        self.Repair.Enable(False)
        self.m_calibration.Enable(False)
        self.repair = TransferRepair(self)
        self.flash = [wx.Colour(215, 80, 159),
                      wx.Colour(99, 0, 255),
                      wx.Colour(0, 255, 255),
                      wx.Colour(204, 204, 0),
                      self.YellowColor]
        self.Buffers = [[Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self)], [Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self),
          Buffer(self)]]
        self.Buffers[0][0].CardName = 'CCU'
        self.Buffers[1][0].CardName = 'CCU'

        self.fixture = fixture
        for i in range(8, 16):
            self.Buffers[0][i].CardName = 'DUT\t%s' % (i - 8)
            self.Buffers[1][i].CardName = 'DUT\t%s' % (i - 8)
        
        
        
        if self.fixture:
            wx.FutureCall(500, self._Pi_Thread)
            wx.FutureCall(5000, self.fixtureInit)
            wx.FutureCall(20000, self.cytAction)

        if TFTP:
            wx.FutureCall(1000, self._TFTPTransfer_Thread)
        
        if DB:
            wx.FutureCall(3000, self.Mongo_Thread)
        
        wx.FutureCall(15000, self._Status_Thread)
        wx.FutureCall(500, self._Result_Panel_Thread)

        self.ScanLabel.Bind(wx.EVT_TEXT_ENTER, self.ScanLabelEnter)
        self.Bind(wx.EVT_MENU, self.Show_L_CCU, id=self.M_L_CCU.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S0, id=self.M_L_S0.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S1, id=self.M_L_S1.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S2, id=self.M_L_S2.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S3, id=self.M_L_S3.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S4, id=self.M_L_S4.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S5, id=self.M_L_S5.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_S6, id=self.M_L_S6.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT0, id=self.M_L_DUT0.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT1, id=self.M_L_DUT1.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT2, id=self.M_L_DUT2.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT3, id=self.M_L_DUT3.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT4, id=self.M_L_DUT4.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT5, id=self.M_L_DUT5.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT6, id=self.M_L_DUT6.GetId())
        self.Bind(wx.EVT_MENU, self.Show_L_DUT7, id=self.M_L_DUT7.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_CCU, id=self.M_R_CCU.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S0, id=self.M_R_S0.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S1, id=self.M_R_S1.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S2, id=self.M_R_S2.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S3, id=self.M_R_S3.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S4, id=self.M_R_S4.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S5, id=self.M_R_S5.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_S6, id=self.M_R_S6.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT0, id=self.M_R_DUT0.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT1, id=self.M_R_DUT1.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT2, id=self.M_R_DUT2.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT3, id=self.M_R_DUT3.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT4, id=self.M_R_DUT4.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT5, id=self.M_R_DUT5.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT6, id=self.M_R_DUT6.GetId())
        self.Bind(wx.EVT_MENU, self.Show_R_DUT7, id=self.M_R_DUT7.GetId())
        self.Bind(wx.EVT_MENU, self.Debug_Message, self.Process)
        self.Bind(wx.EVT_MENU, self.show_help_message, self.About)
        self.Bind(wx.EVT_MENU, self.show_repair, self.Repair)
        self.Bind(wx.EVT_MENU, self.calibration, self.m_calibration)
        
        self.raspiNotExit = None
        self.fixtureNotWork = None
        self.ScanLabel.Enable(False)
        
    def calibration(self, event):
        if self.configfile:
            CalibrationGUI.AFI_Clibration_Tools(self).Show(True)
        else:
            dlgmsg = wx.MessageDialog(self, 'Please selection model', 'Infomation', wx.OK | wx.ICON_INFORMATION)
            dlgmsg.Center()
            dlgmsg.ShowModal()
            dlgmsg.Destroy()

    def show_help_message(self, event):
        HelpGui(self).Show(True)

    def show_repair(self, event):
        self.repair.ShowModal()

    def Debug_Message(self, event):
        dlgtext = wx.TextEntryDialog(self, 'Please input password :', 'Password', '')
        if dlgtext.ShowModal() == wx.ID_OK:
            if dlgtext.GetValue() != 'hitron':
                dlgmsg = wx.MessageDialog(self, 'Input password error', 'Password', wx.OK | wx.ICON_ERROR)
                dlgmsg.Center()
                dlgmsg.ShowModal()
                dlgmsg.Destroy()
            elif self.configfile:
                ProcessGui(self).Show(True)
            else:
                dlgmsg = wx.MessageDialog(self, 'Please selection model', 'Infomation', wx.OK | wx.ICON_INFORMATION)
                dlgmsg.Center()
                dlgmsg.ShowModal()
                dlgmsg.Destroy()
        dlgtext.Destroy()

    def Show_L_CCU(self, event):
        self.Buffers[0][0].Show(True)
        self.Buffers[0][0].SetTitle('Left CCU Console - Socket 9')

    def Show_L_S0(self, event):
        self.Buffers[0][1].SetTitle('Left S0 Console - Socket 8')
        self.Buffers[0][1].Show(True)

    def Show_L_S1(self, event):
        self.Buffers[0][2].SetTitle('Left S1 Console - Socket 7')
        self.Buffers[0][2].Show(True)

    def Show_L_S2(self, event):
        self.Buffers[0][3].SetTitle('Left S2 Console - Socket 6')
        self.Buffers[0][3].Show(True)

    def Show_L_S3(self, event):
        self.Buffers[0][4].SetTitle('Left S3 Console - Socket 5')
        self.Buffers[0][4].Show(True)

    def Show_L_S4(self, event):
        self.Buffers[0][5].SetTitle('Left S4 Console - Socket 4')
        self.Buffers[0][5].Show(True)

    def Show_L_S5(self, event):
        self.Buffers[0][6].SetTitle('Left S5 Console - Socket 3')
        self.Buffers[0][6].Show(True)

    def Show_L_S6(self, event):
        self.Buffers[0][7].SetTitle('Left S6 Console - Socket 2')
        self.Buffers[0][7].Show(True)

    def Show_L_DUT0(self, event):
        self.Buffers[0][8].SetTitle('Left DUT 0 Console - Socket 9')
        self.Buffers[0][8].Show(True)

    def Show_L_DUT1(self, event):
        self.Buffers[0][9].SetTitle('Left DUT 1 Console - Socket 9')
        self.Buffers[0][9].Show(True)

    def Show_L_DUT2(self, event):
        self.Buffers[0][10].SetTitle('Left DUT 2 Console - Socket 9')
        self.Buffers[0][10].Show(True)

    def Show_L_DUT3(self, event):
        self.Buffers[0][11].SetTitle('Left DUT 3 Console - Socket 9')
        self.Buffers[0][11].Show(True)

    def Show_L_DUT4(self, event):
        self.Buffers[0][12].SetTitle('Left DUT 4 Console - Socket 9')
        self.Buffers[0][12].Show(True)

    def Show_L_DUT5(self, event):
        self.Buffers[0][13].SetTitle('Left DUT 5 Console - Socket 9')
        self.Buffers[0][13].Show(True)

    def Show_L_DUT6(self, event):
        self.Buffers[0][14].SetTitle('Left DUT 6 Console - Socket 9')
        self.Buffers[0][14].Show(True)

    def Show_L_DUT7(self, event):
        self.Buffers[0][15].SetTitle('Left DUT 7 Console - Socket 9')
        self.Buffers[0][15].Show(True)

    def Show_R_CCU(self, event):
        self.Buffers[1][0].SetTitle('Right CCU Console - Socket 10')
        self.Buffers[1][0].Show(True)

    def Show_R_S0(self, event):
        self.Buffers[1][1].SetTitle('Right S0 Console - Socket 11')
        self.Buffers[1][1].Show(True)

    def Show_R_S1(self, event):
        self.Buffers[1][2].SetTitle('Right S1 Console - Socket 12')
        self.Buffers[1][2].Show(True)

    def Show_R_S2(self, event):
        self.Buffers[1][3].SetTitle('Right S2 Console - Socket 13')
        self.Buffers[1][3].Show(True)

    def Show_R_S3(self, event):
        self.Buffers[1][4].SetTitle('Right S3 Console - Socket 14')
        self.Buffers[1][4].Show(True)

    def Show_R_S4(self, event):
        self.Buffers[1][5].SetTitle('Right S4 Console - Socket 15')
        self.Buffers[1][5].Show(True)

    def Show_R_S5(self, event):
        self.Buffers[1][6].SetTitle('Right S5 Console - Socket 16')
        self.Buffers[1][6].Show(True)

    def Show_R_S6(self, event):
        self.Buffers[1][7].SetTitle('Right S6 Console - Socket 17')
        self.Buffers[1][7].Show(True)

    def Show_R_DUT0(self, event):
        self.Buffers[1][8].SetTitle('Right DUT 0 Console - Socket 10')
        self.Buffers[1][8].Show(True)

    def Show_R_DUT1(self, event):
        self.Buffers[1][9].SetTitle('Right DUT 1 Console - Socket 10')
        self.Buffers[1][9].Show(True)

    def Show_R_DUT2(self, event):
        self.Buffers[1][10].SetTitle('Right DUT 2 Console - Socket 10')
        self.Buffers[1][10].Show(True)

    def Show_R_DUT3(self, event):
        self.Buffers[1][11].SetTitle('Right DUT 3 Console - Socket 10')
        self.Buffers[1][11].Show(True)

    def Show_R_DUT4(self, event):
        self.Buffers[1][12].SetTitle('Right DUT 4 Console - Socket 10')
        self.Buffers[1][12].Show(True)

    def Show_R_DUT5(self, event):
        self.Buffers[1][13].SetTitle('Right DUT 5 Console - Socket 10')
        self.Buffers[1][13].Show(True)

    def Show_R_DUT6(self, event):
        self.Buffers[1][14].SetTitle('Right DUT 6 Console - Socket 10')
        self.Buffers[1][14].Show(True)

    def Show_R_DUT7(self, event):
        self.Buffers[1][15].SetTitle('Right DUT 7 Console - Socket 10')
        self.Buffers[1][15].Show(True)

    def _Result_Panel_Thread(self):
        for i in range(16):
            if self.DUT_LIST[i][0].GetLabel() == '3':
                if self.DUT_LIST[i][0].GetBackgroundColour() == self.YellowColor:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.flash[0])
                    self.DUT_LIST[i][0].Refresh()
                else:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.YellowColor)
                    self.DUT_LIST[i][0].Refresh()
            if self.DUT_LIST[i][0].GetLabel() == '4':
                if self.DUT_LIST[i][0].GetBackgroundColour() == self.YellowColor:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.flash[1])
                    self.DUT_LIST[i][0].Refresh()
                else:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.YellowColor)
                    self.DUT_LIST[i][0].Refresh()
            if self.DUT_LIST[i][0].GetLabel() == '5':
                if self.DUT_LIST[i][0].GetBackgroundColour() == self.YellowColor:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.flash[2])
                    self.DUT_LIST[i][0].Refresh()
                else:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.YellowColor)
                    self.DUT_LIST[i][0].Refresh()
            if self.DUT_LIST[i][0].GetLabel() == '6':
                if self.DUT_LIST[i][0].GetBackgroundColour() == self.YellowColor:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.flash[3])
                    self.DUT_LIST[i][0].Refresh()
                else:
                    self.DUT_LIST[i][0].SetBackgroundColour(self.YellowColor)
                    self.DUT_LIST[i][0].Refresh()

        wx.FutureCall(500, self._Result_Panel_Thread)

    def _TFTPTransfer_Thread(self):
        if self.TFTP:
            if type(self.TFTPServer) != type(''):
                if self.TFTPServer.serving:
                    if self.TFTPServerState.GetBackgroundColour() == self.YellowColor:
                        self.TFTPServerState.SetBackgroundColour(wx.Colour(153, 255, 153))
                    else:
                        self.TFTPServerState.SetBackgroundColour(self.YellowColor)
                else:
                    self.TFTPServerState.SetBackgroundColour(self.RedColor)
            self.TFTPServerState.Refresh()

        if type(self.TFTPServer) != type(''):
            msg = self.TFTPServer.text.split('\n')[-1]
            if msg:
                self.TFTPTransferFile.SetLabel(msg)
        
        wx.FutureCall(1000, self._TFTPTransfer_Thread)
     
    def Mongo_Thread(self):
        if self.DB:
            msg = self.DB_ip+':'+self.DB_port
            self.DBip.SetLabel(msg)
        #wx.FutureCall(2000, self.Mongo_Thread)   
    
    def _Status_Thread(self):
        if self.LabelName.GetLabel() == 'DEMO':
           self.pn = GUIDirectory+'\\demo.ini'
           self.ScanLabel.SetValue('START')
           self.ScanLabelEnter('enter')
        
        if self.LabelName.GetLabel() == 'MFG' and self.pn:
            self.ScanLabel.SetValue('START')
            self.ScanLabelEnter('enter')
            
        if self.LabelName.GetLabel() == 'INIT':
            StatusBarStr = self.StatusBar.GetStatusText()
            if len(StatusBarStr) < len(self.InitStr) + 7:
                self.StatusBar.SetStatusText(StatusBarStr + '.')
            else:
                self.StatusBar.SetStatusText(self.InitStr)

        if auto_reader:
            if self.LabelName.GetLabel() == 'DUT ID':
                try:
                    global fixtureStatus
                    self.resp=None
                    for val in fixtureStatus.keys():
                        self.resp = self.ConfigStateMachine.getBarcode(fixtureStatus.get(str(val)), socket_port)
                        logger.info('{}: getBarcode - {}'.format(fixtureStatus.get(str(val)), self.resp))
                        if self.resp:
                            if val == self.resp.split(':')[0][0]:
                                self.ScanLabel.SetValue(str(self.resp.split(':')[0]))
                                self.ScanLabelEnter('enter')
                                time.sleep(0.1)
                            
                            if self.LabelName.GetLabel() == 'MAC':
                                self.ScanLabel.SetValue(str(self.resp.split(':')[-1].split(',')[0].strip()))
                                self.ScanLabelEnter('enter')
                                time.sleep(0.1)
                            
                            if self.LabelName.GetLabel() == 'CSN':
                                self.ScanLabel.SetValue(str(self.resp.split(',')[-1].strip()))
                                self.ScanLabelEnter('enter')
                                time.sleep(0.1)
                        
                except IOError, msg:
                    self.ScanLabel.Enable(False)
                    error_class = msg.__class__.__name__
                    detail = msg.args[0]
                    cl, exc, tb = sys.exc_info()
                    lastCallStack = traceback.extract_tb(tb)[-1]
                    fileName = lastCallStack[0] 
                    lineNum = lastCallStack[1]
                    funcName = lastCallStack[2]
                    errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
                    logger.error('{} exception: {}'.format(fixtureStatus.get(str(val)), errMsg))

        wx.FutureCall(2000, self._Status_Thread)

    def _Pi_Thread(self):
        global afiStation
        global fixtureStatus
        def checkPing(idx, hostname):
            global lock
            try:
                #response = os.popen(GUIDirectory + "\\Fping.exe {} -n 10 -w 10 -t".format(hostname)).read()
                response = os.popen(GUIDirectory + "\\Fping.exe {} -n 3 -w 5 -t".format(hostname)).read()
                logger.debug('{}: {}'.format(hostname, response))
                lock.acquire()
                if int(response.split('Lost = ')[-1].split('(')[0].strip()) <= 2:
                    afiStation.update({str(idx):str(fixture_pi.get(idx))})
                else:
                    if str(idx) in afiStation:
                        del afiStation[str(idx)]
                    if str(idx) in fixtureStatus:
                        del fixtureStatus[str(idx)]
                lock.release()
            except Exception as msg:
                error_class = msg.__class__.__name__
                detail = msg.args[0]
                cl, exc, tb = sys.exc_info()
                lastCallStack = traceback.extract_tb(tb)[-1]
                fileName = lastCallStack[0] 
                lineNum = lastCallStack[1]
                funcName = lastCallStack[2]
                errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
                lock.release()
                logger.error('{} exception: {}'.format(hostname, errMsg))
            
        def panelSetUnwork(idx):
            if idx:
                raspiAlive = self.dutFormat[idx]
                self.DUT_LIST[raspiAlive[0]][0].SetBackgroundColour(self.fixtureUnworkColor)
                self.DUT_LIST[raspiAlive[0]][0].Refresh()
                self.DUT_LIST[raspiAlive[1]][0].SetBackgroundColour(self.fixtureUnworkColor)
                self.DUT_LIST[raspiAlive[1]][0].Refresh()

        def panelstatus(raspiNotWorkdict):
            set1 = set(self.dutFormat.items())
            set2 = set(raspiNotWorkdict.items())
            self.alive = dict(set1 ^ set2)
            
            for _dut in raspiNotWorkdict.keys():
                locate = raspiNotWorkdict[_dut]
                self.DUT_LIST[locate[0]][0].SetBackgroundColour(self.UnworkColor)
                self.DUT_LIST[locate[0]][0].Refresh()
                self.DUT_LIST[locate[1]][0].SetBackgroundColour(self.UnworkColor)
                self.DUT_LIST[locate[1]][0].Refresh()
                logger.warning('RasPi{}: Not Work'.format(_dut))
                
            for _dut in self.alive.keys():
                if _dut not in fixtureStatus:
                    panelSetUnwork(_dut)
                    self.StatusBar.SetStatusText('Fixture{}: Not Work'.format(_dut))
                    logger.warning('Fixture{}: Not Work'.format(_dut))
                    continue
                else:
                    locate = self.dutFormat[_dut]
                    if self.DUT_LIST[locate[0]][0].GetBackgroundColour()==self.UnworkColor:
                        self.DUT_LIST[locate[0]][0].SetBackgroundColour(self.DisableColor)
                        self.DUT_LIST[locate[0]][0].Refresh()
                    if self.DUT_LIST[locate[1]][0].GetBackgroundColour()==self.UnworkColor:
                        self.DUT_LIST[locate[1]][0].SetBackgroundColour(self.DisableColor)
                        self.DUT_LIST[locate[1]][0].Refresh()
        
        t_list = list()    
        for idx in fixture_pi.keys():
            thread = threading.Thread(target=checkPing,
                                       args=(idx,fixture_pi.get(idx),),
                                       name='piPing')
            t_list.append(thread)
            
        for t in t_list:
            time.sleep(0.1)
            t.start()  
        
        '''for t in t_list:
            t.join()'''
            
        self.dutNotWork = self.dutFormat.copy()
        for dut in afiStation.keys():
            del self.dutNotWork[dut]
        
        if not self.raspiNotExit:
            self.raspiNotExit = self.dutNotWork.copy()
            panelstatus(self.dutNotWork)
        else:
            if self.dutNotWork.keys() != self.raspiNotExit.keys():
                panelstatus(self.dutNotWork)
                self.raspiNotExit = self.dutNotWork.copy()
        
        logger.info('alive raspi: {}'.format(afiStation))    
        logger.debug('raspiNotWork:{}'.format(self.dutNotWork)) 
        logger.debug('raspiNotExit:{}'.format(self.raspiNotExit))
        wx.FutureCall(10000, self._Pi_Thread)
        

    def fixtureInit(self):
        global afiStation
        global fixtureStatus
        def fixtureHome(idx, port):
            def detectInit(idx, port, tid):
                raspiIp = afiStation.get(str(idx))
                self.ConfigStateMachine.carryAct(raspiIp, port, tid, idx)

            try:
                resp = self.ConfigStateMachine.fixtureInit(afiStation.get(str(idx)), port)
                logger.info('{}: {}'.format(afiStation.get(str(idx)),resp))
                if 'MACHINE:READY' not in resp:
                    panelSetUnwork(idx)
                    if bool(fixtureStatus):
                        if str(idx) in fixtureStatus:
                            del fixtureStatus[str(idx)]
                    self.StatusBar.SetStatusText('Fixture{}: Not Work'.format(idx))
                    logger.warning('Fixture{}: Init fail'.format(idx))
                else:
                    fixtureStatus.update({str(idx):str(fixture_pi.get(idx))})
                    panelSetWork(idx)
                    self.ScanLabel.Enable(True)
                    
                    daemonList = list()
                    for tid in ['A', 'B']:
                        dProcess = threading.Thread(target=detectInit,
                        args=(idx, socket_port, tid),
                        name='detectFeature')
                        daemonList.append(dProcess)

                    for daemon in daemonList:
                        daemon.start()
                        time.sleep(0.1)

            except Exception as msg:
                error_class = msg.__class__.__name__
                detail = msg.args[0]
                cl, exc, tb = sys.exc_info()
                lastCallStack = traceback.extract_tb(tb)[-1]
                fileName = lastCallStack[0] 
                lineNum = lastCallStack[1]
                funcName = lastCallStack[2]
                errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
                logger.error('{} exception: {}'.format(afiStation.get(str(idx)), errMsg))
                
        def panelSetUnwork(idx):
            if idx:
                raspiAlive = self.dutFormat[idx]
                self.DUT_LIST[raspiAlive[0]][0].SetBackgroundColour(self.fixtureUnworkColor)
                self.DUT_LIST[raspiAlive[0]][0].Refresh()
                self.DUT_LIST[raspiAlive[1]][0].SetBackgroundColour(self.fixtureUnworkColor)
                self.DUT_LIST[raspiAlive[1]][0].Refresh()

        def panelSetWork(idx):
            if idx:
                fixtureAlive = self.dutFormat[idx]
                if self.DUT_LIST[fixtureAlive[0]][0].GetBackgroundColour()==self.fixtureUnworkColor:
                    self.DUT_LIST[fixtureAlive[0]][0].SetBackgroundColour(self.DisableColor)
                    self.DUT_LIST[fixtureAlive[0]][0].Refresh()
                if self.DUT_LIST[fixtureAlive[1]][0].GetBackgroundColour()==self.fixtureUnworkColor:
                    self.DUT_LIST[fixtureAlive[1]][0].SetBackgroundColour(self.DisableColor)
                    self.DUT_LIST[fixtureAlive[1]][0].Refresh()
        
        p_list = list()

        for idx in afiStation.keys():
            if idx not in fixtureStatus:
                process = threading.Thread(target=fixtureHome,
                                        args=(idx, socket_port),
                                        name='fixtureCheck')
                p_list.append(process)

        for p in p_list:
            p.start()
            time.sleep(0.1)
        
        wx.FutureCall(35000, self.fixtureInit)

    def cytAction(self):
        global afiStation
        global fixtureStatus        
        def posGet(idx, port):
            try:
                resp = self.ConfigStateMachine.fixtureAction(afiStation.get(str(idx)), port, 'carrierGet')
                if 'failed' in resp:
                    panelSetUnwork(idx)
                    if bool(fixtureStatus):
                        if str(idx) in fixtureStatus:
                            del fixtureStatus[str(idx)]
                self.StatusBar.SetStatusText('Fixture{}:{}'.format(idx,resp))
                logger.info('{}: Fixture{} {}'.format(afiStation.get(str(idx)),idx,resp))    
            except Exception as msg:
                error_class = msg.__class__.__name__
                detail = msg.args[0]
                cl, exc, tb = sys.exc_info()
                lastCallStack = traceback.extract_tb(tb)[-1]
                fileName = lastCallStack[0] 
                lineNum = lastCallStack[1]
                funcName = lastCallStack[2]
                errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
                logger.error('{} exception: {}'.format(afiStation.get(str(idx)), errMsg))
        
        def panelSetUnwork(idx):
            if idx:
                raspiAlive = self.dutFormat[idx]
                self.DUT_LIST[raspiAlive[0]][0].SetBackgroundColour(self.fixtureUnworkColor)
                self.DUT_LIST[raspiAlive[0]][0].Refresh()
                self.DUT_LIST[raspiAlive[1]][0].SetBackgroundColour(self.fixtureUnworkColor)
                self.DUT_LIST[raspiAlive[1]][0].Refresh()
        
        d_list = list()
        for idx in afiStation.keys():
            if str(idx) in fixtureStatus.keys():
                process = threading.Thread(target=posGet,
                                        args=(idx, socket_port),
                                        name='carrierGet')
                d_list.append(process)

        for d in d_list:
            d.start()
            time.sleep(0.1)
            
        #for d in d_list:
        #    d.join()

        wx.FutureCall(10000, self.cytAction)    

    def ScanLabelEnter(self, event):
        self.ConfigStateMachine.state(self.LabelName.GetLabel())
        self.ScanLabel.SetFocus()
        self.ScanLabel.SelectAll()
        self.ScanLabel.Refresh()


class HelpGui(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title='About ', pos=wx.DefaultPosition, size=wx.Size(634, 481), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)
        self.about = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS)
        bSizer2.Add(self.about, 1, wx.EXPAND | wx.ALL, 5)
        self.SetSizer(bSizer2)
        self.Layout()
        self.Centre(wx.BOTH)
        self.about.LoadFile(GUIDirectory + '\\lib\\ReadMe.txt')

    def __del__(self):
        pass


class ProcessGui(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition, size=wx.Size(346, 300), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)
        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer2.AddButton(self.m_sdbSizer2OK)
        self.m_sdbSizer2Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer2.AddButton(self.m_sdbSizer2Cancel)
        m_sdbSizer2.Realize()
        bSizer1.Add(m_sdbSizer2, 0, 0, 5)
        self.m_scrolledWindow1 = wx.ScrolledWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL | wx.VSCROLL)
        self.m_scrolledWindow1.SetScrollRate(5, 5)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)
        self.checkboxs = []
        self.configfile = parent.configfile
        self.ConfigFile = ConfigParser.SafeConfigParser()
        if self.ConfigFile.read(self.configfile):
            self.Flows = map(strip, self.ConfigFile.get('Base', 'Flows').split(','))
            for flow in self.Flows:
                fname = self.ConfigFile.get(flow, 'FlowName')
                fenable = self.ConfigFile.get(flow, 'Enable').strip()
                self.checkboxs.append(wx.CheckBox(self.m_scrolledWindow1, wx.ID_ANY, fname, wx.DefaultPosition, wx.DefaultSize, 0))
                bSizer2.Add(self.checkboxs[-1], 0, wx.ALL, 5)
                self.checkboxs[-1].SetValue(int(fenable))

        self.m_scrolledWindow1.SetSizer(bSizer2)
        self.m_scrolledWindow1.Layout()
        bSizer2.Fit(self.m_scrolledWindow1)
        bSizer1.Add(self.m_scrolledWindow1, 1, wx.EXPAND | wx.ALL, 5)
        self.SetSizer(bSizer1)
        self.Layout()
        self.Centre(wx.BOTH)
        self.m_sdbSizer2OK.Bind(wx.EVT_BUTTON, self.save)
        self.m_sdbSizer2Cancel.Bind(wx.EVT_BUTTON, self.exit)

    def __del__(self):
        pass

    def save(self, event):
        try:
            i = 0
            for flow in self.Flows:
                val = '0'
                if self.checkboxs[i].GetValue():
                    val = '1'
                self.ConfigFile.set(flow, 'Enable', val)
                i += 1

            fp = open(self.configfile, 'w')
            self.ConfigFile.write(fp)
            fp.close()
            dlgmsg = wx.MessageDialog(self, 'Config set success', 'Infomation', wx.OK | wx.ICON_INFORMATION)
            dlgmsg.Center()
            dlgmsg.ShowModal()
            dlgmsg.Destroy()
        except:
            dlgmsg = wx.MessageDialog(self, 'Config set failed', 'Infomation', wx.OK | wx.ICON_ERROR)
            dlgmsg.Center()
            dlgmsg.ShowModal()
            dlgmsg.Destroy()

    def exit(self, event):
        self.Show(False)


class CheckMACLED_Dialog(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'Check LED & MAC', pos=wx.DefaultPosition, size=wx.Size(224, 160), style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)
        self.DUT = wx.StaticText(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE)
        self.DUT.Wrap(-1)
        self.DUT.SetFont(wx.Font(28, 74, 90, 92, False, 'Arial Black'))
        self.DUT.SetForegroundColour(wx.Colour(0, 0, 255))
        self.DUT.SetBackgroundColour(wx.Colour(85, 141, 255))
        bSizer1.Add(self.DUT, 0, wx.ALL | wx.EXPAND, 5)
        self.LabelName = wx.StaticText(self, wx.ID_ANY, u'SCAN MAC', wx.DefaultPosition, wx.DefaultSize, 0)
        self.LabelName.Wrap(-1)
        self.LabelName.SetFont(wx.Font(10, 74, 90, 90, False, 'Arial'))
        bSizer1.Add(self.LabelName, 0, wx.ALL | wx.EXPAND, 5)
        self.ScanLabel = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(200, -1), wx.TE_NO_VSCROLL | wx.TE_PROCESS_ENTER | wx.TE_PROCESS_TAB)
        self.ScanLabel.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        bSizer1.Add(self.ScanLabel, 0, wx.ALL | wx.EXPAND, 5)
        self.SetSizer(bSizer1)
        self.Layout()
        self.Label_ = ''
        self.Centre(wx.BOTH)
        self.ScanLabel.SetFocus()
        self.ScanLabel.SelectAll()
        self.ScanLabel.Bind(wx.EVT_TEXT_ENTER, self.Scaned)

    def __del__(self):
        pass

    def Scaned(self, event):
        self.Label_ = self.ScanLabel.GetValue()
        self.ScanLabel.SetFocus()
        self.ScanLabel.SelectAll()
        self.ScanLabel.Refresh()


class TransferRepair(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'To Repair', pos=wx.DefaultPosition, size=wx.Size(379, 123), style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)
        #bSizer2.AddSpacer((0, 0), 1, wx.EXPAND, 5)
        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY, u'DUT ID', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        self.m_staticText2.SetFont(wx.Font(10, 74, 93, 92, False, 'Arial'))
        bSizer4.Add(self.m_staticText2, 0, wx.ALL, 5)
        dutidChoices = [u'DUT 1',
         u'DUT 2',
         u'DUT 3',
         u'DUT 4',
         u'DUT 5',
         u'DUT 6',
         u'DUT 7',
         u'DUT 8']
        self.dutid = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(60, -1), dutidChoices, 0)
        self.dutid.SetSelection(0)
        self.dutid.SetFont(wx.Font(9, 74, 90, 90, False, 'Arial'))
        bSizer4.Add(self.dutid, 0, wx.ALL, 5)
        self.m_staticText3 = wx.StaticText(self, wx.ID_ANY, u'MAC', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        self.m_staticText3.SetFont(wx.Font(10, 74, 93, 92, False, wx.EmptyString))
        bSizer4.Add(self.m_staticText3, 0, wx.ALL, 5)
        self.mac = wx.TextCtrl(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.Size(110, -1), style=wx.TE_PROCESS_ENTER)
        self.mac.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        bSizer4.Add(self.mac, 0, wx.ALL, 5)
        self.m_staticText5 = wx.StaticText(self, wx.ID_ANY, u'-', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText5.Wrap(-1)
        self.m_staticText5.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        bSizer4.Add(self.m_staticText5, 0, wx.ALL, 5)
        self.errcode = wx.TextCtrl(self, wx.ID_ANY, u'', wx.DefaultPosition, wx.Size(60, -1), style=wx.TE_PROCESS_ENTER)
        self.errcode.SetFont(wx.Font(10, 74, 90, 90, False, wx.EmptyString))
        bSizer4.Add(self.errcode, 0, wx.ALL, 5)
        bSizer2.Add(bSizer4, 1, wx.EXPAND, 5)
        #bSizer2.AddSpacer((0, 0), 1, wx.EXPAND, 5)
        self.SetSizer(bSizer2)
        self.Layout()
        self.Centre(wx.BOTH)
        self.dutid.Bind(wx.EVT_CHOICE, self.dutchoice)
        self.mac.Bind(wx.EVT_TEXT_ENTER, self.MACEnter)
        self.errcode.Bind(wx.EVT_TEXT_ENTER, self.ErrCodeEnter)
        self.parent = parent
        self.mac.SetFocus()
        self.mac.SelectAll()

    def tr(self):
        if self.parent.StatusBar.GetStatusText(2) == 'MES Offline':
            dlg = wx.MessageDialog(self, 'MES Off line', 'MES Status', wx.OK | wx.ICON_INFORMATION)
            dlg.Center()
            dlg.ShowModal()
            dlg.Destroy()
            return
        dlgmsg = wx.MessageDialog(self, 'Confirm to repair ?', 'Repair', wx.YES_NO | wx.ICON_INFORMATION)
        dlgmsg.Center()
        flag = dlgmsg.ShowModal()
        dlgmsg.Destroy()
        if flag == 5103:
            msg = failtravel(self.parent.emp, self.mac.GetValue(), self.errcode.GetValue())
            dlg = wx.MessageDialog(self, msg[-1], 'To repair', wx.OK | wx.ICON_INFORMATION)
            dlg.Center()
            dlg.ShowModal()
            dlg.Destroy()
            self.mac.SetFocus()

    def dutchoice(self, evt):
        self.mac.SetValue(self.parent.DUT_LIST[self.dutid.GetSelection()][2].GetValue())
        self.errcode.SetValue(self.parent.DUT_LIST[self.dutid.GetSelection()][1].GetLabel().split()[-1])
        self.tr()

    def MACEnter(self, evt):
        self.errcode.SetFocus()
        self.errcode.SelectAll()

    def ErrCodeEnter(self, evt):
        self.tr()


class App(wx.App):

    def OnInit(self):
        try:
            wx.InitAllImageHandlers()
            self.main = MainGUI(None)
            self.main.Show(True)
            self.SetTopWindow(self.main)
        except Exception as e:
            logger.error('{}'.format(e))

        return True


def main():
    application = App(0)
    application.MainLoop()


if __name__ == '__main__':
    main()
