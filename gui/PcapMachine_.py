# Embedded file name: gui\PcapMachine.pyc
from winpcapy import *
from ctypes import *
import thread, time
import socket as sk

class S_un_b(Structure):
    _fields_ = [('s_b1', c_ubyte),
     ('s_b2', c_ubyte),
     ('s_b3', c_ubyte),
     ('s_b4', c_ubyte)]


class S_un_w(Structure):
    _fields_ = [('s_wl', c_ushort), ('s_w2', c_ushort)]


class S_un(Union):
    _fields_ = [('S_un_b', S_un_b), ('S_un_w', S_un_w), ('S_addr', c_ulong)]


class in_addr(Structure):
    _fields_ = [('S_un', S_un)]


class sockaddr_in(Structure):
    _fields_ = [('sin_family', c_ushort),
     ('sin_port', c_ushort),
     ('sin_addr', in_addr),
     ('sin_zero', c_char * 8)]


class _S6_un(Union):
    _fields_ = [('_S6_u8', c_ubyte * 16), ('_S6_u16', c_ushort * 8), ('_S6_u32', c_ulong * 4)]


class in6_addr(Structure):
    _fields_ = [('_S6_un', _S6_un)]


class sockaddr_in6(Structure):
    _fields_ = [('sin6_family', c_short),
     ('sin6_port', c_ushort),
     ('sin6_flowinfo', c_ulong),
     ('sin6_addr', in6_addr),
     ('sin6_scope_id', c_ulong)]


def iptos(in_):
    return '%d.%d.%d.%d' % (in_.s_b1,
     in_.s_b2,
     in_.s_b3,
     in_.s_b4)


def ip6tos(in_):
    addr = in_.contents.sin6_addr._S6_un._S6_u16
    vals = []
    for x in range(0, 8):
        vals.append(sk.ntohs(addr[x]))

    host = '%x:%x:%x:%x:%x:%x:%x:%x' % tuple(vals)
    port = 0
    flowinfo = in_.contents.sin6_flowinfo
    scopeid = in_.contents.sin6_scope_id
    flags = sk.NI_NUMERICHOST | sk.NI_NUMERICSERV
    retAddr, retPort = sk.getnameinfo((host,
     port,
     flowinfo,
     scopeid), flags)
    return retAddr


class server:

    def __init__(self, parent):
        self._type = (18, 52)
        self._nic = ''
        self._src_mac = []
        self._dst_mac = [[255] * 6, [255] * 6]
        self.alldevs = POINTER(pcap_if_t)()
        self.fcode = bpf_program()
        self.errbuf = create_string_buffer(PCAP_ERRBUF_SIZE)
        self.parent = parent
        self.recived_break_loop = False
        self.send_break_loop = False
        self.tn = None
        self.state = self._init()
        return

    def _init(self):
        if not self._getSrcMac():
            self.parent._int_msg('Cannot find the %s ether card' % self.parent.AFI_LANIP)
            return
        if not self._getNIC():
            self.parent._int_msg('Cannot find the %s nic name' % self.parent.AFI_LANIP)
            return
        self.tn = pcap_open_live(self._nic, 65536, 1, 100, self.errbuf)
        if not self.tn:
            self.parent._int_msg('Open nic failed : %s' % self.errbuf)
            return
        if not self.compiles('(ether proto 0x%x%x) and' % self._type + '(ether dst host %02x:%02x:%02x:%02x:%02x:%02x)' % tuple(self._src_mac)):
            self.parent._int_msg('Set ether filter failed')
            return
        thread.start_new_thread(self.RecivedThread, ())
        thread.start_new_thread(self.SendThread, ())
        return 1

    def CreatePacket(self, value = '', dst = [255] * 6):
        p_len = len(value) + 15
        packet = (c_ubyte * p_len)()
        packet[:14] = dst + self._src_mac + list(self._type)
        for i in range(len(value)):
            packet[i + 14] = ord(value[i])

        packet[-1] = 0
        return packet

    def _getSrcMac(self):
        val = os.popen('ipconfig -all').read()
        if self.parent.AFI_LANIP not in val:
            return
        val = val.split(self.parent.AFI_LANIP)[0].split('Physical Address')[-1].split('\n')[0].split(':')[-1].strip().split('-')
        try:
            for mac in val:
                self._src_mac.append(int(mac, 16))

        except:
            self._src_mac = []

        if len(self._src_mac) != 6:
            self._src_mac = []
        return self._src_mac

    def _getNIC(self):
        try:
            if pcap_findalldevs(byref(self.alldevs), self.errbuf) != -1:
                d = self.alldevs.contents
                while d:
                    if d.addresses:
                        a = d.addresses.contents
                        if a.addr.contents.sa_family == sk.AF_INET:
                            mysockaddr_in = sockaddr_in
                            c = ''
                            if a.netmask:
                                aTmp = cast(a.netmask, POINTER(mysockaddr_in))
                                for i in iptos(aTmp.contents.sin_addr.S_un.S_un_b).split('.'):
                                    c = c + '%.2x' % int(i)

                                self.netmask = int(c, 16)
                            if a.addr:
                                aTmp = cast(a.addr, POINTER(mysockaddr_in))
                                if iptos(aTmp.contents.sin_addr.S_un.S_un_b) == self.parent.AFI_LANIP:
                                    self._nic = d.name
                                    break
                    if d.next:
                        d = d.next.contents
                    else:
                        d = False

        except:
            pass

        return self._nic

    def compiles(self, str):
        err1 = pcap_compile(self.tn, byref(self.fcode), str, 1, self.netmask)
        err2 = pcap_setfilter(self.tn, byref(self.fcode))
        if err1 or err2:
            return 0
        return 1

    def RecivedThread(self):
        while not self.recived_break_loop:
            try:
                data = ''
                dst_mac = []
                header = POINTER(pcap_pkthdr)()
                pkt_data = POINTER(c_ubyte)()
                if pcap_next_ex(self.tn, byref(header), byref(pkt_data)) > 0:
                    dst_mac = pkt_data[6:12]
                    for i in range(header.contents.len):
                        if i > 13:
                            if not pkt_data[i]:
                                break
                            else:
                                data += chr(pkt_data[i])

                    if data.split()[0] == 'CCU' and data.split()[1] != 'ping':
                        port = 0
                        if data.split()[1] == 'info':
                            ccu_index = int(data.split()[2])
                            mac = []
                            for _mac in data.split()[3].split(':'):
                                mac.append(int(_mac, 16))

                            self._dst_mac[ccu_index] = mac
                            for _name in data.split():
                                if 'S' in _name:
                                    card_index = int(_name[1])
                                    self.parent.parent.Buffers[ccu_index][card_index + 1].CardName = '%s\t%s' % (_name[2:], card_index)

                    elif data.split()[0] == 'CCU' and data.split()[1] == 'ping':
                        port = int(data.split('vlan')[-1].split()[0][0])
                    elif data.split()[0] == 'DUT':
                        port = int(data.split()[1]) + 8
                    else:
                        port = int(data.split()[1]) + 1
                    if dst_mac in self._dst_mac:
                        if data.split()[0] != 'DUT':
                            data += '\n'
                        CardName = self.parent.parent.Buffers[self._dst_mac.index(dst_mac)][port].CardName + '\t'
                        data = data.split(CardName)[-1]
                        self.parent.parent.Buffers[self._dst_mac.index(dst_mac)][port].SetOut(data)
            except:
                pass

    def SendThread(self):
        while not self.send_break_loop:
            packet = ''
            for index in range(2):
                for buf_index in range(16):
                    cmd = ''
                    try:
                        if 13 in self.parent.parent.Buffers[index][buf_index].BufferIn or 4 in self.parent.parent.Buffers[index][buf_index].BufferIn or 26 in self.parent.parent.Buffers[index][buf_index].BufferIn:
                            if 13 in self.parent.parent.Buffers[index][buf_index].BufferIn:
                                cr_index = self.parent.parent.Buffers[index][buf_index].BufferIn.index(13) + 1
                            if 4 in self.parent.parent.Buffers[index][buf_index].BufferIn:
                                cr_index = self.parent.parent.Buffers[index][buf_index].BufferIn.index(4) + 1
                            if 26 in self.parent.parent.Buffers[index][buf_index].BufferIn:
                                cr_index = self.parent.parent.Buffers[index][buf_index].BufferIn.index(26) + 1
                            for i in range(cr_index):
                                cmd_ord = self.parent.parent.Buffers[index][buf_index].BufferIn[0]
                                cmd += chr(cmd_ord)
                                self.parent.parent.Buffers[index][buf_index].BufferIn.remove(cmd_ord)

                            CardName = self.parent.parent.Buffers[index][buf_index].CardName
                            cmd = '%s\t%s' % (CardName, cmd)
                            if CardName == 'CCU':
                                cmd = cmd[:-1]
                                if 'info' in cmd:
                                    cmd = cmd.split(CardName)[-1].strip()
                            if 'DUT' not in CardName and 'ping' in cmd and CardName != 'CCU':
                                cmd = 'CCU\tping vlan%s%s ' % (int(CardName.split()[1]) + 1, cmd.split('ping')[-1].strip())
                            packet = self.CreatePacket(cmd, self._dst_mac[index])
                            pcap_sendpacket(self.tn, packet, len(packet))
                    except:
                        pass

            if not packet:
                time.sleep(0.1)

    def close(self):
        self.send_break_loop = True
        self.recived_break_loop = True
        time.sleep(1)
        if self.tn:
            pcap_freecode(byref(self.fcode))
            pcap_close(self.tn)

    def __del__(self):
        self.close()