# Embedded file name: G:\AFI\AFIMainGui_YJ\library\gui\BufferConsole.py
import wx, time, thread

class Buffer(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition, size=wx.Size(500, 300), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)
        self.BufferOut = wx.TextCtrl(self, -1, '', style=wx.TE_PROCESS_TAB | wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH2)
        bSizer1.Add(self.BufferOut, 1, wx.EXPAND | wx.ALL, 5)
        self.SetSizer(bSizer1)
        self.Layout()
        self.Centre(wx.BOTH)
        self.parent = parent
        self.BufferIn = []
        self.BufferOut_ = []
        self.CardName = ''
        self.mute = thread.allocate_lock()
        self.BufferOut.Bind(wx.EVT_CHAR, self.onChar)
        self.Bind(wx.EVT_CLOSE, self.close)

    def close(self, event):
        self.Show(False)

    def Clear(self):
        self.BufferOut.Clear()

    def SetIn(self, value):
        for val in value:
            self.BufferIn.append(ord(val))

        return len(value)

    def SetOut(self, value):
        self.BufferOut.AppendText(value)
        if len(self.BufferOut.GetValue()) > 32768:
            self.Clear()
        self.BufferOut_.append(value)
        return len(value)

    def Get(self):
        buf = ''
        for b in self.BufferOut_:
            buf += self.BufferOut_.pop(0)

        return buf

    def onChar(self, event):
        char = event.GetKeyCode()
        if char < 256:
            if chr(char) == '\x08':
                beg = self.BufferOut.GetLastPosition()
                end = beg - 1
                self.BufferOut.Remove(beg, end)
                if self.BufferIn:
                    self.BufferIn.pop()
            else:
                self.BufferIn.append(char)
                self.SetOut(chr(char))