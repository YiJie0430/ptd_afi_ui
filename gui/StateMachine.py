#sudo ln -s /home/pi/scripts/myscript.py /usr/local/bin/myscript
#https://stackoverflow.com/questions/788411/check-to-see-if-python-script-is-running/7758075#7758075
#https://raspberrypi.stackexchange.com/questions/22005/how-to-prevent-python-script-from-running-more-than-once
import ConfigParser,os,sys,time,thread,threading,htx
from tftp import tftpcfg, tftp_engine
import PcapMachine
from testlibs import *
import wx.richtext as rt
import wx
import traceback
import htx
from sysVars import *
import buildkey
from refstation import afiStation, fixtureStatus
from myssh import mySSH, TcpSocket
import subprocess
import pymongo
import DBserver
import logging
import logging.config
#execfile('sysVars.py')
if os.path.isfile('c:\\station.ini'):
    execfile('c:\\station.ini')

ConfigDirectory =os.getcwd()# os.path.join(os.getcwd(),'config')
sys.path.append(ConfigDirectory)

if os.path.isfile('%s\\__init__.py'%ConfigDirectory):
    execfile('%s\\__init__.py'%ConfigDirectory)

logging.config.fileConfig(os.path.dirname(sys.argv[0])+'\\logging.ini', disable_existing_loggers=False)
logger = logging.getLogger(__name__)

scan_trigger = 0

class StateMachine:
    def __init__(self,parent):
        self.ConfigFilePath=''
        self.ConfigFile=[]
        self.parent=parent
        self.PcapServer=''
        self.label_name=''
        self.emp = parent.emp
        self.opt = {'A':'1',
                    'B':'2',
                    'run':'YELLOW',
                    'pass':'GREEN',
                    'fail':'RED',
                    'on':'ON',
                    'off':'OFF'}
        piterm = None
        self.hostname = None
        self.port = None
        self.DB_ip = '192.168.70.50'
        self.DB_port = '27017'

    def getBarcode(self, hostname, port):
        val = None
        status = 0
        #piterm = None
        self.hostname = hostname
        self.port = port
        try:
            state_list = ['/PTD/tmp/statusA', '/PTD/tmp/statusB']
            pi = TcpSocket(hostname, port)
            for cmd in state_list:
                status = int(pi.send('cat {}'.format(cmd)).decode().strip(), 2)
                logger.debug('{}: value of {} is {}'.format(hostname,cmd,status))
                if status == 3:
                    val = pi.send('cat /PTD/tmp/output')
                    logger.info('{}: value of /PTD/tmp/output is {}'.format(hostname,val))
                    if len(val) >= 2:
                        pi.send("echo '' > /PTD/tmp/output")
                        logger.info('{}: reset /PTD/tmp/output'.format(hostname))
                    else:
                        pi.send('echo 00 > {}'.format(cmd))
                        logger.warning('{}: /PTD/tmp/output error, set {} to 00'.format(hostname,cmd))
                            
        except IOError, msg:
            pi.send('echo 00 > {}'.format(cmd))
            error_class = msg.__class__.__name__
            detail = msg.args[0]
            cl, exc, tb = sys.exc_info()
            lastCallStack = traceback.extract_tb(tb)[-1]
            fileName = lastCallStack[0] 
            lineNum = lastCallStack[1]
            funcName = lastCallStack[2]
            errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
            logger.error('{} exception: {}'.format(hostname, errMsg))
        
        if val:
            logger.debug('{}: return {}'.format(hostname, val))
        return val

    def fixtureAction(self, hostname, port, cmd):
        global fixtureStatus
        pi = TcpSocket(hostname, port) 
        carrierStatus = pi.send(cmd).decode()
        if 'failed' in carrierStatus:
            for dut in ['A', 'B']:
                for state in ['RED', 'YELLOW', 'GREEN']:
                    pi.send('fixture LIGHT{}{}:OFF 5'.format(self.opt[dut],state), 'ASS}')
                pi.send('fixture LIGHT{}RED:ON 5'.format(self.opt[dut]), 'ASS}')
            if bool(fixtureStatus):
                if hostname in fixtureStatus.values():
                    idx = fixtureStatus.keys()[fixtureStatus.values().index(hostname)]
                    del fixtureStatus[str(idx)]
        logger.info('{}: {}'.format(hostname,carrierStatus))
        return carrierStatus

    def fixtureInit(self, hostname, port):
        
        pi = TcpSocket(hostname, port)
        pi.send('factoryReset', 'resetDefault')
        logger.debug('{}: reset to default'.format(hostname))
        for dut in ['A', 'B']:
            for state in ['RED', 'YELLOW', 'GREEN']:
                pi.send('fixture LIGHT{}{}:OFF 5'.format(self.opt[dut],state), 'ASS}')
                logger.debug('{}: {} off'.format(hostname,state))
        logger.debug('{}: fixture init..'.format(hostname))                
        return pi.send('fixture MACHINE:HOME 50', 'READY}').decode()

    def carryAct(self, hostname, port, tid, slot):
        pi = TcpSocket(hostname, port)
        if self.parent.demo:
            pi.send('pcbaDetect {} {}:{} {} demo'.format(tid, self.parent.DB_ip, self.parent.DB_port, slot), 'detected')
            logger.debug('{}: pcbaDetect {} {}:{} {} demo'.format(hostname, tid, self.parent.DB_ip, self.parent.DB_port, slot))
        else:
            pi.send('pcbaDetect {} {}:{} {}'.format(tid, self.parent.DB_ip, self.parent.DB_port, slot), 'detected')
            logger.debug('{}: pcbaDetect {} {}:{} {}'.format(hostname, tid, self.parent.DB_ip, self.parent.DB_port, slot))
        
    
    def _in_action(self):
        id_ = self.parent.ScanLabel.GetValue().strip()[-1]
        if id_ not in ('1','2','3','4','5','6','7','8') or len(self.parent.ScanLabel.GetValue()) > 4:
            self._scan_msg("Input dut id (1~8) error : %s"%id_)
            return
        id_ = int(id_)-1
        if self.parent.DUT_LIST[id_][0].GetBackgroundColour() in self.parent.flash:
            #print self.parent.flash, self.parent.DUT_LIST[id_][0].GetBackgroundColour()
            self._scan_msg("DUT %s in the test"%(id_+1))
            return
        for p in range(8):
            if self.parent.DUT_LIST[p][0].GetBackgroundColour()==self.parent.ActionColor:
                self.parent.DUT_LIST[p][0].SetBackgroundColour(self.parent.DisableColor)
                self.parent.DUT_LIST[p][0].Refresh()
        self.parent.DUT_LIST[id_][0].SetBackgroundColour(self.parent.ActionColor)
        self.parent.DUT_LIST[id_][2].SetValue('')
        self.parent.DUT_LIST[id_][3].SetValue('')
        self.parent.DUT_LIST[id_][4].SetValue('')
        self.parent.DUT_LIST[id_][5].SetValue(0)
        self.parent.DUT_LIST[id_][6].SetValue('')
        self.parent.DUT_LIST[id_][0].SetToolTipString('')
        dutid = self.parent.DUT_LIST[id_][1].GetLabel()
        self.parent.DUT_LIST[id_][1].SetLabel(dutid[:6])
        self.parent.DUT_LIST[id_][0].Refresh()
        self.dutid = id_
        self.labels=[]
        self.label_id=0
        self.label_name=''
        self.parent.DUT_LIST[id_][-2].SetValue('')
        self.label_name = self.LabelType[self.label_id].split(',')[0].strip().upper()
        self._in_label(self.label_name)

    def fixture_in_action(self):
        global afiStation
        global fixtureStatus
        cunt=0
        inputid = self.parent.ScanLabel.GetValue().strip().encode('utf-8')
        alive_id = fixtureStatus.keys()
        logger.debug('{}: DUT{} start'.format(afiStation[inputid[0]],inputid))
        if inputid[0] not in ('1','2','3','4','5','6','7','8') or len(self.parent.ScanLabel.GetValue()) > 2:
            self._scan_msg("Input dut id (1~8) error : %s"%inputid)
            logger.warning('{}: input {} out of range'.format(afiStation[inputid[0]], inputid))
            return
        elif inputid[0] not in alive_id:
            self._scan_msg("FIXTURE %s NOT WORK"%inputid)
            logger.warning('{}: Fixture {} not work'.format(afiStation[inputid[0]], inputid))
            return
        else:
            pass

        for idx in self.parent.dutFormat.get(inputid[0]):
            if self.parent.DUT_LIST[idx][0].GetBackgroundColour() not in self.parent.flash:
                if self.parent.DUT_LIST[idx][-1] == inputid:
                    id_ = int(idx)
                    break
                else:
                    continue
            else:
                cunt+=1
            if cunt == 2:
                self._scan_msg("DUT %s PROCESSING"%(inputid))
                return
        '''
        if self.parent.DUT_LIST[id_][0].GetBackgroundColour()==self.parent.ActionColor:
            self.parent.DUT_LIST[id_][0].SetBackgroundColour(self.parent.DisableColor)
            self.parent.DUT_LIST[id_][0].Refresh()
        '''
        logger.debug('{}: DUT{} id is {}'.format(afiStation[inputid[0]],inputid,id_))
        
        self.parent.DUT_LIST[id_][0].SetBackgroundColour(self.parent.ActionColor)
        self.parent.DUT_LIST[id_][2].SetValue('')
        self.parent.DUT_LIST[id_][3].SetValue('')
        self.parent.DUT_LIST[id_][4].SetValue('')
        self.parent.DUT_LIST[id_][5].SetValue(0)
        self.parent.DUT_LIST[id_][6].SetValue('')
        self.parent.DUT_LIST[id_][0].SetToolTipString('')
        dutid = self.parent.DUT_LIST[id_][1].GetLabel()
        self.parent.DUT_LIST[id_][1].SetLabel(dutid[:6])
        self.parent.DUT_LIST[id_][0].Refresh()

        self.dutid = id_
        self.labels=[]
        self.label_id=0
        self.label_name=''
        self.parent.DUT_LIST[id_][-2].SetValue('')
        self.label_name = self.LabelType[self.label_id].split(',')[0].strip().upper()
        self._in_label(self.label_name)

    def state(self, val):
        if val=='DEMO':
            self.demoinit()
        elif val=='MFG':
            self._init()
        else:
            label_ = self.parent.ScanLabel.GetValue()
            if 'CANCEL'==label_.strip().upper():
                self._in_dutid()
                return
            if 'CHECK'==label_.strip().upper():
                self.parent.CheckLED_Dialog.ShowModal()
                return
        if val=='DUT ID':
            if self.parent.fixture:
                self.fixture_in_action()
            else:
                self._in_action()
        if val== self.label_name :
            self._check_label()

    def _check_label(self):
        slot = self.parent.DUT_LIST[self.dutid][-1][0]
        board = self.parent.DUT_LIST[self.dutid][-1][1]
        hostname = afiStation.get(str(slot))
        port = self.parent.socketPort
        val_ = self.parent.ScanLabel.GetValue().encode('utf8')
        len_ = self.LabelType[self.label_id].split(',')[1].strip()
        chart_= self.LabelType[self.label_id].split(',')[2].strip()
        if len(val_) <> int(len_) or chart_ <> val_[:len(chart_)]:
            self._scan_msg("%s Label format error ,length %s ,fix chart %s : %s"%(self.parent.DUT_LIST[self.dutid][-1],
                                                                                len_,
                                                                                chart_,
                                                                                val_))
            logger.warning("%s Label format error ,length %s ,fix chart %s : %s"%(self.parent.DUT_LIST[self.dutid][-1],
                                                                                len_,
                                                                                chart_,
                                                                                val_))
            pi = TcpSocket(hostname, port)
            if 'A' in self.parent.DUT_LIST[self.dutid][-1]:
                pi.send('echo 00 > /PTD/tmp/statusA')
                logger.warning('{}: label error, set /PTD/tmp/statusA to 00'.format(hostname))
            elif 'B' in self.parent.DUT_LIST[self.dutid][-1]:
                pi.send('echo 00 > /PTD/tmp/statusB')
                logger.warning('{}: label error, set /PTD/tmp/statusB to 00'.format(hostname))

            self.parent.LabelName.SetLabel('DUT ID')
            self.parent.DUT_LIST[self.dutid][0].SetBackgroundColour(self.parent.DisableColor)
            self.parent.DUT_LIST[self.dutid][0].Refresh()
            return
        if len(self.LabelType[self.label_id].split(',')) > 3:
            try:
                logger.debug('{}: {}'.format(hostname, str(val_))) #int(val_,16)
            except:
                error_class = msg.__class__.__name__
                detail = msg.args[0]
                cl, exc, tb = sys.exc_info()
                lastCallStack = traceback.extract_tb(tb)[-1]
                fileName = lastCallStack[0] 
                lineNum = lastCallStack[1]
                funcName = lastCallStack[2]
                errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
                logger.error('{} exception: {}'.format(hostname, errMsg))
                logger.error('{}: {}'.format(hostname, str(traceback.format_exc())))
                self._scan_msg("%s Label format error ,length %s ,fix chart %s : %s"%(self.parent.DUT_LIST[self.dutid][-1],
                                                                                        len_,
                                                                                        chart_,
                                                                                        val_))
                pi = TcpSocket(hostname, port)
                if 'A' in self.parent.DUT_LIST[self.dutid][-1]:
                    pi.send('echo 00 > /PTD/tmp/statusA')
                    logger.warning('{}: label format error, set /PTD/tmp/statusA to 00'.format(hostname))
                elif 'B' in self.parent.DUT_LIST[self.dutid][-1]:
                    pi.send('echo 00 > /PTD/tmp/statusB')
                    logger.warning('{}: label format error, set /PTD/tmp/statusB to 00'.format(hostname))
            
                self.parent.LabelName.SetLabel('DUT ID')
                self.parent.DUT_LIST[self.dutid][0].SetBackgroundColour(self.parent.DisableColor)
                self.parent.DUT_LIST[self.dutid][0].Refresh()
                return

        if self.label_id < 2:
            for d in range(8):
                if val_==self.parent.DUT_LIST[d][2].GetValue() or val_==self.parent.DUT_LIST[d][3].GetValue() :
                    c_ = self.parent.DUT_LIST[d][0].GetBackgroundColour()
                    if c_<>self.parent.RedColor and c_<>self.parent.LimeColor and c_<>self.parent.DisableColor:
                        self._scan_msg("{} Bar code scanning repetition".format(self.parent.DUT_LIST[self.dutid][-1]))
                        pi = TcpSocket(hostname, port)
                        if 'A' in self.parent.DUT_LIST[self.dutid][-1]:
                            pi.send('echo 00 > /PTD/tmp/statusA')
                            logger.warning('{}: label repet, set /PTD/tmp/statusA to 00'.format(hostname))
                        elif 'B' in self.parent.DUT_LIST[self.dutid][-1]:
                            pi.send('echo 00 > /PTD/tmp/statusB')
                            logger.warning('{}: label repet, set /PTD/tmp/statusA to 00'.format(hostname))
                        self.parent.DUT_LIST[self.dutid][0].SetBackgroundColour(self.parent.DisableColor)
                        self.parent.DUT_LIST[self.dutid][0].Refresh()
                        self.parent.LabelName.SetLabel('DUT ID')
                        return
        
        self.labels.append(val_)
        if self.label_id==0:
            self.parent.DUT_LIST[self.dutid][2].SetValue(val_)
        elif self.label_id==1:
            self.parent.DUT_LIST[self.dutid][3].SetValue(val_)
        if self.label_id == len(self.LabelType)-1:
            if self.parent.demo:
                DemoFunction(self,'test').start()
            else:
                MainFunction(self,'test').start()
            self._in_dutid()

        else:
            self.label_id += 1
            self.label_name = self.LabelType[self.label_id].split(',')[0].strip().upper()
            self._in_label(self.label_name)

    def _in_label(self,name):
        self.parent.LabelName.SetLabel(name)
        self.parent.ScanLabel.SetBackgroundColour((229,255,204))
        self.parent.StatusBar.SetStatusText("Please input %s label"%name)

    def close(self):
        if self.parent.TFTP:
            if type(self.parent.TFTPServer)<>type(''):
                self.parent.TFTPServer.shutdown()
        if self.parent.AFI:
            if self.PcapServer:
                self.PcapServer.close()

    def _in_dutid(self):
        self.parent.LabelName.SetLabel('DUT ID')
        self.parent.ScanLabel.SetBackgroundColour((204,255,204))
        self.parent.StatusBar.SetStatusText("INPUT DUT ID (1-8)")
        self.parent.ScanLabel.Enable(True)

    def _show_test_state(self,pos,color=0,errcode='',errinfo=''):
        colors_=(self.parent.YellowColor,self.parent.RedColor,self.parent.LimeColor)
        self.parent.DUT_LIST[pos][0].SetBackgroundColour(colors_[color])
        self.parent.DUT_LIST[pos][0].SetToolTipString(errinfo)
        dutid = self.parent.DUT_LIST[pos][1].GetLabel()
        if errcode:
            self.parent.DUT_LIST[pos][1].SetLabel(dutid[:6]+' - %s'%errcode)
        self.parent.DUT_LIST[pos][0].Refresh()

    def _scan_msg(self,e):
        self.parent.StatusBar.SetStatusText(e)
        self.parent.ScanLabel.SetBackgroundColour(wx.Colour(255, 102, 102))

    def _int_msg(self,e):
        self.close()
        time.sleep(1)
        self.parent.StatusBar.SetStatusText(e)
        self.parent.LabelName.SetLabel('Err, No configuration found')
        self.parent.ScanLabel.SetBackgroundColour( wx.Colour(255, 102, 102) )
        self.parent.ScanLabel.Enable(True)

    def _set_gauge_range(self,range_):
        for ind_ in range(len(self.parent.DUT_LIST)):
            self.parent.DUT_LIST[ind_][5].SetRange(range_)

    def set_gauge_range(self, slot, range_):
        self.parent.DUT_LIST[slot][5].SetRange(range_)
            
    def _init(self):
        logger.info('production start')
        self.PN=self.parent.pn
        self.ConfigFilePath='%s\\%s.ini'%(ConfigDirectory,self.PN)
        logger.info('ini path: {}'.format(self.ConfigFilePath))
        self.ConfigFile=ConfigParser.SafeConfigParser()
        if not self.ConfigFile.read(self.ConfigFilePath):
            self._int_msg("Cannot find the %s models"%self.PN)
            logger.warning("Cannot find the %s models"%self.PN)
            return
        else:
            #self.parent.ScanLabel.SetBackgroundColour( "Green" )
            self.parent.configfile = self.ConfigFilePath
            self.parent.ScanLabel.Enable(False)
            self.parent.StatusBar.SetStatusText(self.parent.InitStr)
            self.parent.LabelName.SetLabel('INIT')
            #read tftp config
            try:
                if self.parent.TFTP:
                    cfgdict = tftpcfg.getconfigstrict(ConfigDirectory, self.ConfigFilePath)
                self.PartNumber = self.ConfigFile.get('Base', 'PN')
                self.ModelName = self.ConfigFile.get('Base','ModelName')
                self.StationName = self.ConfigFile.get('Base','StationName')
                if self.parent.TFTP:
                    self.parent.TFTPPath.SetLabel(self.ConfigFile.get('TFTPSERVER','tftprootfolder'))
                self.AFI_LANIP = self.ConfigFile.get('AFI','LANIP')
                self.AFI_ETH0IP = self.ConfigFile.get('AFI','ETH0IP')
                self.AFI_VLAN1IP = self.ConfigFile.get('AFI','VLAN1IP')
                self.AFI_CARDS = self.ConfigFile.get('AFI','CARDS')
                self.TELNET = self.ConfigFile.get('Base','TELNET')
                self.LANIP = self.ConfigFile.get('Base','LANIP')
                self.LabelType = self.ConfigFile.get('Base','LabelType').split('|')
                self.config = self.ConfigFile
                self.parent.FlowsA=self.FlowsA = map(str.strip,self.ConfigFile.get('Base','FlowsA').split(','))
                self.parent.FlowsB=self.FlowsB = map(str.strip,self.ConfigFile.get('Base','FlowsB').split(','))
                
            except tftpcfg.ConfigError, e:
                self._int_msg("Error in config file : %s of [TFTPSERVER] section"%e)
                logger.error("Error in config file : %s of [TFTPSERVER] section"%e)
                return
            except ConfigParser.NoSectionError,e:
                self._int_msg("Error in config file : %s"%e)
                logger.error("Error in config file : %s"%e)
                return
            except ConfigParser.NoOptionError,e:
                self._int_msg("Error in config file : %s"%e)
                logger.error("Error in config file : %s"%e)
                return
            self.logPath = "c:\\%s-Log\\"%self.ModelName+"-".join(map(str,time.gmtime()[:3]))+"\\"
            if not os.path.isdir(self.logPath):
                os.system("mkdir %s"%self.logPath)
            self.parent.Partnum.SetLabel(self.PartNumber)
            self.parent.ModelName.SetLabel(self.ModelName)
            self.parent.StationName.SetLabel(self.StationName)
            
            if self.parent.TFTP:
                self.parent.TFTPServer = tftp_engine.ServerState(**cfgdict)
                thread.start_new_thread(tftp_engine.loop_nogui, (self.parent.TFTPServer,))
                #self.parent.TFTPServerState.SetBackgroundColour("yellow")
                self.parent.TFTPServerState.SetBackgroundColour(wx.Colour(255, 255, 153))
                time.sleep(1)
                if not self.parent.TFTPServer.serving:
                    self._int_msg(self.parent.TFTPServer.text.split('\n')[-1])
                    return
            
            #Mongo daemon
            if self.parent.Mongo:
                self.DBServerState = self.parent.DBServerState
                #self.DB_ip = self.parent.DB_ip
                #self.DB_port = self.parent.DB_port
                self.YellowColor = self.parent.YellowColor
                thread.start_new_thread(DBserver.connect,(self,))
                #time.sleep(1)

            if self.parent.AFI:
                self.parent.AFIServerState.SetBackgroundColour("yellow")
                self.parent.AFIServerState.Refresh()
                self.PcapServer=PcapMachine.server(self)
                if not self.PcapServer.state:
                    self.parent.AFIServerState.SetBackgroundColour(wx.Colour(255, 102, 102))
                    self.parent.AFIServerState.Refresh()
                    return
            MainFunction(self).start()

    def demoinit(self):
        logger.info('demo start')
        self.PN=self.parent.pn
        self.ConfigFilePath=self.PN
        logger.info('ini path: {}'.format(self.ConfigFilePath))
        self.ConfigFile=ConfigParser.SafeConfigParser()
        if not self.ConfigFile.read(self.ConfigFilePath):
            self._int_msg("Cannot find the %s models"%self.PN)
            logger.warning("Cannot find the %s models"%self.PN)
            return
        else:
            self.parent.configfile = self.ConfigFilePath
            self.parent.ScanLabel.Enable(False)
            self.parent.StatusBar.SetStatusText(self.parent.InitStr)
            self.parent.LabelName.SetLabel('INIT')
            #read tftp config
        try:
            tips = 'demo'
            cfgdict = tftpcfg.getconfigstrict(ConfigDirectory, self.ConfigFilePath)
            self.PartNumber = self.ModelName = self.StationName = tips
            if self.parent.TFTP:
                self.parent.TFTPPath.SetLabel(self.ConfigFile.get('TFTPSERVER','tftprootfolder'))
            self.AFI_LANIP = self.ConfigFile.get('AFI','LANIP')
            self.AFI_ETH0IP = self.ConfigFile.get('AFI','ETH0IP')
            self.AFI_VLAN1IP = self.ConfigFile.get('AFI','VLAN1IP')
            self.AFI_CARDS = self.ConfigFile.get('AFI','CARDS')
            self.TELNET = self.ConfigFile.get('Base','TELNET')
            self.LANIP = self.ConfigFile.get('Base','LANIP')
            self.LabelType = self.ConfigFile.get('Base','LabelType').split('|')
            self.config = self.ConfigFile
            self.parent.FlowsA=self.FlowsA = map(str.strip,self.ConfigFile.get('Base','FlowsA').split(','))
            self.parent.FlowsB=self.FlowsB = map(str.strip,self.ConfigFile.get('Base','FlowsB').split(','))
            
        except tftpcfg.ConfigError, e:
            self._int_msg("Error in config file : %s of [TFTPSERVER] section"%e)
            logger.error("Error in config file : %s of [TFTPSERVER] section"%e)
            return
        except ConfigParser.NoSectionError,e:
            self._int_msg("Error in config file : %s"%e)
            logger.error("Error in config file : %s"%e)
            return
        except ConfigParser.NoOptionError,e:
            logger.error("Error in config file : %s"%e)
            self._int_msg("Error in config file : %s"%e)
            return
        self.logPath = "c:\\%s-Log\\"%self.ModelName+"-".join(map(str,time.gmtime()[:3]))+"\\"
        if not os.path.isdir(self.logPath):
            os.system("mkdir %s"%self.logPath)
        self.parent.Partnum.SetLabel(self.PartNumber)
        self.parent.ModelName.SetLabel(self.ModelName)
        self.parent.StationName.SetLabel(self.StationName)
        
        #tftp daemon
        if self.parent.TFTP:
            self.parent.TFTPServer = tftp_engine.ServerState(**cfgdict)
            thread.start_new_thread(tftp_engine.loop_nogui, (self.parent.TFTPServer,))
            self.parent.TFTPServerState.SetBackgroundColour(wx.Colour(255, 255, 153))
            time.sleep(1)
            if not self.parent.TFTPServer.serving:
                self._int_msg(self.parent.TFTPServer.text.split('\n')[-1])
                return

        #Mongo daemon
        if self.parent.Mongo:
            self.DBServerState = self.parent.DBServerState
            #self.DB_ip = self.parent.DB_ip
            #self.DB_port = self.parent.DB_port
            self.YellowColor = self.parent.YellowColor
            thread.start_new_thread(DBserver.connect,(self,))
            #time.sleep(1)
            
        
        if self.parent.AFI:
            self.parent.AFIServerState.SetBackgroundColour("yellow")
            self.parent.AFIServerState.Refresh()
            self.PcapServer=PcapMachine.server(self)
            if not self.PcapServer.state:
                self.parent.AFIServerState.SetBackgroundColour(wx.Colour(255, 102, 102))
                self.parent.AFIServerState.Refresh()
                return
        DemoFunction(self).start()



class MainFunction(threading.Thread):
    def __init__(self,parent,state='init'):
        threading.Thread.__init__(self)
        self.parent = parent
        self.state = state
        self.msg = ''
        self.emp = parent.emp
        self.StationName = parent.StationName
        self.act = None
        self.piterm = None
        self.opt = {'A':'1',
                    'B':'2',
                    'run':'YELLOW',
                    'pass':'GREEN',
                    'fail':'RED',
                    'on':'ON',
                    'off':'OFF'}

    def _init(self):
        '''AFI Init and install patarmete'''
        if self.parent.parent.AFI:
            try:
                term_c = (SocketTTY(self.parent.parent.Buffers,(0,0)),SocketTTY(self.parent.parent.Buffers,(1,0)))
                for i in range(2):
                    #detection ccu card
                    for wait_s in range(30):
                        term_c[i] << 'info'
                        if 'info' in term_c[i].wait('info',3)[-1]:
                            break
                        elif wait_s==29 :
                            logger.error('CCU %s not found'%i)
                            raise Except('CCU %s not found'%i)
                    #detection other cards
                    for wait_s in range(60):
                        info = lWaitCmdTerm(term_c[i],'info','info',3)
                        flag_ = 1
                        for card in self.parent.AFI_CARDS.split('|')[i].split(','):
                            if card.strip().upper() not in info:
                                if wait_s==59:
                                    logger.error('CCU %s : %s not found'%(i,card))
                                    raise Except('CCU %s : %s not found'%(i,card))
                                flag_ = 0
                                break
                        if flag_:break
                    term_cb = (SocketTTY(self.parent.parent.Buffers,(0,1)),SocketTTY(self.parent.parent.Buffers,(1,1)))
                    ports=['1','2','3','4']
                    for termcb in term_cb:
                        for p in ports:
                            lWaitCmdTerm(termcb,'rf %s n'%p,'OK',3)
                    #set ccu eth0 and vlan ip
                    mask_ = '255.255.255.0'
                    eth0_=self.parent.AFI_ETH0IP.split('|')
                    vlan1_=self.parent.AFI_VLAN1IP
                    lWaitCmdTerm(term_c[i],'ip %s %s %s %s'%(eth0_[i],mask_,vlan1_,mask_),'OK',3)

                    #set ccu prelay
                    lWaitCmdTerm(term_c[i],'prelay 69 eth0 69 %s 1'%self.parent.AFI_LANIP,'OK',3)  #tftp prelay
                    for v_ in range(1,8):
                        for p_ in range(1,5):
                            # if p_ != 2:
                            lWaitCmdTerm(term_c[i],'prelay 30%s%s vlan%s%s 23 %s 23'%(v_,p_,v_,p_,self.parent.LANIP),'OK',3)  #telnet prelay
                            # else:
                            # lWaitCmdTerm(term_c[i],'prelay 30%s%s vlan%s%s 23 192.168.100.2 23'%(v_,p_,v_,p_),'OK',3)
                #self.parent._in_dutid()
                self.parent.parent.AFIServerState.SetBackgroundColour(wx.Colour(51, 153, 255))
                self.parent.parent.AFIServerState.Refresh()
            except Except as msg:
                logger.error('{}'.format(msg))
                self.parent._int_msg(str(msg))
                self.parent.parent.AFIServerState.SetBackgroundColour(wx.Colour(255, 102, 102))
                self.parent.parent.AFIServerState.Refresh()
                return
        self.parent._in_dutid()

    def SetPanel(self,*argv):
        ''' argv = sate , errcode , errinfo
            state = start,fail,pass,flash1,flash2,flash2,flash4
        '''
        self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('')
        if argv[0] == 'start':self.parent._show_test_state(self.dutpos)
        if argv[0] == 'fail' :
            self.parent._show_test_state(self.dutpos,color = 1,errcode=argv[1],errinfo=argv[2])
        if argv[0] == 'pass':self.parent._show_test_state(self.dutpos,color = 2)
        if argv[0] == 'flash1':self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('3')
        if argv[0] == 'flash2':self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('4')
        if argv[0] == 'flash3':self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('5')
        if argv[0] == 'flash4':self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('6')

    def SetLog(self,s,color=0):
        '''
            color : 0  --  white
                    1  --  red
                    2  --  lime
        '''
        colors=[(255,255,255),(255,0,0),(0,255,0)]
        s += '\n'
        #self.parent.parent.DUT_LIST[self.dutid][-1].AppendText(str(type(self.log)))
        #self.parent.parent.DUT_LIST[self.dutid][-1].AppendText(str(type(sys.stdout)))
        #if type(self.log)==type(sys.stdout):
        if self.log and not self.log.closed:
            self.log.write(s)
            self.log.flush()
        lines = len(s.split('\n'))
        #self.parent.parent.DUT_LIST[self.dutid][-1].BeginTextColour(colors[color])
        #self.parent.parent.DUT_LIST[self.dutid][-1].selcolor(colors[color])
        #RichTextAttr = rt.TextAttrEx()
        #RichTextAttr.SetTextColour(colors[color])
        #self.parent.parent.DUT_LIST[self.dutid][-1].BeginStyle(RichTextAttr)
        beg = self.parent.parent.DUT_LIST[self.dutpos][-2].GetLastPosition()
        end = self.parent.parent.DUT_LIST[self.dutpos][-2].GetLastPosition() + len(s)
        self.parent.parent.DUT_LIST[self.dutpos][-2].SetStyle(beg,end,wx.TextAttr(colors[color],'black'))
        #self.parent.parent.DUT_LIST[self.dutid][-2].WriteText(s)
        self.parent.parent.DUT_LIST[self.dutpos][-2].AppendText(s)
        self.parent.parent.DUT_LIST[self.dutpos][-2].ScrollLines(lines + 1)
        #self.parent.parent.DUT_LIST[self.dutid][-2].EndStyle()
        self.parent.parent.DUT_LIST[self.dutpos][-2].ShowPosition(end)
        #time.sleep(0.2)

    def SetProcess(self,flow):
        self.parent.parent.DUT_LIST[self.dutpos][4].SetValue(flow)
        val_ = self.parent.parent.DUT_LIST[self.dutpos][5].GetValue()
        self.parent.parent.DUT_LIST[self.dutpos][5].SetValue(val_+1)

    def GetConfig(self,section,item):
        return self.parent.ConfigFile.get(section,item).strip()

    def ExcuteCmd(self,term,cmds):
        for cmd_ in map(strip,cmds):
            if not cmd_:continue
            cmd,waitstr,timeout,count=map(strip,cmd_.split(','))
            c_port = self.dutid
            if c_port > 3 : c_port  -=  4
            if cmd in self.cmd_pwd_dict:
                term << cmd
                if waitstr in term.wait(waitstr,5)[-1]: continue
                for try_ in range(6):
                    lWaitCmdTerm(self.term[0],'uartd close %s'%c_port,'ok',5)
                    term << self.cmd_pwd_dict[cmd]
                    time.sleep(0.5)
                    term << cmd
                    lWaitCmdTerm(self.term[0],'uartd open %s 0'%c_port,'ok',5)
                    term << ''
                    data = term.wait(waitstr,5)[-1]
                    if waitstr in data : break
                    if try_==5:
                        logger.error('failed: %s,%s'%(cmd,data))
                        raise Except('failed: %s,%s'%(cmd,data))
                continue
            lWaitCmdTerm(term,cmd,waitstr,float(timeout),int(count))
    
    def _act(self,piTerm):
        return piTerm.send('actGet').decode()

    def led(self, opt, dut, status, act, pi):
        for state in ['RED', 'YELLOW', 'GREEN']:
            pi.send('fixture LIGHT{}{}:OFF 5'.format(opt[dut],state))
        pi.send('fixture LIGHT{}{}:{} 5'.format(opt[dut],
                                            opt[status],
                                            opt[act]))

    def run(self):
        if self.state=='init':
            #self.parent._in_dutid()
            self._init()
        else:
            try:
                self.result = ['FAIL',1]
                self.errcode = ''
                self.errinfo = ''
                self.log = ''
                self.dutid = self.dutpos = self.parent.dutid
                self.act = None
                self.Flows = None
                if self.dutid >=8:
                    self.dutid-=8
                self.port=self.dutid+1
                if self.port >4 : 
                    self.port -= 4
                self.starttime=time.time()
                sw_id_ = self.dutid + 2
                vm_id_ = self.dutid/2 + 6
                dut_id_ = self.dutid+8
                tport_ = '30%s1'%(self.dutid+1)
                if self.dutid > 3:
                    sw_id_ = self.dutid - 2
                    vm_id_ = (self.dutid-4)/2 + 6
                    tport_ = '30%s1'%(self.dutid-3)
                    dut_id_ = self.dutid + 4
                if self.dutpos >= 8:
                    self.dut = 'B'
                    self.path = '/PTD/tmp/statusB'
                else:
                    self.dut = 'A'
                    self.path = '/PTD/tmp/statusA'
                
                piterm = TcpSocket(afiStation.get(str(self.dutid+1)), self.parent.parent.socketPort)
                self.term = list()  #[CCU,CB,SW,VM,DUT]
                self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,0)))            #ccu term
                self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,1)))            #CB term
                self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,sw_id_)))       #SW term
                self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,vm_id_)))       #VM term
                self.term.append(piterm)
                if int(self.parent.TELNET.strip()):
                    self.term.append(htx.Telnet(self.parent.LANIP,int(tport_)))                      #Dut telnet
                else:
                    self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,dut_id_)))   #Dut term
                self.labels=self.parent.labels #0:mac,1:sn,2:ssid,3:wpakey
                #self.parent.parent.DUT_LIST[self.dutid][-1].Enable( False )
                #self.SetPanel('start')
                self.Return=[self.parent.parent.CheckLED_Dialog,self.parent.parent.emp]
                self.led(self.opt, self.dut, 'run', 'on', piterm)
                piterm.send('echo 01 > {}'.format(self.path))
                logger.debug('{}: {} testing'.format(afiStation.get(str(self.port)), self.dut))
                cnt_num=1
                while os.path.isfile('%s%s_%s.%s'%(self.parent.logPath,self.labels[0],cnt_num,self.StationName)):
                    cnt_num+=1
                self.log=open('%s%s_%s.%s'%(self.parent.logPath,self.labels[0],cnt_num,self.StationName),'w')
                #self.log=open('%s%s.%s'%(self.parent.logPath,self.labels[0],self.StationName),'w')

                for try_ in range(15):
                    act = self.parent.fixtureAction(afiStation.get(str(self.port)), self.parent.parent.socketPort, 'actGet')
                    #act = piterm.send('actGet').decode()
                    logger.info('{}: round{}-DUT{} get {}'.format(afiStation.get(str(self.port)), try_, self.dut, act))
                    if 'flowa' in act:
                        self.Flows = self.parent.FlowsA
                        break
                    elif 'flowb' in act:
                        self.Flows = self.parent.FlowsB
                        break
                    elif 'failed' in act:
                        logger.error('{}:{}'.format(afiStation.get(str(self.port)), act))
                        raise Except('%s'%act)
                    else:
                        time.sleep(0.5)
                    if try_ == 14:
                        logger.error('{}:{}'.format(afiStation.get(str(self.port)), act))
                        raise Except('flow err')
                    
                self.parent.set_gauge_range(self.dutpos,len(self.Flows))
                
                self.SetLog("Auto-AFI v%s: %s, Station: %s ; dut_id:%s"%(VERSION,self.parent.ModelName,self.parent.parent.station,int(self.dutid)+1))
                self.SetLog("EMP : %s"%self.emp)
                self.SetLog("Flow : %s"%(act))
                self.SetLog("------------------------------------------------------------------------------")
                self.SetLog("Start Time : %s"%time.asctime())
                self.mac = self.labels[0]
                self.SetLog("MAC Address : %s"%self.mac )
                if len(self.labels) > 1:
                    self.sn = self.labels[1]
                    self.SetLog("Carrier Number : %s"%self.sn)
                if len(self.labels) > 2:
                    self.ssid = self.labels[2]
                    self.SetLog("SSID : %s"%self.ssid)
                if len(self.labels) > 3:
                    self.wpakey = self.labels[3]
                    self.SetLog("WPAKey : %s"%self.wpakey)
                self.cmd_pwd_dict = eval(self.GetConfig('Base','CMD_PWD_DICT'))
                
                test_time = time.time()
                for flow in self.Flows[:-1]:
                    f_time = time.time()
                    self.SetPanel('start')
                    self.SetProcess(self.GetConfig(flow,'FlowName'))
                    self.ExcuteCmd(self.term[-1],map(strip,self.GetConfig(flow,'InCmd').split('|')))
                    if int (self.GetConfig(flow,'Enable').strip()):
                        eval('%s(self.dutpos,self.term,self.labels,self.SetPanel,self.SetLog,self.GetConfig,flow,[self.Return])'%self.GetConfig(flow,'FunctionName'))
                    self.ExcuteCmd(self.term[-1],map(strip,self.GetConfig(flow,'OutCmd').split('|')))
                    end_f_time = time.time()- f_time
                    self.SetLog( "\n")
                    self.SetLog( "%s Test time: %3.2f (sec)"%(self.GetConfig(flow,'FunctionName'),end_f_time))
                    self.SetLog( "---------------------------------------------------------------------------")
                ######################################################################################
                self.result=('PASS',2)
                self.led(self.opt, self.dut, 'pass', 'on', piterm)

            except Except,msg:
                msg = str(msg)[:500]
                if 'ErrorCode' not in msg:
                    msg = 'ErrorCode(0000):'+msg
                self.errcode = msg.split('ErrorCode(')[-1].split(')')[0]
                self.errinfo = str(msg)
                self.led(self.opt, self.dut, 'fail', 'on', piterm)
                self.result=('FAIL',1)
                piterm.send('echo 00 > {}'.format(self.path))
                self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
                self.SetLog(msg,1)
                logger.error('{}'.format(msg))
            
            except Exception as msg:
                error_class = msg.__class__.__name__
                detail = msg.args[0]
                cl, exc, tb = sys.exc_info()
                lastCallStack = traceback.extract_tb(tb)[-1]
                fileName = lastCallStack[0] 
                lineNum = lastCallStack[1]
                funcName = lastCallStack[2]
                errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
                self.led(self.opt, self.dut, 'fail', 'on', piterm) 
                self.result=('FAIL',1)
                piterm.send('echo 00 > {}'.format(self.path))
                self.SetPanel(self.result[0].lower(),'','')
                self.SetLog(errMsg,1)
                logger.error('{}'.format(errMsg))

            except:
                msg=str(traceback.format_exc())
                if 'ErrorCode' not in msg:
                    msg = 'ErrorCode(0001):'+msg
                self.errcode = msg.split('ErrorCode(')[-1].split(')')[0]
                self.errinfo = str(msg)
                self.led(self.opt, self.dut, 'fail', 'on', piterm) 
                self.result=('FAIL',1)
                piterm.send('echo 00 > {}'.format(self.path))
                self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
                self.SetLog(msg,1)

            self.term[0] << 'uartd close %s'%(self.port-1)
            self.SetLog("End Time : %s"%time.asctime())
            self.SetLog("Total Time : %s"%(time.time()-self.starttime))
            self.SetLog("Test Result:%s"%self.result[0],self.result[1])
            self.Return.append(self.result[0])
            
            try:
                if self.result[0]=='PASS':
                    self.SetProcess(self.GetConfig(self.Flows[-1],'FlowName'))
                    if int (self.GetConfig(self.Flows[-1],'Enable').strip()) and self.log:
                        eval('%s(self.dutpos,self.term,self.labels,self.SetPanel,self.SetLog,self.GetConfig,flow,[self.Return])'%self.GetConfig(self.Flows[-1],'FunctionName'))
            except Except,msg:
                msg = str(msg)
                if 'ErrorCode' not in msg:
                    msg = 'ErrorCode(0002):'+msg
                if self.result[0]=='PASS':
                    self.errcode = msg.split('ErrorCode(')[-1].split(')')[0]
                    self.errinfo = str(msg)
                self.led(self.opt, self.dut, 'fail', 'on', piterm)
                piterm.send('echo 00 > {}'.format(self.path))
                self.result=('FAIL',1)
                self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
                self.SetLog(msg,1)
                logger.error('{}'.format(msg))

            except:
                msg = str(traceback.format_exc())
                if 'ErrorCode' not in msg:
                    msg = 'ErrorCode(0003):'+msg
                if self.result[0]=='PASS':
                    self.errcode = msg.split('ErrorCode(')[-1].split(')')[0]
                    self.errinfo = str(msg)
                self.led(self.opt, self.dut, 'fail', 'on', piterm)
                piterm.send('echo 00 > {}'.format(self.path))
                self.result=('FAIL',1)
                self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
                self.SetLog(msg,1)
                logger.error('{}'.format(msg))
            
            if self.log:self.log.close()
            piterm.send('echo 00 > {}'.format(self.path))
            #self._act(piterm)
            piterm.send('actGet')
            self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
            logger.debug('{}: {} test done'.format(afiStation.get(str(self.port)), self.dut))

class DemoFunction(threading.Thread):
    def __init__(self,parent,state='init'):
        threading.Thread.__init__(self)
        self.parent = parent
        self.state = state
        self.msg = ''
        self.emp = parent.emp
        self.StationName = parent.StationName
        self.act = None
        self.piterm = None
        self.opt = {'A':'1',
                    'B':'2',
                    'run':'YELLOW',
                    'pass':'GREEN',
                    'fail':'RED',
                    'on':'ON',
                    'off':'OFF'}

    def _init(self):
        '''AFI Init and install patarmete'''
        if self.parent.parent.AFI:
            try:
                term_c = (SocketTTY(self.parent.parent.Buffers,(0,0)),SocketTTY(self.parent.parent.Buffers,(1,0)))
                for i in range(2):
                    #detection ccu card
                    for wait_s in range(30):
                        term_c[i] << 'info'
                        if 'info' in term_c[i].wait('info',3)[-1]:
                            break
                        elif wait_s==29 :
                            logger.error('CCU %s not found'%i)
                            raise Except('CCU %s not found'%i)
                    #detection other cards
                    for wait_s in range(60):
                        info = lWaitCmdTerm(term_c[i],'info','info',3)
                        flag_ = 1
                        for card in self.parent.AFI_CARDS.split('|')[i].split(','):
                            if card.strip().upper() not in info:
                                if wait_s==59:
                                    logger.error('CCU %s : %s not found'%(i,card))
                                    raise Except('CCU %s : %s not found'%(i,card))
                                flag_ = 0
                                break
                        if flag_:break
                    term_cb = (SocketTTY(self.parent.parent.Buffers,(0,1)),SocketTTY(self.parent.parent.Buffers,(1,1)))
                    ports=['1','2','3','4']
                    for termcb in term_cb:
                        for p in ports:
                            lWaitCmdTerm(termcb,'rf %s n'%p,'OK',3)
                    #set ccu eth0 and vlan ip
                    mask_ = '255.255.255.0'
                    eth0_=self.parent.AFI_ETH0IP.split('|')
                    vlan1_=self.parent.AFI_VLAN1IP
                    lWaitCmdTerm(term_c[i],'ip %s %s %s %s'%(eth0_[i],mask_,vlan1_,mask_),'OK',3)

                    #set ccu prelay
                    lWaitCmdTerm(term_c[i],'prelay 69 eth0 69 %s 1'%self.parent.AFI_LANIP,'OK',3)  #tftp prelay
                    for v_ in range(1,8):
                        for p_ in range(1,5):
                            # if p_ != 2:
                            lWaitCmdTerm(term_c[i],'prelay 30%s%s vlan%s%s 23 %s 23'%(v_,p_,v_,p_,self.parent.LANIP),'OK',3)  #telnet prelay
                            # else:
                            # lWaitCmdTerm(term_c[i],'prelay 30%s%s vlan%s%s 23 192.168.100.2 23'%(v_,p_,v_,p_),'OK',3)
                #self.parent._in_dutid()
                self.parent.parent.AFIServerState.SetBackgroundColour(wx.Colour(51, 153, 255))
                self.parent.parent.AFIServerState.Refresh()
            except Except as msg:
                logger.error('{}'.format(msg))
                self.parent._int_msg(str(msg))
                self.parent.parent.AFIServerState.SetBackgroundColour(wx.Colour(255, 102, 102))
                self.parent.parent.AFIServerState.Refresh()
                return

        self.parent._in_dutid()

    def SetPanel(self,*argv):
        ''' argv = sate , errcode , errinfo
            state = start,fail,pass,flash1,flash2,flash2,flash4
        '''
        self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('')
        if argv[0] == 'start':self.parent._show_test_state(self.dutpos)
        if argv[0] == 'fail' :
            self.parent._show_test_state(self.dutpos, color = 1, errcode=argv[1], errinfo=argv[2])
        if argv[0] == 'pass':self.parent._show_test_state(self.dutpos, color = 2)
        if argv[0] == 'flash1':self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('3')
        if argv[0] == 'flash2':self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('4')
        if argv[0] == 'flash3':self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('5')
        if argv[0] == 'flash4':self.parent.parent.DUT_LIST[self.dutpos][0].SetLabel('6')

    def SetLog(self,s,color=0):
        '''
            color : 0  --  white
                    1  --  red
                    2  --  lime
        '''
        colors=[(255,255,255),(255,0,0),(0,255,0)]
        s += '\n'
        #self.parent.parent.DUT_LIST[self.dutid][-1].AppendText(str(type(self.log)))
        #self.parent.parent.DUT_LIST[self.dutid][-1].AppendText(str(type(sys.stdout)))
        #if type(self.log)==type(sys.stdout):
        if self.log and not self.log.closed:
            self.log.write(s)
            self.log.flush()
        lines = len(s.split('\n'))
        #self.parent.parent.DUT_LIST[self.dutid][-1].BeginTextColour(colors[color])
        #self.parent.parent.DUT_LIST[self.dutid][-1].selcolor(colors[color])
        #RichTextAttr = rt.TextAttrEx()
        #RichTextAttr.SetTextColour(colors[color])
        #self.parent.parent.DUT_LIST[self.dutid][-1].BeginStyle(RichTextAttr)
        beg = self.parent.parent.DUT_LIST[self.dutpos][-2].GetLastPosition()
        end = self.parent.parent.DUT_LIST[self.dutpos][-2].GetLastPosition() + len(s)
        self.parent.parent.DUT_LIST[self.dutpos][-2].SetStyle(beg,end,wx.TextAttr(colors[color],'black'))
        #self.parent.parent.DUT_LIST[self.dutid][-2].WriteText(s)
        self.parent.parent.DUT_LIST[self.dutpos][-2].AppendText(s)
        self.parent.parent.DUT_LIST[self.dutpos][-2].ScrollLines(lines + 1)
        #self.parent.parent.DUT_LIST[self.dutid][-2].EndStyle()
        self.parent.parent.DUT_LIST[self.dutpos][-2].ShowPosition(end)
        #time.sleep(0.2)

    def SetProcess(self,flow):
        self.parent.parent.DUT_LIST[self.dutpos][4].SetValue(flow)
        val_ = self.parent.parent.DUT_LIST[self.dutpos][5].GetValue()
        self.parent.parent.DUT_LIST[self.dutpos][5].SetValue(val_+1)

    def GetConfig(self,section,item):
        return self.parent.ConfigFile.get(section,item).strip()

    def ExcuteCmd(self,term,cmds):
        for cmd_ in map(strip,cmds):
            if not cmd_:continue
            cmd,waitstr,timeout,count=map(strip,cmd_.split(','))
            c_port = self.dutid
            if c_port > 3 : c_port  -=  4
            if cmd in self.cmd_pwd_dict:
                term << cmd
                if waitstr in term.wait(waitstr,5)[-1]: continue
                for try_ in range(6):
                    lWaitCmdTerm(self.term[0],'uartd close %s'%c_port,'ok',5)
                    term << self.cmd_pwd_dict[cmd]
                    time.sleep(0.5)
                    term << cmd
                    lWaitCmdTerm(self.term[0],'uartd open %s 0'%c_port,'ok',5)
                    term << ''
                    data = term.wait(waitstr,5)[-1]
                    if waitstr in data : break
                    if try_==5:
                        logger.error('failed: %s,%s'%(cmd,data))
                        raise Except('failed: %s,%s'%(cmd,data))
                continue
            lWaitCmdTerm(term,cmd,waitstr,float(timeout),int(count))

    def _act(self):
        return piterm.send('actGet').decode()

    def led(self, opt, dut, status, act, pi):
        for state in ['RED', 'YELLOW', 'GREEN']:
            pi.send('fixture LIGHT{}{}:OFF 5'.format(opt[dut],state))
        pi.send('fixture LIGHT{}{}:{} 5'.format(opt[dut],
                                            opt[status],
                                            opt[act]))

    def run(self):
        if self.state=='init':
            #self.parent._in_dutid()
            self._init()
        else:
            try:
                self.result = ['FAIL',1]
                self.errcode = ''
                self.errinfo = ''
                self.log = ''
                self.dutid = self.dutpos = self.parent.dutid
                self.act = None
                self.Flows = None
                if self.dutid >=8:
                    self.dutid-=8
                self.port=self.dutid+1
                if self.port >4 : 
                    self.port -= 4
                self.starttime=time.time()
                sw_id_ = self.dutid + 2
                vm_id_ = self.dutid/2 + 6
                dut_id_ = self.dutid+8
                tport_ = '30%s1'%(self.dutid+1)
                if self.dutid > 3:
                    sw_id_ = self.dutid - 2
                    vm_id_ = (self.dutid-4)/2 + 6
                    tport_ = '30%s1'%(self.dutid-3)
                    dut_id_ = self.dutid + 4
                if self.dutpos >= 8:
                    self.dut = 'B'
                    self.path = '/PTD/tmp/statusB'
                else:
                    self.dut = 'A'
                    self.path = '/PTD/tmp/statusA'
                            
                piterm = TcpSocket(afiStation.get(str(self.dutid+1)), self.parent.parent.socketPort)
                self.term = list() #[CCU,CB,SW,VM,DUT]
                self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,0)))            #ccu term
                self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,1)))            #CB term
                self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,sw_id_)))       #SW term
                self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,vm_id_)))       #VM term
                self.term.append(piterm)
                if int(self.parent.TELNET.strip()):
                    self.term.append(htx.Telnet(self.parent.LANIP,int(tport_)))                      #Dut telnet
                else:
                    self.term.append(SocketTTY(self.parent.parent.Buffers,(self.dutid/4,dut_id_)))   #Dut term
                self.labels=self.parent.labels #0:mac,1:sn,2:ssid,3:wpakey
                #self.parent.parent.DUT_LIST[self.dutid][-1].Enable( False )
                #self.SetPanel('start')
                self.Return=[self.parent.parent.CheckLED_Dialog,self.parent.parent.emp]
                self.led(self.opt, self.dut, 'run', 'on', piterm)
                piterm.send('echo 01 > {}'.format(self.path))
                logger.debug('{}: {} testing'.format(afiStation.get(str(self.port)), self.dut))
                cnt_num=1
                while os.path.isfile('%s%s_%s.%s'%(self.parent.logPath,self.labels[0],cnt_num,self.StationName)):
                    cnt_num+=1
                self.log=open('%s%s_%s.%s'%(self.parent.logPath,self.labels[0],cnt_num,self.StationName),'w')
                #self.log=open('%s%s.%s'%(self.parent.logPath,self.labels[0],self.StationName),'w')
                
                for try_ in range(15):
                    act = self.parent.fixtureAction(afiStation.get(str(self.port)), self.parent.parent.socketPort, 'actGet')
                    #act = piterm.send('actGet').decode()
                    logger.info('{}: round{}-DUT{} get {}'.format(afiStation.get(str(self.port)), try_, self.dut, act))
                    if 'flowa' in act:
                        self.Flows = self.parent.FlowsA
                        break
                    elif 'flowb' in act:
                        self.Flows = self.parent.FlowsB
                        break
                    elif 'failed' in act:
                        logger.error('{}:{}'.format(afiStation.get(str(self.port)), act))
                        raise Except('%s'%act)
                    else:
                        time.sleep(0.5)
                    if try_ == 14:
                        logger.error('{}:{}'.format(afiStation.get(str(self.port)), act))
                        raise Except('flow err')
                
                self.parent.set_gauge_range(self.dutpos,len(self.Flows))
                
                self.SetLog("Auto-AFI v%s: %s, Station: %s ; dut_id:%s"%(VERSION,self.parent.ModelName,self.parent.parent.station,int(self.dutid)+1))
                self.SetLog("EMP : %s"%self.emp)
                self.SetLog("Flow : %s"%(act))
                self.SetLog("------------------------------------------------------------------------------")
                self.SetLog("Start Time : %s"%time.asctime())
                self.mac = self.labels[0]
                self.SetLog("MAC Address : %s"%self.mac )
                if len(self.labels) > 1:
                    self.sn = self.labels[1]
                    self.SetLog("Carrier Number : %s"%self.sn)
                if len(self.labels) > 2:
                    self.ssid = self.labels[2]
                    self.SetLog("SSID : %s"%self.ssid)
                if len(self.labels) > 3:
                    self.wpakey = self.labels[3]
                    self.SetLog("WPAKey : %s"%self.wpakey)
                self.cmd_pwd_dict = eval(self.GetConfig('Base','CMD_PWD_DICT'))
                
                test_time = time.time()
                for flow in self.Flows[:-1]:
                    f_time = time.time()
                    self.SetPanel('start')
                    self.SetProcess(self.GetConfig(flow,'FlowName'))
                    self.ExcuteCmd(self.term[-1],map(strip,self.GetConfig(flow,'InCmd').split('|')))
                    if int(self.GetConfig(flow,'Enable').strip()):
                        eval('%s(self.dutpos,self.term,self.labels,self.SetPanel,self.SetLog,self.GetConfig,flow,[self.Return])'%self.GetConfig(flow,'FunctionName'))
                    self.ExcuteCmd(self.term[-1],map(strip,self.GetConfig(flow,'OutCmd').split('|')))
                    end_f_time = time.time()- f_time
                    self.SetLog( "\n")
                    self.SetLog( "%s Test time: %3.2f (sec)"%(self.GetConfig(flow,'FunctionName'),end_f_time))
                    self.SetLog( "---------------------------------------------------------------------------")
                ######################################################################################
                self.result=('PASS',2)
                self.led(self.opt, self.dut, 'pass', 'on', piterm)
                    
            except Except,msg:
                msg = str(msg)[:500]
                if 'ErrorCode' not in msg:
                    msg = 'ErrorCode(0000):'+msg
                self.errcode = msg.split('ErrorCode(')[-1].split(')')[0]
                self.errinfo = str(msg)
                self.led(self.opt, self.dut, 'fail', 'on', piterm)
                self.result=('FAIL',1)
                piterm.send('echo 00 > {}'.format(self.path))
                self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
                self.SetLog(msg,1)
                logger.error('{}'.format(msg))
            
            except Exception as msg:
                error_class = msg.__class__.__name__
                detail = msg.args[0]
                cl, exc, tb = sys.exc_info()
                lastCallStack = traceback.extract_tb(tb)[-1]
                fileName = lastCallStack[0] 
                lineNum = lastCallStack[1]
                funcName = lastCallStack[2]
                errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
                self.led(self.opt, self.dut, 'fail', 'on', piterm) 
                self.result=('FAIL',1)
                piterm.send('echo 00 > {}'.format(self.path))
                self.SetPanel(self.result[0].lower(),'','')
                self.SetLog(errMsg,1)
                logger.error('{}'.format(errMsg))

            except:
                msg=str(traceback.format_exc())
                if 'ErrorCode' not in msg:
                    msg = 'ErrorCode(0001):'+msg
                self.errcode = msg.split('ErrorCode(')[-1].split(')')[0]
                self.errinfo = str(msg)
                self.led(self.opt, self.dut, 'fail', 'on', piterm) 
                self.result=('FAIL',1)
                piterm.send('echo 00 > {}'.format(self.path))
                self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
                self.SetLog(msg,1)
                logger.error('{}'.format(msg))
                
                
            self.term[0] << 'uartd close %s'%(self.port-1)
            self.SetLog("End Time : %s"%time.asctime())
            self.SetLog("Total Time : %s"%(time.time()-self.starttime))
            self.SetLog("Test Result:%s"%self.result[0],self.result[1])
            self.Return.append(self.result[0])
            
            try:
                if self.result[0]=='PASS':
                    self.SetProcess(self.GetConfig(self.Flows[-1],'FlowName'))
                    if int (self.GetConfig(self.Flows[-1],'Enable').strip()) and self.log:
                        eval('%s(self.dutpos,self.term,self.labels,self.SetPanel,self.SetLog,self.GetConfig,flow,[self.Return])'%self.GetConfig(self.Flows[-1],'FunctionName'))
            except Except,msg:
                msg = str(msg)
                if 'ErrorCode' not in msg:
                    msg = 'ErrorCode(0002):'+msg
                if self.result[0]=='PASS':
                    self.errcode = msg.split('ErrorCode(')[-1].split(')')[0]
                    self.errinfo = str(msg)
                self.led(self.opt, self.dut, 'fail', 'on', piterm)
                piterm.send('echo 00 > {}'.format(self.path))
                self.result=('FAIL',1)
                self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
                self.SetLog(msg,1)
                logger.error('{}'.format(msg))

            except:
                msg = str(traceback.format_exc())
                if 'ErrorCode' not in msg:
                    msg = 'ErrorCode(0003):'+msg
                if self.result[0]=='PASS':
                    self.errcode = msg.split('ErrorCode(')[-1].split(')')[0]
                    self.errinfo = str(msg)
                self.led(self.opt, self.dut, 'fail', 'on', piterm)
                piterm.send('echo 00 > {}'.format(self.path))
                self.result=('FAIL',1)
                self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
                self.SetLog(msg,1)
                logger.error('{}'.format(msg))

            if self.log:self.log.close()
            piterm.send('echo 00 > {}'.format(self.path))
            #self._act(piterm)    
            piterm.send('actGet')
            self.SetPanel(self.result[0].lower(),self.errcode,self.errinfo)
            logger.debug('{}: {} test done'.format(afiStation.get(str(self.port)), self.dut))
